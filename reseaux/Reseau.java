/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.reseaux;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javafx.scene.paint.Color;
import projet_java_gl_reseau.player.Compte;
import projet_java_gl_reseau.player.Joueur;

/**
 *
 * @author greg
 */
public class Reseau {

    private static Socket socket;
    private static BufferedReader in;
    private static PrintWriter out;

    public static void initConnexionServeur() throws IOException {
        //socket = new Socket(InetAddress.getLocalHost(), 2009);
        socket = new Socket("localhost", 2009);
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream());
    }

    public static boolean connexionServeur(String user, String pass) throws IOException, ClassNotFoundException {
        out.println(user + ";" + pass);
        out.flush();
        String connexion = in.readLine();
        return connexion.equals("true");
    }

    public static void close() throws IOException {
        socket.close();
    }

    //exemple message : greg,rouge,0;franck,jaune,0
    private static ArrayList<Joueur> decryptInforJoueurServeur(String message) {
        String[] tabJoueur = message.split(";");

        ArrayList<Joueur> lesJoueur = new ArrayList<Joueur>();
        for (int i = 0; i < tabJoueur.length; i++) {
            Color color = Color.valueOf(tabJoueur[i].split(",")[1]);
            int solde = Integer.parseInt(tabJoueur[i].split(",")[2]);
            lesJoueur.add(new Joueur(tabJoueur[i].split(",")[0], color, new Compte(solde)));
        }
        return lesJoueur;
    }

    //permet de recuperer la map serveur
    public static String getMapXmlServeur() throws IOException {
        return in.readLine();
    }

    //permet de recuperer la commande envoyé par le serveur
    public static String getCommandeServeur() throws IOException {
        String message = in.readLine();
        afficheMessageReceive(message);
        return message;
    }

    public static void sendMessage(String message) {
        out.println(message);
        out.flush();
        afficheMessageSend(message);
    }

    public static void afficheMessageSend(String message) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd : k:m:s:S");
        Date jour = new Date();
        System.out.println("[ENVOYER][INFO " + dateFormat.format(jour) + "] : " + message);
    }

    public static void afficheMessageReceive(String message) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd : k:m:s:S");
        Date jour = new Date();
        System.out.println("[RECU][INFO " + dateFormat.format(jour) + "] : " + message);
    }
}
