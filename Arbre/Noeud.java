 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.Arbre;

import java.util.ArrayList;
import projet_java_gl_reseau.Jeu.Carte;
import projet_java_gl_reseau.Jeu.lesCarte.CarteVille;

/**
 *
 * @author greg
 */
public class Noeud {

    //ATTRIBUT

    private CarteVille laVille;
    private ArrayList<Noeud> lesVilles;
    private ArrayList<ArrayList<Integer>> lesCartesRails;

    //CONSTRUCTEUR
    public Noeud(Noeud noeudP){
        this.laVille = noeudP.getVille();
        this.lesVilles = new ArrayList<Noeud>(noeudP.lesVilles);
        this.lesCartesRails = new ArrayList<ArrayList<Integer>>(noeudP.lesCartesRails);
    }
    public Noeud(CarteVille laVille) {
        this.laVille = laVille;
        this.lesVilles = new ArrayList<Noeud>();
        this.lesCartesRails = new ArrayList<ArrayList<Integer>>();
    }

    public Noeud(CarteVille laVille, ArrayList<Integer> lesArcs) {
        this.laVille = laVille;
        this.lesVilles = new ArrayList<Noeud>();
        this.lesCartesRails = new ArrayList<ArrayList<Integer>>();
        this.lesCartesRails.add(lesArcs);
    }

    public Noeud(CarteVille laVille, ArrayList<Noeud> lesFils, ArrayList<Integer> lesArcs) {
        this.laVille = laVille;
        this.lesVilles = new ArrayList<Noeud>();
        this.lesCartesRails = new ArrayList<ArrayList<Integer>>();
        this.lesVilles.addAll(lesFils);
        this.lesCartesRails.add(lesArcs);
    }

    //ACCESSEUR
    public boolean isFeuille() {
        return (lesVilles.size() == 0);
    }

    public int getSizeFils() {
        return this.lesVilles.size();
    }
    public int getSizeRail(int i){
        return this.lesCartesRails.get(i).size();
    }
    public int getIdRail(int fils, int index){
        return this.lesCartesRails.get(fils).get(index);
    }

    public Noeud getFils(int index) {
        return this.lesVilles.get(index);
    }

    public String getNameVille() {
        return this.laVille.getName();
    }

    public void addAllNoeud(ArrayList<Noeud> lesFils) {
        this.lesVilles.addAll(lesFils);
    }
    public void addNoeud(Noeud fils) {
        this.lesVilles.add(fils);
    }
    public void addCheminRail(ArrayList<Integer> lesCartesRail){
        this.lesCartesRails.add(lesCartesRail);
    }

    public CarteVille getVille() {
        return this.laVille;
    }
    
    public ArrayList<Integer> getFilsRails(int i){
        return this.lesCartesRails.get(i);
    }
    
    public boolean containsIdCarteRail(int id){

        int i = 0;
        while (i < lesCartesRails.size() && lesCartesRails.get(i).contains(id) == false) {
            i++;
        }
        if (i < lesCartesRails.size() && lesCartesRails.get(i).contains(id)) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean isEqualVille(CarteVille carte){
        return (this.laVille == carte);
    }
}
