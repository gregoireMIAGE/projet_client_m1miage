/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.Arbre;

import java.util.ArrayList;
import javafx.scene.paint.Color;
import projet_java_gl_reseau.Jeu.Carte;
import projet_java_gl_reseau.Jeu.Chemin;
import projet_java_gl_reseau.Jeu.Marchandise;
import projet_java_gl_reseau.Jeu.lesCarte.CarteVille;

/**
 *
 * @author greg
 */
public class Arbre {

    //ATTRIBUT
    private Noeud racine;
    private Noeud enCours;

    private ArrayList<ArrayList<Noeud>> lesNoeudRecherche;
    private ArrayList<Chemin> cheminSupprimer;

    //CONSTRUCTEUR
    private Arbre(Noeud racine) {
        this.racine = racine;
    }

    public Arbre() {
        //
    }

    //ACCESSEUR
    //METHODE
    //permet d'afficher l'integralité de l'arbre
    public void afficheArbre() {
        afficheArbreRecursif(this.racine);
    }

    public void afficheArbreRecursif(Noeud encours) {
        System.out.println(encours.getNameVille());
        if (encours.isFeuille() == false) {
            for (int i = 0; i < encours.getSizeFils(); i++) {
                afficheArbreRecursif(encours.getFils(i));
            }
        }
    }

    //pemret de construire un arbre
    public void constructArbre(ArrayList<Chemin> cheminFini, CarteVille dep) {
        this.racine = new Noeud(dep);
        constructArbreRecursif(cheminFini, new ArrayList<Chemin>(), this.racine);
    }

    private void constructArbreRecursif(ArrayList<Chemin> cheminFini, ArrayList<Chemin> cheminParcourue, Noeud enCours) {
        //on recupere tous les chemin de la ville
        //on recupere les chemin de la ville de depart
        ArrayList<Chemin> lesCheminVilleDep = this.getCheminVilleDep(cheminFini, enCours.getVille());
        if (lesCheminVilleDep.size() > 0) {
            for (int i = 0; i < lesCheminVilleDep.size(); i++) {
                if (cheminParcourue.contains(lesCheminVilleDep.get(i)) == false) {
                    enCours.addCheminRail(lesCheminVilleDep.get(i).getIdCarteRail());
                    cheminParcourue.add(lesCheminVilleDep.get(i));
                    enCours.addNoeud(new Noeud(lesCheminVilleDep.get(i).getVilleArr()));
                }
            }
            for (int i = 0; i < enCours.getSizeFils(); i++) {
                //je crer une nouvelle liste pour ne pas ajouter les chein parcourue par les enfant qui peuvent perturber la construction de l'arbre
                ArrayList<Chemin> cheminParcourueFils = new ArrayList<Chemin>(cheminParcourue);
                constructArbreRecursif(cheminFini, cheminParcourueFils, enCours.getFils(i));
            }
        }
    }

    //permet de trier le chemin de façon a se que la ville de depart soit en preième
    private void sortChemin(Chemin cheminFini, CarteVille carte) {
        //si la ville est la derniere on retorune l'arrayList
        if (cheminFini.getVilleArr() == carte) {
            cheminFini.reverse();
        }
    }

    //permet de retourner la liste des chemin ou al ville est présente
    private ArrayList<Chemin> getCheminVilleDep(ArrayList<Chemin> cheminFini, CarteVille carte) {
        ArrayList<Chemin> lesCheminsVille = new ArrayList<>();
        for (int i = 0; i < cheminFini.size(); i++) {
            if (cheminFini.get(i).getVilleArr() == carte || cheminFini.get(i).getVilleDep() == carte) {
                sortChemin(cheminFini.get(i), carte);
                lesCheminsVille.add(cheminFini.get(i));
            }
        }
        return lesCheminsVille;
    }

    private ArrayList<Chemin> getCheminVilleArr(ArrayList<Chemin> cheminFini, CarteVille carte) {
        ArrayList<Chemin> lesCheminsVille = new ArrayList<Chemin>();
        for (int i = 0; i < cheminFini.size(); i++) {
            if (cheminFini.get(i).getVilleDep() == carte || cheminFini.get(i).getVilleDep() == carte) {
                sortChemin(cheminFini.get(i), carte);
                lesCheminsVille.add(cheminFini.get(i));
            }
        }
        return lesCheminsVille;
    }

    //pemret de recuperer le chemin a parcourir aller justqu'a la case correcte
    public ArrayList<ArrayList<Noeud>> getParcourtCartes(Color color) {
        this.lesNoeudRecherche = new ArrayList<ArrayList<Noeud>>();
        this.lesNoeudRecherche.clear();
        recursifParcourtCartes(color, this.racine, new ArrayList<Noeud>());
        return lesNoeudRecherche;
    }

    private void recursifParcourtCartes(Color color, Noeud enCours, ArrayList<Noeud> parcourt) {
        if (enCours.getVille().getColor().equals(color)) {//si il sont de la même couleur, on ajoute le noeud au parcours realisé
            parcourt.add(enCours);
            this.lesNoeudRecherche.add(parcourt);
        } //si des fils alors on va rechercher si il y a la ville qui nous intresse
        else if (enCours.getSizeFils() > 0) {
            ArrayList<Noeud> parcourtEnCours = new ArrayList<Noeud>(parcourt);
            parcourtEnCours.add(enCours);//ajout de ce noeud dans le parcours en cas de trouvaille
            for (int i = 0; i < enCours.getSizeFils(); i++) {
                recursifParcourtCartes(color, enCours.getFils(i), parcourtEnCours);
            }
        }
    }

    //pemret de recuperer le chemin a parcourir aller justqu'a la case correcte
    public Noeud getParcourtCartes2(Color color) {
        Noeud chemins = new Noeud(this.racine.getVille());
        recursifParcourtCartes2(color, this.racine, chemins);
        return chemins;
    }

    private boolean recursifParcourtCartes2(Color color, Noeud enCours, Noeud chemins) {
        if (enCours.isFeuille() && (enCours.getVille().getColor().equals(color) || color.equals(Color.BLACK))) {//si il sont de la même couleur, on ajoute le noeud au parcours realisé. ou si marchandise noir, peuc aller partout
            return true;
        } //si des fils alors on va rechercher si il y a la ville qui nous intresse
        else if (enCours.getSizeFils() > 0) {
            boolean value = false;
            for (int i = 0; i < enCours.getSizeFils(); i++) {
                Noeud cheminParcours = new Noeud(enCours.getFils(i).getVille());
                if (recursifParcourtCartes2(color, enCours.getFils(i), cheminParcours)) {
                    chemins.addNoeud(cheminParcours);//on ajoute la ville
                    chemins.addCheminRail(enCours.getFilsRails(i));//on joute les rail
                    value = true;
                }
            }
            return value;
        } else {
            return false;
        }
    }

    //permet de savoir si cette arbre correspond a la ville
    public boolean isEqualVille(CarteVille carte) {
        return (this.racine.getVille() == carte);
    }
}
