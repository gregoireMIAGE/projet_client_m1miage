/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.controler;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import projet_java_gl_reseau.Commande.Commande;
import projet_java_gl_reseau.reseaux.Reseau;

/**
 *
 * @author greg
 */
public class LireInfoServeur implements Runnable {

    private boolean cont;

    public LireInfoServeur() {
        this.cont = true;
    }

    @Override
    public void run() {
        Scanner sc = new Scanner(System.in);
        String val;
        //pemret de lire les commandes serveur
        while (this.cont) {
            String message = "";
            try {
                //recupere le message
                message = Reseau.getCommandeServeur();
                afficheMessageReceive(message);
                //on execute le message
                Commande.execute(message);
            } catch (IOException ex) {
                Logger.getLogger(LireInfoServeur.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        /*while (this.cont) {
         val = sc.nextLine();
         //on execute le message
         Commande.execute(val);
         }*/
    }

    public void stop() {
        this.cont = false;
    }

    public static void afficheMessageReceive(String message) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd : k:m:s:S");
        Date jour = new Date();
        System.out.println("[RECU][INFO " + dateFormat.format(jour) + "] : " + message);
    }
}
