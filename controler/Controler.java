/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.controler;

import java.util.ArrayList;
import java.util.LinkedList;
import javafx.scene.Group;
import projet_java_gl_reseau.Jeu.Partie;
import projet_java_gl_reseau.player.Joueur;
import projet_java_gl_reseau.reseaux.Reseau;

/**
 *
 * @author greg
 */
public class Controler {

    //ATTRIBUT

    private static Partie partie;
    private static Thread threadLireInfoServeur;
    private static LireInfoServeur lireInfoServeur;
    private static Group root;

    //CONSTRUCTEUR

    public static void init(Partie partiepar, Group rootpar) {
        partie = partiepar;
        root = rootpar;
        lireInfoServeur = new LireInfoServeur();
        threadLireInfoServeur = new Thread(lireInfoServeur);
        threadLireInfoServeur.setDaemon(true);
    }

    //METHODE
    //pemret d'executer le controle de lecture des info serveur
    public static void start() {
        threadLireInfoServeur.start();
    }

    //pemret de finir le controle de lecture des info serveur

    public static void finish() {
        lireInfoServeur.stop();
    }

    //Methode statique permettant d'envoyer des messgae au serveur
    public static void sendMessageServeur(String message) {
        //permet d'envoyer le message au serveur distant
        Reseau.sendMessage(message);
    }

    //permet d'ajouter un joueur a la partie
    public static void addJoueurPartie(ArrayList<Joueur> lesJoueur) {
        partie.addJoueurAll(lesJoueur);
    }

    //permet e retourner la liste des joueurs et leurs caractéristique

    public static LinkedList<Joueur> getJoueurs() {
        return partie.getLesJoueurs();
    }

    //permet de retrouner la liste des joueurs
    public static LinkedList<Joueur> getListAdversaire() {
        return partie.getLesAdversaire();
    }
    public static LinkedList<Joueur> getListJoueur() {
        return partie.getLesJoueurs();
    }
    
    //permet de recuperer un joueur de la partie
    public static Joueur getIdJoueur(String name){
                int i = 0;
        while (i < partie.getLesJoueurs().size() && partie.getLesJoueurs().get(i).isEqualJoueur(name) == false) {
            i++;
        }
        if (i < partie.getLesJoueurs().size() && partie.getLesJoueurs().get(i).isEqualJoueur(name)) {
            return partie.getLesJoueurs().get(i);
        } else {
            return null;
        }
    }
}
