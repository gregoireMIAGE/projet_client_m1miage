/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Information;

import javafx.scene.Group;
import javafx.scene.Scene;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.ControlerPlateau;

/**
 *
 * @author greg
 */
public class ControlerInformation {

    private static InterfaceInformation interfaceInformation;
    private static String courantCarteTuille; //permet de stocket la carte tuile action en attendant

    public static void initControlerInformation(Scene scene, Group root) {
        interfaceInformation = new InterfaceInformation();
        dessinFenetreInfo(scene);
        if (courantCarteTuille != null) {
            interfaceInformation.setCarte(ControlerPlateau.getCarteTuile(courantCarteTuille));
        }
        root.getChildren().add(interfaceInformation);
    }

    public static void dessinFenetreInfo(Scene scene) {
        interfaceInformation.initInterfaceInformation(scene.getWidth(), scene.getHeight());
    }

    public static void setTextInfo(String text) {
        if(interfaceInformation!=null){
            interfaceInformation.setText(text);
        }
    }

    public static void showInfo() {
        interfaceInformation.show();
    }

    public static void hideInfo() {
        interfaceInformation.hide();
    }

    public static void hideInfo(int milis) {
        interfaceInformation.hide(milis);
    }

    public static void setTuileAction(String tuile) {
        courantCarteTuille = tuile;
        if (interfaceInformation != null) {
            interfaceInformation.setCarte(ControlerPlateau.getCarteTuile(tuile));
        }
    }

}
