/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Information;

import javafx.scene.Group;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Border;
import javafx.scene.layout.VBox;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Window;
import javax.naming.Context;

/**
 *
 * @author greg
 */
public class InterfaceInformation extends Group {

    private Label labInformation;
    private Rectangle rectangleInformation;
    private Rectangle rectanglePause;
    private TextArea textbox;

    private ImageView carte;

    private Group info;

    public void initInterfaceInformation(double width, double height) {

        info = new Group();

        rectanglePause = new Rectangle(0, 0, width, height);
        rectanglePause.setTranslateX(0);
        rectanglePause.setTranslateY(0);
        rectanglePause.setFill(Color.GREY);
        rectanglePause.setOpacity(0.5);
        rectanglePause.setVisible(false);

        Group InfoView = new Group();
        rectangleInformation = new Rectangle(0, 0, 200, 400);
        //rectangleInformation.setTranslateX(scene.getWidth()/2 - rectangleInformation.getWidth()/2);
        //rectangleInformation.setTranslateY(scene.getHeight()/2 - rectangleInformation.getHeight()/2);
        //rectangleInformation.setArcHeight(20);
        //rectangleInformation.setArcWidth(20);
        rectangleInformation.setFill(Color.BLACK);
        rectangleInformation.setVisible(true);
        InfoView.getChildren().add(rectangleInformation);

        textbox = new TextArea();
        textbox.setPrefRowCount(10);
        textbox.setPrefColumnCount(100);
        textbox.setPrefWidth(rectangleInformation.getWidth());
        textbox.setPrefHeight(rectangleInformation.getHeight());
        textbox.setFont(Font.font(10));
        textbox.setOpacity(1);
        textbox.setBorder(Border.EMPTY);
        textbox.setEditable(false);
        textbox.setStyle("-fx-text-fill: black;\n");

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(textbox);
        scrollPane.setFitToWidth(true);
        scrollPane.setPrefWidth(rectangleInformation.getWidth());
        scrollPane.setPrefHeight(rectangleInformation.getHeight());

        VBox vBox = VBoxBuilder.create()
                .children(scrollPane)
                .build();

        //InfoView.getChildren().add(vBox);
        InfoView.getChildren().add(textbox);

        carte = new ImageView();
        carte.setFitWidth(100);
        carte.setFitHeight(100);
        carte.setTranslateX(rectangleInformation.getWidth() - carte.getFitWidth() - 10);
        carte.setTranslateY(rectangleInformation.getHeight() / 2 - carte.getFitHeight() / 2);
        carte.setVisible(true);
        InfoView.getChildren().add(carte);

        labInformation = new Label();
        labInformation.setText("initialisation ...");
        labInformation.setMinHeight(20);
        labInformation.setMinWidth(200);
        labInformation.setFont(Font.font(20));
        final String cssDefaultLabel = "-fx-text-alignment: center;\n"
                + "-fx-text-fill: white;\n";
        labInformation.setStyle(cssDefaultLabel);
        labInformation.setTranslateX(0);
        labInformation.setTranslateY(rectangleInformation.getHeight() / 2 - labInformation.getMinHeight() / 2);
        labInformation.setVisible(true);
        //InfoView.getChildren().add(labInformation);

        //InfoView.setTranslateX(width / 2 - rectangleInformation.getWidth() / 2);
        InfoView.setTranslateX(0);
        InfoView.setTranslateY(height / 2 - rectangleInformation.getHeight() / 2);

        info.getChildren().add(InfoView);
        info.getChildren().add(rectanglePause);
        
        this.getChildren().add(info);
        //this.setVisible(false);
    }

    public void show() {
        rectanglePause.setVisible(true);
        info.setVisible(true);
    }

    public void hide(int milis) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                info.setVisible(true);
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        }).start();

    }

    public void hide() {
        //info.setVisible(false);
        rectanglePause.setVisible(false);
    }

    public void setText(String text) {
        //this.labInformation.setText(text);
        this.textbox.appendText("\n" + text);
    }

    public void setCarte(String carteP) {
        //Image image = new Image("file:" + System.getProperty("user.dir") + carteP);
        //this.carte.setImage(image);
    }
}
