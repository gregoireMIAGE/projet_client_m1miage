/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique;

import javafx.scene.Group;
import javafx.scene.image.ImageView;

/**
 *
 * @author greg
 */
public class ControlerInterfaceGraphique {

    private static Group wait;
    private static Group connection;
    private static Group plateau;
    private static Group menu;

    public static void initControlerInterfaceGraphique(Group root) {
        wait = new Group();
        connection = new Group();
        plateau = new Group();
        menu = new Group();

        wait.setVisible(false);
        connection.setVisible(false);
        plateau.setVisible(false);
        menu.setVisible(false);

        root.getChildren().add(wait);
        root.getChildren().add(connection);
        root.getChildren().add(plateau);
        root.getChildren().add(menu);
    }

    public static void afficheMenu() {
        menu.setVisible(true);
        wait.setVisible(false);
        connection.setVisible(false);
        plateau.setVisible(false);
    }

    public static void afficheConnexion() {
        connection.setVisible(true);
        wait.setVisible(false);
        plateau.setVisible(false);
        menu.setVisible(false);
    }

    public static void affichePlateau() {
        plateau.setVisible(true);
        wait.setVisible(false);
        connection.setVisible(false);
        menu.setVisible(false);
    }

    public static void afficheWait() {
        wait.setVisible(true);
        connection.setVisible(false);
        plateau.setVisible(false);
        menu.setVisible(false);
    }
    
    public static Group getRootMenu(){
        return menu;
    }
    public static Group getRootWait(){
        return wait;
    }
    public static Group getRootPlateau(){
        return plateau;
    }
    public static Group getRootConnexion(){
        return connection;
    }
    
    public static void effactePlateau(){
        plateau.getChildren().clear();
    }
}
