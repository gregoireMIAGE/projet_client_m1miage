/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Plateau.InformationJoueur.event;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Rectangle;
import projet_java_gl_reseau.Commande.CommandeAfficheInfo;
import projet_java_gl_reseau.InterfaceGraphique.Information.ControlerInformation;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.ControlerPlateau;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.InformationJoueur.ControlerPlateauInfoJoueur;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.map.ControlerPlateauMap;

/**
 *
 * @author greg
 */
public class EventPressedFinTour extends ControlerPlateauInfoJoueur implements EventHandler<MouseEvent> {

    @Override
    public void handle(MouseEvent event) {
        if (test) {
             if(phase2){
                 projet_java_gl_reseau.Projet_java_gl_reseau.testPhase3();
             }else if(phase3){
                 ControlerPlateauMap.deplaceLesMarchandises();
                 projet_java_gl_reseau.Projet_java_gl_reseau.testPhase4();
             }
        } else if (!pause) {
            if (phase3) {
                //on deplace les marchandise
                ControlerPlateauMap.deplaceLesMarchandises();
            }
            fini();
        }
    }

}
