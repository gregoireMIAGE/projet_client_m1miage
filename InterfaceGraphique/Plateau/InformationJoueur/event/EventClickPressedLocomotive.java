/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Plateau.InformationJoueur.event;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.InformationJoueur.ControlerPlateauInfoJoueur;

/**
 *
 * @author greg
 */
public class EventClickPressedLocomotive extends ControlerPlateauInfoJoueur implements EventHandler<MouseEvent> {

    @Override
    public void handle(MouseEvent event) {
        if (!pause && phase3) {
            addLevelLocomotive();
        //envoyer la mise a jour de la loco
            //...
            //mettre phase3 a false pour le plus faire d'action et finir le tour
            phase3 = false;
        }
    }

}
