/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Plateau.InformationJoueur;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import javafx.scene.Group;
import projet_java_gl_reseau.InterfaceGraphique.Connexion.InterfaceConnexion;
import projet_java_gl_reseau.InterfaceGraphique.Information.ControlerInformation;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.ControlerPlateau;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.InformationJoueur.event.EventClickPressedLocomotive;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.InformationJoueur.event.EventClickShowPlayer;
import projet_java_gl_reseau.controler.Controler;
import projet_java_gl_reseau.player.Joueur;
import projet_java_gl_reseau.reseaux.Reseau;

/**
 *
 * @author greg
 */
public class ControlerPlateauInfoJoueur extends ControlerPlateau {

    private static InterfacePlateauInfoJoueur interfacePlateauInfoJoueur;
    private static EventClickPressedLocomotive eventClickShowPlayer;

    public static void initControlerPlateauInfoJoueur(double widthFenetre, double heightFenetre) {

        interfacePlateauInfoJoueur = new InterfacePlateauInfoJoueur(widthFenetre, heightFenetre);
        eventClickShowPlayer = new EventClickPressedLocomotive();
    }

    //permet de dessiner les information de la bar d'inofrmation
    //permet de dessiner les premieres info sur les joueurs
    public static Group dessinNavBarInfo() {
        interfacePlateauInfoJoueur.dessineInfoBar(eventClickShowPlayer);
        interfacePlateauInfoJoueur.setLevelLocomotive(unJoueur.getNivLocomotive());//permet d'initialiser la locomotive du joueur
        return interfacePlateauInfoJoueur;
    }

    //permet de mettre a jour les infor des jouerus
    public static void dessinInfoJoueur(LinkedList<Joueur> lesJoueurs) {
        interfacePlateauInfoJoueur.dessineJoueur(lesJoueurs, unJoueur);
    }
    
    //permet de mettre a jour les infor des jouerus
    public static void miseAjourInfoJoueur() {
        interfacePlateauInfoJoueur.dessineJoueur(Controler.getListAdversaire());

        interfacePlateauInfoJoueur.setSolde(String.valueOf(unJoueur.getSolde()));
        interfacePlateauInfoJoueur.setLevelLocomotive(unJoueur.getNivLocomotive());
        interfacePlateauInfoJoueur.setPoint(String.valueOf(unJoueur.getPointVictoire()));
    }

    public static void miseAjourSold(String solde) {
        interfacePlateauInfoJoueur.setSolde(solde);
    }

    public static void miseNbPoint(String nbPoint) {
        interfacePlateauInfoJoueur.setPoint(nbPoint);
    }

    public static void miseOrdreTour(String ordreTour) {
        interfacePlateauInfoJoueur.setSolde(ordreTour);
    }

    public static void addLevelLocomotive() {
        if (interfacePlateauInfoJoueur.getLevelLocomotive() < 6) {
            interfacePlateauInfoJoueur.addLevelLocomotive();
            //permet de mettre a jour le niveau de al locomotive du joueur
            ControlerPlateau.unJoueur.addNivLocomotive();
            //permet d'envoyé le niveau de la locomotive
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Reseau.sendMessage("AddLevelLocomotive:"+String.valueOf(unJoueur.getNivLocomotive()));
                }
            });
        }
    }

    public static void addTuileAction(String carte) {
        interfacePlateauInfoJoueur.addTuileACtion(ControlerPlateau.getCarteTuile(carte));
        ControlerInformation.setTextInfo("votre tuile action : "+carte);
    }
    public static boolean getPause() {
        return pause;
    }

    public static void fini() {
        if (!test) {
            ControlerPlateau.fini();
        }
        //phase de test 
        if (phase2 == true) {
            phase3();
        } else if (phase3 == true) {
            phase2();
        }
    }
}
