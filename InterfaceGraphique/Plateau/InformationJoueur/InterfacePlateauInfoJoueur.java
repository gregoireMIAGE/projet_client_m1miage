/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Plateau.InformationJoueur;

import java.util.ArrayList;
import java.util.LinkedList;
import javafx.animation.TranslateTransition;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Border;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.InformationJoueur.event.EventClickPressedLocomotive;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.InformationJoueur.event.EventClickShowPlayer;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.InformationJoueur.event.EventPressedFinTour;
import projet_java_gl_reseau.player.Joueur;

/**
 *
 * @author greg
 */
public class InterfacePlateauInfoJoueur extends Group {

    private double widthFenetre;
    private double heightFenetre;

    //rectangle d'inofmration nav bar
    private Rectangle infoBar;

    //buton permettant de finir le tour
    private Button fini;

    //permet de dessiner le texte du nombre de point
    private Text nbPoint;
    private Text solde;
    private Text OrdreTour;

    private Text levelLocomotive;
    private ImageView tuileAction;

    //groupe de la bar
    private Group infoJoueurs;

    public InterfacePlateauInfoJoueur(double widthFenetre, double heightFenetre) {
        this.widthFenetre = widthFenetre;
        this.heightFenetre = heightFenetre;
        this.infoJoueurs = new Group();
    }

    //dessin de la bar
    public void dessineInfoBar(EventClickPressedLocomotive eventClickShowPlayer) {
        //bar du bas d'info
        this.infoBar = new Rectangle();
        this.infoBar.setWidth(this.widthFenetre);
        this.infoBar.setHeight(100);
        this.infoBar.setTranslateX(0);
        this.infoBar.setTranslateY(this.heightFenetre - this.infoBar.getHeight());
        this.infoBar.setFill(Color.color(0, 0, 0));
        //this.infoBar.setOpacity(0.8);
        this.infoBar.setStrokeWidth(5);
        this.infoBar.setArcHeight(10);
        this.infoBar.setArcWidth(10);
        this.getChildren().add(this.infoBar);

        //ajout du bouton sur la bar
        this.fini = new Button("Fin de tour");
        this.fini.setBorder(Border.EMPTY);
        this.fini.setMinSize(20, 20);
        this.fini.setMinWidth(200);
        this.fini.setMinHeight(40);
        this.fini.setTranslateX(this.widthFenetre - this.fini.getMinWidth());
        this.fini.setTranslateY(this.heightFenetre - this.fini.getMinHeight() * 2);
        final String cssDefault = "-fx-background-color: #AA3939;\n"
                + "-fx-text-alignment: center;\n"
                + "-fx-text-fill: white;\n";
        this.fini.setStyle(cssDefault);
        this.fini.setVisible(true);
        this.fini.setOnMousePressed(new EventPressedFinTour());
        this.getChildren().add(this.fini);

        //groupe info le joueur
        Group infoJoueur = new Group();
        //dessin le joueur
        Image image = new Image("file:" + System.getProperty("user.dir") + "/src/projet_java_gl_reseau/config/images/partie_plateau_infopartie.png");
        ImageView joueur = new ImageView(image);
        joueur.setFitWidth(333);
        joueur.setFitHeight(158);
        infoJoueur.getChildren().add(joueur);

        this.nbPoint = new Text("0");
        this.nbPoint.setTranslateX(joueur.getFitWidth() * ((double) 2 / 3));
        this.nbPoint.setTranslateY(joueur.getFitHeight() * ((double) 3 / 4));
        this.nbPoint.setTextAlignment(TextAlignment.CENTER);
        this.nbPoint.setFill(Color.BLACK);
        this.nbPoint.setFont(Font.font(20));
        infoJoueur.getChildren().add(nbPoint);
        this.solde = new Text("0");
        this.solde.setTranslateX(joueur.getFitWidth() * ((double) 10 / 12));
        this.solde.setTranslateY(joueur.getFitHeight() * ((double) 11 / 12));
        this.solde.setTextAlignment(TextAlignment.CENTER);
        this.solde.setFill(Color.BLACK);
        this.solde.setFont(Font.font(20));
        infoJoueur.getChildren().add(solde);

        infoJoueur.setTranslateX(0);
        infoJoueur.setTranslateY(heightFenetre - joueur.getFitHeight());

        this.getChildren().add(infoJoueur);
        this.infoJoueurs.setTranslateX(joueur.getFitWidth() + 10);
        this.getChildren().add(this.infoJoueurs);

        //dessin de la loco
        Image imageLoco = new Image("file:" + System.getProperty("user.dir") + "/src/projet_java_gl_reseau/config/images/locomotive.png");
        ImageView imgLoco = new ImageView(imageLoco);
        imgLoco.setFitWidth(178.1);
        imgLoco.setFitHeight(214.2);
        imgLoco.setTranslateX((this.fini.getTranslateX()) - (imgLoco.getFitWidth() + 40));
        imgLoco.setTranslateY((this.fini.getTranslateY() - imgLoco.getFitHeight() / 2));
        imgLoco.setOnMousePressed(eventClickShowPlayer);
        //imgLoco.setTranslateX(0);
        //imgLoco.setTranslateY(0);
        this.levelLocomotive = new Text("0");
        this.levelLocomotive.setFont(Font.font(36));
        this.levelLocomotive.setTranslateX(imgLoco.getTranslateX() + imgLoco.getFitWidth() / 2 - 30);
        this.levelLocomotive.setTranslateY(imgLoco.getTranslateY() + imgLoco.getFitHeight() * (double) 3 / 4);
        this.levelLocomotive.setFill(Color.BLACK);

        //dessin de la tuile action
        this.tuileAction = new ImageView();
        this.tuileAction.setFitWidth(150);
        this.tuileAction.setFitHeight(200);
        this.tuileAction.setTranslateX((this.fini.getTranslateX()) - (imgLoco.getFitWidth() + 40) - (tuileAction.getFitWidth()));
        this.tuileAction.setTranslateY((this.fini.getTranslateY() - tuileAction.getFitHeight() / 2));

        this.getChildren().add(imgLoco);
        this.getChildren().add(tuileAction);
        this.getChildren().add(this.levelLocomotive);
    }

    //permet de dessiner les joueur et leur information
    public void dessineJoueur(LinkedList<Joueur> lesJoueur, Joueur unJoueur) {
        this.infoJoueurs.setTranslateY(heightFenetre - 97);
        int compt = 0;
        for (int i = 0; i < lesJoueur.size(); i++) {
            if (unJoueur.isEqualJoueur(lesJoueur.get(i)) == false) {
                //rectangle pour le font 
                Rectangle fond = new Rectangle();
                fond.setArcHeight(20);
                fond.setArcWidth(20);
                fond.setFill(lesJoueur.get(i).getCouleur());
                fond.setWidth(136);
                fond.setHeight(59);
                fond.setTranslateX((161 * compt + compt * 40) + 15);
                fond.setTranslateY(17);
                infoJoueurs.getChildren().add(fond);

                Image image = new Image("file:" + System.getProperty("user.dir") + "/src/projet_java_gl_reseau/config/images/partie_plateau_infopartie_joueur.png");
                ImageView joueur = new ImageView(image);
                joueur.setFitWidth(161);
                joueur.setFitHeight(97);
                joueur.setTranslateX((161 * compt + compt * 40));
                infoJoueurs.getChildren().add(joueur);

                Text nbPointLocal = new Text("0");
                nbPointLocal.setTranslateX(joueur.getFitWidth() * ((double) 1 / 2) + joueur.getTranslateX());
                nbPointLocal.setTranslateY(joueur.getFitHeight() * ((double) 3 / 4));
                nbPointLocal.setTextAlignment(TextAlignment.CENTER);
                nbPointLocal.setFill(Color.BLACK);
                nbPointLocal.setFont(Font.font(10));
                infoJoueurs.getChildren().add(nbPointLocal);

                Text soldeLocal = new Text("0");
                soldeLocal.setTranslateX(joueur.getFitWidth() * ((double) 10 / 12) + joueur.getTranslateX());
                soldeLocal.setTranslateY(joueur.getFitHeight() * ((double) 9 / 12));
                soldeLocal.setTextAlignment(TextAlignment.CENTER);
                soldeLocal.setFill(Color.BLACK);
                soldeLocal.setFont(Font.font(10));
                infoJoueurs.getChildren().add(soldeLocal);

                //pemermer de compter le nombre de joueur afficher pour le placement des images
                compt++;
            }
        }

    }

    //permet de dessiner les joueur et leur information
    public void dessineJoueur(LinkedList<Joueur> lesJoueur) {
        this.infoJoueurs.setTranslateY(heightFenetre - 97);
        int compt = 0;
        for (int i = 0; i < lesJoueur.size(); i++) {
                //rectangle pour le font 
                Rectangle fond = new Rectangle();
                fond.setArcHeight(20);
                fond.setArcWidth(20);
                fond.setFill(lesJoueur.get(i).getCouleur());
                fond.setWidth(136);
                fond.setHeight(59);
                fond.setTranslateX((161 * compt + compt * 40) + 15);
                fond.setTranslateY(17);
                infoJoueurs.getChildren().add(fond);

                Image image = new Image("file:" + System.getProperty("user.dir") + "/src/projet_java_gl_reseau/config/images/partie_plateau_infopartie_joueur.png");
                ImageView joueur = new ImageView(image);
                joueur.setFitWidth(161);
                joueur.setFitHeight(97);
                joueur.setTranslateX((161 * compt + compt * 40));
                infoJoueurs.getChildren().add(joueur);

                Text nbPointLocal = new Text(String.valueOf(lesJoueur.get(i).getPointVictoire()));
                nbPointLocal.setTranslateX(joueur.getFitWidth() * ((double) 1 / 2) + joueur.getTranslateX());
                nbPointLocal.setTranslateY(joueur.getFitHeight() * ((double) 3 / 4));
                nbPointLocal.setTextAlignment(TextAlignment.CENTER);
                nbPointLocal.setFill(Color.BLACK);
                nbPointLocal.setFont(Font.font(10));
                infoJoueurs.getChildren().add(nbPointLocal);

                Text soldeLocal = new Text(String.valueOf(lesJoueur.get(i).getNivLocomotive()));
                soldeLocal.setTranslateX(joueur.getFitWidth() * ((double) 10 / 12) + joueur.getTranslateX());
                soldeLocal.setTranslateY(joueur.getFitHeight() * ((double) 9 / 12));
                soldeLocal.setTextAlignment(TextAlignment.CENTER);
                soldeLocal.setFill(Color.BLACK);
                soldeLocal.setFont(Font.font(10));
                infoJoueurs.getChildren().add(soldeLocal);

                //pemermer de compter le nombre de joueur afficher pour le placement des images
                compt++;
        }

    }

    //dessin des point du joeuru
    public void dessinePointJoueur() {
        //dessine les poitn du joueur; initaliement a 0
    }

    //dessins du solde du joueur
    public void dessineSoldeJoueur() {

    }

    public void setPoint(String point) {
        this.nbPoint.setText(point);
    }

    public void setSolde(String solde) {
        this.solde.setText(solde);
    }

    public void setOrdreTour(String ordre) {
        this.OrdreTour.setText(ordre);
    }

    public int getLevelLocomotive() {
        return Integer.parseInt(this.levelLocomotive.getText());
    }

    public void addLevelLocomotive() {
        this.levelLocomotive.setText(String.valueOf(Integer.parseInt(this.levelLocomotive.getText()) + 1));
    }
    public void setLevelLocomotive(int i) {
        this.levelLocomotive.setText(String.valueOf(i));
    }

    //permet d'ajouter la tuile action a afficher dans la bar de navigation
    public void addTuileACtion(String image) {
        this.tuileAction.setImage(new Image("file:" + System.getProperty("user.dir") + image));
    }
    
    //permet de mettre a jour les information du joueur
    public void miseJourJoueur(int point, int locomotive, int solde){
        
    }
}
