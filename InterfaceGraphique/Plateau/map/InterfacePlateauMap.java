/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Plateau.map;

import java.util.ArrayList;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import projet_java_gl_reseau.Arbre.Noeud;
import static projet_java_gl_reseau.InterfaceGraphique.Plateau.map.ControlerPlateauMap.imgPressedCroissance;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.map.event.EventPressedCroissance;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.map.event.EventPressedVille;
import projet_java_gl_reseau.Jeu.Chemin;
import projet_java_gl_reseau.Jeu.Marchandise;
import projet_java_gl_reseau.Jeu.lesCarte.CarteMap;
import projet_java_gl_reseau.Jeu.lesCarte.CarteVille;

/**
 *
 * @author greg
 */
public class InterfacePlateauMap extends Group {

    private final double widthFenetre;
    private final double heightFenetre;

    private final Group lesCartes;//toute les cartes affiché et référencé sur le plateau
    private final Group lesMarchandise;//toute les marchandise affiché et réferencé sur le plateau

    public InterfacePlateauMap(double widthFenetre, double heightFenetre) {
        this.widthFenetre = widthFenetre;
        this.heightFenetre = heightFenetre;

        this.lesCartes = new Group();
        this.getChildren().add(this.lesCartes);
        this.lesMarchandise = new Group();
        this.getChildren().add(this.lesMarchandise);
    }

    //ACCESSEUR
    //METHODE
    //dessin du plateau
    public void dessinMap(String[][] map) {
        for (int i = 0; i < map.length; i++) {
            for (int a = 0; a < map[i].length; a++) {
                if (map[i][a].equalsIgnoreCase("0") == false) {
                    int construct = 0;
                    CarteVille carte;
                    ImageView imgView2 = new ImageView();
                    imgView2.setFitHeight(100);
                    imgView2.setFitWidth(100);
                    imgView2.setTranslateX(300 + 70 * a);
                    imgView2.setTranslateY(100+ 80 * i + ((a % 2) * 40));
                    carte = ControlerPlateauMap.lesCartesMapXml.get(map[i][a]);
                    imgView2.setImage(new Image(carte.getLocation()));
                    double x = imgView2.getTranslateX() + ((imgView2.getFitWidth() / 2) - (25));
                    double y = imgView2.getTranslateY() + ((imgView2.getFitHeight() / 2) - (25));
                    //si c'est une carte ville, on l'enregistre
                    if (map[i][a].equalsIgnoreCase("V") == false && map[i][a].contains("V")) {
                        CarteVille carteVille = new CarteVille(carte.getName(), carte.getLocation(), carte.getNiveau(), a, i, x, y);
                        carteVille.setColor(carte.getColor());
                        imgView2.setOnMouseEntered(ControlerPlateauMap.imgEnteredVille);//ajout de l'evenemtn pour deplacer une marchandise
                        carteVille.setImageView(imgView2);
                        imgView2.setOnMouseClicked(new EventPressedVille(carteVille));//ajout de l'èvènement pour la croissance
                        ControlerPlateauMap.addVilleInMap(carteVille);
                    } else if (map[i][a].equalsIgnoreCase("V")) {//carte city a construire
                        imgView2.setOnMouseEntered(ControlerPlateauMap.imgMapOnMouseEnteredEventHandler);
                        CarteMap carteMap = new CarteMap(carte.getName(), imgView2, a, i, x, y, 0);
                        ControlerPlateauMap.addMapInMap(carteMap);
                    } else {//si non c'est une cate ordianire
                        imgView2.setOnMouseEntered(ControlerPlateauMap.imgMapOnMouseEnteredEventHandler);
                        CarteMap carteMap = new CarteMap(carte.getName(), imgView2, a, i, x, y, 0);
                        ControlerPlateauMap.addMapInMap(carteMap);
                    }
                    this.lesCartes.getChildren().add(imgView2);
                }
            }
        }
    }

    public void dessinImagesViews() {
        ControlerPlateauMap.imgViewValidate = new ImageView(new Image("file:" + System.getProperty("user.dir") + "/src/projet_java_gl_reseau/config/images/partie_plateau_valide.png"));
        ControlerPlateauMap.imgViewValidate.setFitHeight(100);
        ControlerPlateauMap.imgViewValidate.setFitWidth(100);
        ControlerPlateauMap.imgViewValidate.setTranslateX(0);
        ControlerPlateauMap.imgViewValidate.setTranslateY(0);
        ControlerPlateauMap.imgViewValidate.setOpacity(0.6);
        ControlerPlateauMap.imgViewValidate.setOnMouseClicked(ControlerPlateauMap.imgValideEventHandler);
        ControlerPlateauMap.imgViewValidate.setVisible(false);

        //polygone valide
        ControlerPlateauMap.imgViewUnvalidate = new ImageView(new Image("file:" + System.getProperty("user.dir") + "/src/projet_java_gl_reseau/config/images/partie_plateau_unvalide.png"));
        ControlerPlateauMap.imgViewUnvalidate.setFitHeight(100);
        ControlerPlateauMap.imgViewUnvalidate.setFitWidth(100);
        ControlerPlateauMap.imgViewUnvalidate.setTranslateX(0);
        ControlerPlateauMap.imgViewUnvalidate.setTranslateY(0);
        ControlerPlateauMap.imgViewUnvalidate.setOpacity(0.6);
        ControlerPlateauMap.imgViewUnvalidate.setVisible(false);
        //add(this.imgViewUnvalidate);

        //fleche de ration des carte 
        ControlerPlateauMap.imageFlechDroite = new ImageView(new Image("file:" + System.getProperty("user.dir") + "/src/projet_java_gl_reseau/config/images/logo_fleche_droite.png"));
        ControlerPlateauMap.imageFlechDroite.setFitWidth(20);
        ControlerPlateauMap.imageFlechDroite.setFitHeight(20);
        ControlerPlateauMap.imageFlechDroite.setVisible(false);
        ControlerPlateauMap.imageFlechDroite.setCursor(Cursor.OPEN_HAND);
        ControlerPlateauMap.imageFlechDroite.setOnMouseClicked(ControlerPlateauMap.imgFlecheDroiteOnMousePressedEventHandler);

        ControlerPlateauMap.imageFlechGauche = new ImageView(new Image("file:" + System.getProperty("user.dir") + "/src/projet_java_gl_reseau/config/images/logo_fleche_Gauche.png"));
        ControlerPlateauMap.imageFlechGauche.setFitWidth(20);
        ControlerPlateauMap.imageFlechGauche.setFitHeight(20);
        ControlerPlateauMap.imageFlechGauche.setVisible(false);
        ControlerPlateauMap.imageFlechGauche.setCursor(Cursor.OPEN_HAND);
        ControlerPlateauMap.imageFlechGauche.setOnMouseClicked(ControlerPlateauMap.imgFlecheGaucheOnMouseDraggedEventHandler);

        ControlerPlateauMap.imageferme = new ImageView(new Image("file:" + System.getProperty("user.dir") + "/src/projet_java_gl_reseau/config/images/croix.png"));
        ControlerPlateauMap.imageferme.setFitWidth(20);
        ControlerPlateauMap.imageferme.setFitHeight(20);
        ControlerPlateauMap.imageferme.setVisible(false);
        ControlerPlateauMap.imageferme.setOnMouseClicked(ControlerPlateauMap.imageFermeEvantHandler);
        ControlerPlateauMap.imageferme.setCursor(Cursor.OPEN_HAND);
        
        ControlerPlateauMap.selectCube = new Rectangle(27, 27);
        ControlerPlateauMap.selectCube.setFill(Color.TRANSPARENT);
        ControlerPlateauMap.selectCube.setStroke(Color.GREEN);
        ControlerPlateauMap.selectCube.setVisible(false);
        
        ControlerPlateauMap.selectLine = new Line();
        ControlerPlateauMap.selectLine.setVisible(false);

    }

    public void dessineMarchandise(Rectangle rectangle) {
        rectangle.setOnMousePressed(ControlerPlateauMap.rectanglePressedCube);
        this.lesMarchandise.getChildren().add(rectangle);
    }

    //permet de modifier l'affichage du cube
    public void updateMarchandise(Rectangle rectangle, double x, double y) {
        rectangle.setTranslateX(x);
        rectangle.setTranslateY(y);
    }

    //pemret de supprimer une marchandise une fois livré
    public void removeMarchandise(Rectangle rectangle) {
         this.lesMarchandise.getChildren().remove(rectangle);
    }

    //pemret de dessiner le pannel de croissance quand on click sur une ville
    public Group dessinePannelCroissance(ArrayList<Marchandise> lesMarchandisesReserves, int nbCube, double x, double y, EventPressedCroissance press) {
        Group pannelCroissance = new Group();
        ImageView iconEvolytion = new ImageView(new Image("file:" + System.getProperty("user.dir") + "/src/projet_java_gl_reseau/config/images/icon_evolution.png"));
        iconEvolytion.setFitWidth(50.25);
        iconEvolytion.setFitHeight(58.5);
        pannelCroissance.getChildren().add(iconEvolytion);

        //ajoute la vue des prochain petit carré possible
        for (int i = 0; i < nbCube; i++) {

            Rectangle cube = new Rectangle(6, 6);
            cube.setStroke(Color.BLACK);
            cube.setFill(Color.RED);
            cube.setTranslateX(iconEvolytion.getFitWidth() * ((double) i / nbCube));
            cube.setTranslateY(iconEvolytion.getFitHeight() * ((double) 2 / 3));
            pannelCroissance.getChildren().add(cube);
        }

        pannelCroissance.setOnMouseClicked(press);
        pannelCroissance.setCursor(Cursor.HAND);
        pannelCroissance.setTranslateX(x - iconEvolytion.getFitWidth() / 2);
        pannelCroissance.setTranslateY(y - iconEvolytion.getFitHeight());
        pannelCroissance.setVisible(false);

        this.getChildren().add(pannelCroissance);
        return pannelCroissance;
    }
    
    //permet de dessiner le chemin d'un cube
    public void dessinCheminCube(Group cheminCube){
        //pour tous les chemins possible, on affche la selection
       this.getChildren().add(cheminCube);
    }
    
    //permet d'ajouter une marchandise  a l'interface
    public void addNodeMarchandise(Node e){
        this.lesMarchandise.getChildren().add(e);
    }
    
    //permet d'ajouter une carte a l'interface
    public void addNodeCarteRail(Node e){
        this.lesCartes.getChildren().add(e);
    }
}
