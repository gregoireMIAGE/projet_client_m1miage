/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Plateau.map.event;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.map.ControlerPlateauMap;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.naviguationPiece.ControlerNavBarPiece;

/**
 *
 * @author greg
 */
public class EventPressedClose extends ControlerPlateauMap implements EventHandler<MouseEvent> {

    @Override
    public void handle(MouseEvent event) {
        if (!pause && phase2) {
            if (courantImg != null && courantRail != null) {
                if (courantRail != null) {
                    if (courantRail.getPose() == false) { //si le rail n'a pas encore etait posé
                        courantImg.setTranslateX(courantRail.getX());
                        courantImg.setTranslateY(courantRail.getY());
                        courantImg.setFitHeight(50);
                        courantImg.setFitWidth(50);
                        //mise a jou du nombre de carte
                        if (courantRail.getValidate() == true) {
                            courantRail.unvalidate();
                            ControlerNavBarPiece.updateNbCarte(courantRail.getNameRail(), "+");
                        }
                        //modification matrice
                        map[courantRail.getPositionYmap()][courantRail.getPositionXmap()] = mapDebut[courantRail.getPositionYmap()][courantRail.getPositionXmap()];
                    } else {//si le rail a deja etait posé
                        courantImg.setTranslateX(courantRail.getX());
                        courantImg.setTranslateY(courantRail.getY());
                    }
                    //envoie du message aux autre sjoueurs
                    sendMessage("rail", courantRail.getId());

                }
                //suprime la carte de l'array list
                lesRailsInMap.remove(courantRail);
                courantImg = null;
                courantRail = null;
                insertOk = false;
            } else if (courantCity != null) {
                //on replace la carte sur le coté
                courantImg.setTranslateX(courantCity.getX());
                courantImg.setTranslateY(courantCity.getY());
                courantImg.setFitHeight(50);
                courantImg.setFitWidth(50);
                insertOk = false;
                //envoie du message aux autre sjoueurs
                this.sendMessage("city", courantCity.getId());
                courantImg = null;
                courantCity = null;
            }
            imageFlechDroite.setVisible(false);
            imageFlechGauche.setVisible(false);
            imageferme.setVisible(false);
            imgViewValidate.setVisible(false);
            imgViewUnvalidate.setVisible(false);
            //imgViewSelect.setVisible(false);
        }
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    //permet d'envoyer l'action effectué aux autres joueur
    private void sendMessage(String type, int id) {
        if (!test) {
            //envoie du message au client
            new Thread(new Runnable() {
                @Override
                public void run() {
                    //message a envoyé au autre joueur
                    StringBuffer message = new StringBuffer();
                    message.append("closeCard:" + type + "," + id);
                    //envoie du message au serveur
                    sendMessage(message.toString());
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
            }).start();
        }
    }
}
