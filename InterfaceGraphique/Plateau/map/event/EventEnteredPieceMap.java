/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Plateau.map.event;

import java.util.ArrayList;
import javafx.event.EventHandler;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import static projet_java_gl_reseau.InterfaceGraphique.Plateau.ControlerPlateau.getCartePosMap;
import static projet_java_gl_reseau.InterfaceGraphique.Plateau.ControlerPlateau.getCartePosRail;
import static projet_java_gl_reseau.InterfaceGraphique.Plateau.ControlerPlateau.getCartePosVille;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.map.ControlerPlateauMap;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.naviguationPiece.ControlerNavBarPiece;
import projet_java_gl_reseau.Jeu.lesCarte.CarteRail;
import projet_java_gl_reseau.Jeu.lesCarte.CarteVille;

/**
 *
 * @author greg
 */
public class EventEnteredPieceMap extends ControlerPlateauMap implements EventHandler<MouseEvent> {

    public EventEnteredPieceMap() {
        attachCourantRails = new ArrayList<CarteRail>();
        attachCourantVilles = new ArrayList<CarteVille>();
    }

    @Override
    public void handle(MouseEvent event) {

        if (!pause && phase2) {
            StringBuffer message = new StringBuffer();
            //System.out.println("test");
            if (courantRail != null && courantRail.getPose() == false) {//si la carte n'est pas deja posé
                courantImg = courantRail.getImageViw();
                courantCarteMap = getIdCarteMap(((ImageView) event.getSource()));
                message.append("MoveCard:rail," + courantRail.getId() + "," + courantCarteMap.positionX + "," + courantCarteMap.positionY + ",100");
                courantRail.getImageViw().setTranslateX(((ImageView) event.getSource()).getTranslateX());
                courantRail.getImageViw().setTranslateY(((ImageView) event.getSource()).getTranslateY());
                if (courantRail.getImageViw().getFitWidth() == 50) {
                    courantRail.getImageViw().setFitHeight(100);
                    courantRail.getImageViw().setFitWidth(100);
                }
                //initialiser les fleches
                imageFlechDroite.setTranslateX(((ImageView) event.getSource()).getTranslateX() + 60);
                imageFlechDroite.setTranslateY(((ImageView) event.getSource()).getTranslateY() + 40);
                imageFlechGauche.setTranslateX(((ImageView) event.getSource()).getTranslateX() + 20);
                imageFlechGauche.setTranslateY(((ImageView) event.getSource()).getTranslateY() + 40);
                imageferme.setTranslateX(((ImageView) event.getSource()).getTranslateX() + 40);
                imageferme.setTranslateY(((ImageView) event.getSource()).getTranslateY());
                //permet de verifier si la carte peux être inséré
                verifValidateInsertCart();
                imageferme.setVisible(true);
                imageFlechDroite.setVisible(true);
                imageFlechGauche.setVisible(true);
                //insertOk = true;//permet de valider l'insertion 
                //si une carte city
            } else if (courantCity != null && courantCity.getPose() == false) {//si la carte 'a pas deja etait posé
                courantCarteMap = getIdCarteMap(((ImageView) event.getSource()));
                message.append("MoveCard:city," + courantCity.getId() + "," + courantCarteMap.positionX + "," + courantCarteMap.positionY + ",100");
                courantImg = courantCity.getImageViw();
                //on bouge la carte city
                courantCarteMap = getIdCarteMap(((ImageView) event.getSource()));
                courantCity.getImageViw().setTranslateX(((ImageView) event.getSource()).getTranslateX());
                courantCity.getImageViw().setTranslateY(((ImageView) event.getSource()).getTranslateY());
                imageferme.setTranslateX(((ImageView) event.getSource()).getTranslateX() + 40);
                imageferme.setTranslateY(((ImageView) event.getSource()).getTranslateY());
                imageferme.setVisible(true);
                if (courantCity.getImageViw().getFitWidth() == 50) {
                    courantCity.getImageViw().setFitHeight(100);
                    courantCity.getImageViw().setFitWidth(100);
                }
                verifValideCarteCity();
            }
            if (!test) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //envoie du message au serveur
                        sendMessage(message.toString());
                        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }
                }).start();
            }
        }
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    //permet de verifier si oui ou non nous pouvons construire une ville
    private String verifValideCarteCity() {
        if (mapDebut[courantCarteMap.positionY][courantCarteMap.positionX].equalsIgnoreCase("V")) {
            imgViewValidate.setTranslateX(courantImg.getTranslateX());
            imgViewValidate.setTranslateY(courantImg.getTranslateY());
            imgViewValidate.setVisible(true);
            imgViewUnvalidate.setVisible(false);
            return "valide";

        } else {
            imgViewUnvalidate.setTranslateX(courantImg.getTranslateX());
            imgViewUnvalidate.setTranslateY(courantImg.getTranslateY());
            imgViewUnvalidate.setVisible(true);
            imgViewValidate.setVisible(false);
            return "unvalide";
        }
    }

}
