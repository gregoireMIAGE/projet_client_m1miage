/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Plateau.map.event;

import java.util.ArrayList;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.map.ControlerPlateauMap;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.naviguationPiece.ControlerNavBarPiece;
import projet_java_gl_reseau.Jeu.Chemin;
import projet_java_gl_reseau.Jeu.lesCarte.CarteVille;

/**
 *
 * @author greg
 */
public class EventPressedValidate extends ControlerPlateauMap implements EventHandler<MouseEvent> {

    @Override
    public void handle(MouseEvent event) {

        if (!pause && phase2) {
            //recuperation de la carte    
            StringBuffer message = new StringBuffer();
            if (courantImg != null && courantRail != null) { //permet de valider l'insertion de la carte
                message.append("Validate:rail," + courantRail.getId() + "," + courantCarteMap.positionX + "," + courantCarteMap.positionY);
                hidenImageView();
                insertOk = false;
                //mise a jour du nombre de carte a afficher
                miseAjourNbCarteRail();
                //mise a jour de la map
                miseAjourMapRail();
                //mise a jour du chemin construit ou terminé
                updateChemin();
                //on indique que la carte a etait posé
                courantRail.setPose();
                ControlerPlateauMap.addPoseRail(courantRail, courantCarteMap);
                courantImg = null;
                courantRail = null;
                courantCarteMap = null;
            } else if (courantImg != null && courantCity != null) {//si une carte city
                message.append("Validate:city," + courantCity.getId() + "," + courantCarteMap.positionX + "," + courantCarteMap.positionY);
                //retire image valide
                hidenImageView();
                //mise a jour de la carte
                miseAjourMapCity();
                //on modifie ça position x et y sur le tableau map[][]
                miseAjourCarteCity();
                //on met a jour le nombre de carte city
                miseAjourNbCarteCity();
                //on ajoute la carte a la liste des villes
                int index = addCarteCity();
                //ajout des cubes sur la carte
                //addCubeCity(index);
                //on indique que la carte city a etait pose
                courantCity.setPose();
                //on cache l'image de selection
                //imgViewSelect.setVisible(false);
                ControlerPlateauMap.addPoseCity(courantCity, courantCarteMap);
                //on supprime la carte city des carte attribuable
                lesCartesCitys.remove(courantCity);
                //on remte a null une fois les op terminé
                courantCity = null;
                courantImg = null;
                courantCarteMap = null;
            }
            ControlerNavBarPiece.hideImageSelect();
            if (!test) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //envoie du message au serveur
                        sendMessage(message.toString());
                        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }
                }).start();
            }
        }
    }

    //partie Rail************************************************************
    //permet de cacher les images validate, unvalidate, croix et fleches
    private void hidenImageView() {
        //on efface les fleche
        imageFlechDroite.setVisible(false);
        imageFlechGauche.setVisible(false);
        imageferme.setVisible(false);
        imgViewValidate.setVisible(false);
        imgViewUnvalidate.setVisible(false);
        //imgViewSelect.setVisible(false);
    }

    //permet de mettre a jour les info de la map
    private void miseAjourMapRail() {
        //modification de la map, mise a jour de la mapRail
        if (courantCarteMap != null) {//car si non ça veux dire que l'on a pas bougé la piece
                    /*if (courantRail.getPositionYmap() > -1 && courantCarteMap.getPositionX() > -1) {//si la piece a etait deplacé, on supprime de son ancien emplacement
             map[courantRail.getPositionYmap()][courantRail.getPositionXmap()] = "P";
             }*/
            map[courantCarteMap.getPositionY()][courantCarteMap.getPositionX()] = "R";
            courantRail.setPositionXmap(courantCarteMap.getPositionX());
            courantRail.setPositionYmap(courantCarteMap.getPositionY());
            //si la carte n'est pas deja dedans, si non cause d'erreur
            if (lesRailsInMap.contains(courantRail) == false) {
                lesRailsInMap.add(courantRail);
            }
        }
    }

    //permet de mettre a jour le nombre de carte a afficher 
    private void miseAjourNbCarteRail() {
        if (courantRail.getValidate() == false) {
            //permet de mettre a jour le nombre de carte 
            ControlerNavBarPiece.updateNbCarte(courantRail.getNameRail(), "-");
            //on ajoute l'image de la carte a l'interface MAP
            ControlerPlateauMap.addCarteRail(courantRail.getImageViw());
            courantRail.validate();
        }
    }
    //***********************************************************************

    //partie City************************************************************
    //permet de mettre a jour la carte city validé
    private void miseAjourCarteCity() {
        //on modifie ça position x et y sur le tableau map[][]
        courantCity.setPositionXmap(courantCarteMap.getPositionX());
        courantCity.setPositionYmap(courantCarteMap.getPositionY());
        //modifie ça place sur la map affiché
        courantCity.setX(courantImg.getTranslateX());
        courantCity.setY(courantImg.getTranslateY());
        //permet de switché de l'interface navBarPiece a l'interface map
        ControlerPlateauMap.addCarteRail(courantCity.getImageViw());
    }

    //pemret de mettre a jour la map
    private void miseAjourMapCity() {
        map[courantCarteMap.getPositionY()][courantCarteMap.getPositionX()] = courantCity.getKey();
    }

    //permet d'ajouter la carte a la liste des ville
    private int addCarteCity() {
        lesVilleInMap.add(new CarteVille(courantCity));
        courantCarteMap.setConstruct();//indique que la carte city a etait construite (pour valide rail
        return lesVilleInMap.size() - 1;
    }

    //pemret d'ajouter des cubes a la carte
    private void addCubeCity(int indexCrateVille) {
        if (lesMarchandisesReserves.size() > 0) {//si il reste des marchandise en reserve
            //suivat le niveau de la carte city, on ajoute les cubes
            CarteVille carteVille = lesVilleInMap.get(indexCrateVille);
            for (int i = 0; i < carteVille.getNiveau(); i++) {
                carteVille.addMarchandiseVille(lesMarchandisesReserves.get(0));
                lesMarchandisesReserves.get(0).setCarte(carteVille);

                lesMarchandises.add(lesMarchandisesReserves.get(0));//permet d'ajouter la marchandise reserves dans la marchandise active
                lesMarchandisesReserves.remove(0);
            }
            //dessin des marchandises. 
            ControlerPlateauMap.dessinMarchandiseCarte(carteVille);
        }
    }

    //pemret mettre a jour les carte city
    private void miseAjourNbCarteCity() {
        //permet de mettre a jour le nombre de carte 
        ControlerNavBarPiece.updateNbCarte(courantCity.getName(), "-");
    }
    //***********************************************************************

    //permet d'ajouter, finaliser un chemin
    private void updateChemin() {
        //creation ou recuperation du chemin coorespondant au rail
        if (attachCourantRails.size() > 0) {
            //donc on recupere le chemin existant pour un autre rail
            ArrayList<Chemin> chemins = new ArrayList<Chemin>();
            for (int i = 0; i < attachCourantRails.size(); i++) {
                chemins.addAll(attachCourantRails.get(i).getCheminAll());
            }

            //mise  jour des chemins et des rails
            if (attachCourantVilles.size() > 0) {
                //le chemin est un chemin terminé, on luis ajoute la ville (une seul ville possible dans l'arrayList)
                for (int i = 0; i < chemins.size(); i++) {
                    chemins.get(i).setVilleArrivee(attachCourantVilles.get(0));//mise a jour chemin
                    chemins.get(i).addCarteRail(courantRail);
                    //chemins.get(i).finished();
                    courantRail.addChemin(chemins.get(i));//ajout du chemin(pere)
                }
            } else {
                //le chemin continu a être construit, on lui ajoute le rail
                for (int i = 0; i < chemins.size(); i++) {
                    chemins.get(i).addCarteRail(courantRail);
                    courantRail.addChemin(chemins.get(i));
                }

            }
        } else if (attachCourantVilles.size() > 0) {
            //on crer un nouveau chemin avec une ville de depart
            if (attachCourantVilles.size() == 2) {
                //le chemin est un chemin fini (donc un rail deux ville)
                Chemin chemin = new Chemin(attachCourantVilles.get(0), attachCourantVilles.get(1), courantRail);
                //chemin.finished();
                lesChemins.add(chemin);
                //ajout du chemin au aurail (du pere au fils)
                courantRail.addChemin(chemin);

            } else {
                //le chemin est un chemin de depart (l'arraylist ville ne peux contenir qu'un chemin
                Chemin chemin = new Chemin(attachCourantVilles.get(0), courantRail);
                lesChemins.add(chemin);
                //ajout du chemin au aurail (du pere au fils)
                courantRail.addChemin(chemin);
            }
        }
    }

}
