/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Plateau.map.event;

import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Rectangle;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.map.ControlerPlateauMap;
import projet_java_gl_reseau.Jeu.Marchandise;

/**
 *
 * @author greg
 */
public class EventPressedCube extends ControlerPlateauMap implements EventHandler<MouseEvent> {

    private Group root;

    public EventPressedCube(Group root) {
        this.root = root;
    }

    @Override
    public void handle(MouseEvent event) {
        if (!pause && phase3) {
            root.getChildren().remove(cube2);

            courantMarchandise = this.getIdMarchandise((Rectangle) event.getSource());

            //on affiche l'image de selection du cube
            selectCube.setTranslateX(((Rectangle) event.getSource()).getTranslateX() - 1);
            selectCube.setTranslateY(((Rectangle) event.getSource()).getTranslateY() - 1);
            selectCube.setVisible(true);
            //recuperation at affichage des chemins de l'arbre.
            chemins = valideDeplaceMarchandiseCity(courantMarchandise);
            if (lesCheminsCourant.size() > 0) {
                interfacePlateauMap.setOpacity(0.5);
                //chemins.getChildren().add((Rectangle) event.getSource());
                root.getChildren().add(chemins);
            }
            //root.getChildren().add(cube2);
        }
    }
    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
}
