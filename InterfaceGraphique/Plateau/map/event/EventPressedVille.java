/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Plateau.map.event;

import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.map.ControlerPlateauMap;
import projet_java_gl_reseau.Jeu.lesCarte.CarteVille;

/**
 *
 * @author greg
 */
public class EventPressedVille extends ControlerPlateauMap implements EventHandler<MouseEvent> {

    private boolean show;
    private final Group pannel;

    public EventPressedVille(CarteVille carte) {
        show = false;
        this.pannel = croissancePannel(carte);
    }

    @Override
    public void handle(MouseEvent event) {
        if (!pause && phase2) {
            if (show) {
                show = false;
                this.pannel.setVisible(false);
            } else {
                show = true;
                this.pannel.setVisible(true);
            }
        }
    }

}
