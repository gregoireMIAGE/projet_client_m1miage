/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Plateau.map.event;

import java.util.ArrayList;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.map.ControlerPlateauMap;
import projet_java_gl_reseau.Jeu.Carte;
import projet_java_gl_reseau.Jeu.lesCarte.CarteRail;
import projet_java_gl_reseau.Jeu.lesCarte.CarteVille;

/**
 *
 * @author greg
 */
public class EventPressedChemin extends ControlerPlateauMap implements EventHandler<MouseEvent> {

    @Override
    public void handle(MouseEvent event) {
        Group chemin = ((Group) event.getSource());
        //on ajoute le chemin selectionné a la marchandise
        courantMarchandise.setLeChemins(lesCheminsCourant.get(Integer.parseInt(chemin.getId())));
        //on ajoute les marchandise au marchandise en cours de deplacement
        lesMarchandisesDeplacelment.add(courantMarchandise);

        //on reafiche la map
        interfacePlateauMap.setOpacity(1);
        chemins.getChildren().clear();
    }
}
