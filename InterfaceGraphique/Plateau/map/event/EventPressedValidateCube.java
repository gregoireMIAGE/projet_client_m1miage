/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Plateau.map.event;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Rectangle;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.map.ControlerPlateauMap;
import projet_java_gl_reseau.Jeu.Marchandise;

/**
 *
 * @author greg
 */
public class EventPressedValidateCube extends ControlerPlateauMap implements EventHandler<MouseEvent>{

    @Override
    public void handle(MouseEvent event) {
         if (!pause && phase3) {
             Rectangle cube = ((Rectangle)event.getSource());
             //recupration de la marchandise
             Marchandise marchandise = getIdMarchandise(cube2);
             //recuperation at affichage des chemins de l'arbre.
             valideDeplaceMarchandiseCity(marchandise);
         }
    }
    
    
    
}
