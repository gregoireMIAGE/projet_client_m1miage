/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Plateau.map.event;

import javafx.event.EventHandler;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.map.ControlerPlateauMap;
import projet_java_gl_reseau.Jeu.lesCarte.CarteVille;

/**
 *
 * @author greg
 */
public class EventEnteredVille extends ControlerPlateauMap implements EventHandler<MouseEvent> {

    @Override
    public void handle(MouseEvent event) {
        if (!pause && phase3 && courantMarchandise!=null) {

            ImageView imgVille = ((ImageView) event.getSource());
            CarteVille carte = getIdCarteVille(imgVille);

            selectLine.setEndX(imgVille.getTranslateX() + imgVille.getFitWidth()/2);
            selectLine.setEndY(imgVille.getTranslateY() + imgVille.getFitHeight()/2);
            
            cube2.setTranslateX(imgVille.getTranslateX() + imgVille.getFitWidth()/2 - cube2.getWidth()/2);
            cube2.setTranslateY(imgVille.getTranslateY() + imgVille.getFitHeight()/2 - cube2.getHeight()/2);
            
            if(validateMoveMarchandise(carte)){
                selectLine.setFill(Color.GREEN);
                selectLine.setStroke(Color.GREEN);
            }else{
                selectLine.setFill(Color.RED);
                selectLine.setStroke(Color.RED);
            }
        }
    }
    
    private boolean validateMoveMarchandise(CarteVille carte){
        return (courantMarchandise.getCarte().equals(carte)==false && carte.getColor() == courantMarchandise.getColor());
    }

}
