/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Plateau.map.event;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.map.ControlerPlateauMap;
import projet_java_gl_reseau.Jeu.lesCarte.CarteVille;

/**
 *
 * @author greg
 */
public class EventPressedCroissance extends ControlerPlateauMap implements EventHandler<MouseEvent> {

    private CarteVille carte;

    public EventPressedCroissance(CarteVille carte) {
        this.carte = carte;
    }

    @Override
    public void handle(MouseEvent event) {
        if (!pause && phase2) {
            for (int i = 0; i<(carte.getNiveau() - carte.getNbMarchandiseVille()); i++) {
                carte.addMarchandiseVille(lesMarchandisesReserves.get(0));
                lesMarchandisesReserves.get(0).setCarte(carte);
                
                lesMarchandises.add(lesMarchandisesReserves.get(0));//permet d'ajouter la marchandise reserves dans la marchandise active
                lesMarchandisesReserves.remove(0);
            }
            
        }
    }
}
