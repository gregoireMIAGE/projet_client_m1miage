/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Plateau.map.event;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.map.ControlerPlateauMap;

/**
 *
 * @author greg
 */
public class EventPressedFlecheDroite extends ControlerPlateauMap implements EventHandler<MouseEvent> {

    @Override
    public void handle(MouseEvent event) {
        if (!pause && phase2) {
            if (courantRail != null) {

                courantRail.getImageViw().setRotate(courantRail.getImageViw().getRotate() + 60);
                courantRail.moveToArray("droite");

                //envoie du message au client
                if (!test) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            //message a envoyé au autre joueur
                            StringBuffer message = new StringBuffer();
                            message.append("RotateCard:" + courantRail.getId() + "," + 60 + ",droite");
                            //envoie du message au serveur
                            sendMessage(message.toString());
                            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                        }
                    }).start();
                }

                //permet de verifier si la carte peux être inséré
                verifValidateInsertCart();
            }
        }
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
