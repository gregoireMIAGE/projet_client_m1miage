/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Plateau.map;

import java.util.ArrayList;
import java.util.HashMap;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import projet_java_gl_reseau.Arbre.Arbre;
import projet_java_gl_reseau.Arbre.Noeud;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.ControlerPlateau;
import static projet_java_gl_reseau.InterfaceGraphique.Plateau.ControlerPlateau.getCartePosMap;
import static projet_java_gl_reseau.InterfaceGraphique.Plateau.ControlerPlateau.getCartePosRail;
import static projet_java_gl_reseau.InterfaceGraphique.Plateau.ControlerPlateau.getCartePosVille;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.map.event.EventEnteredPieceMap;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.map.event.EventEnteredVille;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.map.event.EventPressedChemin;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.map.event.EventPressedClose;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.map.event.EventPressedCroissance;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.map.event.EventPressedCube;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.map.event.EventPressedFlecheDroite;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.map.event.EventPressedFlecheGauche;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.map.event.EventPressedValidate;
import projet_java_gl_reseau.Jeu.Carte;
import projet_java_gl_reseau.Jeu.Marchandise;
import projet_java_gl_reseau.Jeu.lesCarte.CarteRail;
import projet_java_gl_reseau.Jeu.lesCarte.CarteVille;
import projet_java_gl_reseau.XML.LectureXML;
import static projet_java_gl_reseau.outil.ConvertArray.convertToIntArrayMap;
import static projet_java_gl_reseau.outil.ConvertArray.convertToStringArrayMap;
import projet_java_gl_reseau.reseaux.Reseau;

/**
 *
 * @author greg
 */
public class ControlerPlateauMap extends ControlerPlateau {

    protected static InterfacePlateauMap interfacePlateauMap;
    protected static HashMap<String, CarteVille> lesCartesMapXml;//represente les carte ville recuperer du fichier xml de config

    protected static ImageView imgViewValidate;
    protected static ImageView imgViewUnvalidate;
    protected static ImageView imageFlechDroite;
    protected static ImageView imageFlechGauche;
    protected static ImageView imageferme;
    protected static Rectangle selectCube;
    protected static Group chemins;

    //permet de definir un cube2
    protected static Rectangle cube2;
    //line permettant de selectionner une ville
    protected static Line selectLine;

    //imageView courante
    protected static ImageView courantImg;

    //cube de marchandise courrant
    protected static Marchandise courantMarchandise;

    //event hendler pressed
    protected static EventPressedFlecheDroite imgFlecheDroiteOnMousePressedEventHandler;
    protected static EventPressedFlecheGauche imgFlecheGaucheOnMouseDraggedEventHandler;
    protected static EventEnteredPieceMap imgMapOnMouseEnteredEventHandler;
    protected static EventPressedClose imageFermeEvantHandler;
    protected static EventPressedValidate imgValideEventHandler;
    protected static EventPressedCroissance imgPressedCroissance;
    protected static EventPressedCube rectanglePressedCube;
    protected static EventEnteredVille imgEnteredVille;
    protected static EventPressedChemin choixChemin;

    //errreur de marchandise non ajouté
    protected static ArrayList<Marchandise> lesErreurMarchandises = new ArrayList<Marchandise>();
    protected static ArrayList<Group> lesCheminsGroupCourant = new ArrayList<Group>();
    protected static ArrayList<ArrayList<Carte>> lesCheminsCourant = new ArrayList<ArrayList<Carte>>();

    //valeur du chmin selectionnée
    protected static int courantChoixChemin;

    //permet d'initialiser le controler de linterface
    public static void initControlerPlateauMap(double widthFenetre, double heightFenetre, String xmlMap, String xmlMapRail) {
        interfacePlateauMap = new InterfacePlateauMap(widthFenetre, heightFenetre);

        map = convertToStringArrayMap(xmlMap);
        mapDebut = convertToStringArrayMap(xmlMap);
        mapRail = convertToIntArrayMap(xmlMapRail);
        lesCartesMapXml = new HashMap<>();
        decryptXmlCartesMap(LectureXML.lireXmlCartesMap());
    }

    //permet de dessiner le plateau
    public static void dessinerPlateau(Group root) {
        imgFlecheDroiteOnMousePressedEventHandler = new EventPressedFlecheDroite();
        imgFlecheGaucheOnMouseDraggedEventHandler = new EventPressedFlecheGauche();
        imgValideEventHandler = new EventPressedValidate();
        imageFermeEvantHandler = new EventPressedClose();
        imgEnteredVille = new EventEnteredVille();
        rectanglePressedCube = new EventPressedCube(root);
        choixChemin = new EventPressedChemin();

        interfacePlateauMap.dessinImagesViews(); //on ajoute les images validetas, unvalidates ...
        imgMapOnMouseEnteredEventHandler = new EventEnteredPieceMap();
        //on dessine la map
        interfacePlateauMap.dessinMap(map);
        //on recupere les marchandise qui n'on pas pu être jouté aux villes
        addMarchandiseErreur();
        dessinMarchandise();//on dessine les marchandise
    }

    //permet de dessiner les marchandise sur la map
    public static void dessinMarchandise() {
        //on dessine les cube de marchandise sur chaque carte
        for (int i = 0; i < lesVilleInMap.size(); i++) {
            ArrayList<Rectangle> lesRectangles = lesVilleInMap.get(i).dessinCubes();
            for (int a = 0; a < lesRectangles.size(); a++) {
                interfacePlateauMap.dessineMarchandise(lesRectangles.get(a));
            }
        }
    }

    //permet de dessiner les marchandise sur la map de la carte en paramètre
    public static void dessinMarchandiseCarte(CarteVille carte) {
        //on dessine les cube de marchandise sur la carte
        ArrayList<Rectangle> lesRectangles = carte.dessinCubes();
        for (int a = 0; a < lesRectangles.size(); a++) {

            interfacePlateauMap.dessineMarchandise(lesRectangles.get(a));
        }
    }

    public static void addMarchandise(String marchandises) {
        String[] lesMarchandiseStr = marchandises.split(";");
        for (int i = 0; i < lesMarchandiseStr.length; i++) {
            Color color = Color.valueOf(lesMarchandiseStr[i].split(",")[0]);
            int xmap = Integer.parseInt(lesMarchandiseStr[i].split(",")[1]);
            int ymap = Integer.parseInt(lesMarchandiseStr[i].split(",")[2]);
            int id = Integer.parseInt(lesMarchandiseStr[i].split(",")[3]);
            double width = 25; //double width = Double.parseDouble(lesMarchandiseStr[i].split(",")[3]);
            double height = 25;//double height = Double.parseDouble(lesMarchandiseStr[i].split(",")[4]);
            //recuperation de la carte associé a la marchandise
            CarteVille carte = getCartePosVille(xmap, ymap);
            if (carte == null) {//si les carte n esont pas encore créer
                lesErreurMarchandises.add(new Marchandise(id, color, 0, 0, xmap, ymap, width, height));
            } else {
                //ajout de la nouvelle marchandise
                Marchandise marchandise = new Marchandise(id, color, carte.getX(), carte.getY(), xmap, ymap, width, height);
                marchandise.setCarte(carte);
                lesMarchandises.add(marchandise);
            }
        }
    }

    public static void addMarchandiseErreur() {
        for (int i = 0; i < lesErreurMarchandises.size(); i++) {
            CarteVille carte = getCartePosVille(lesErreurMarchandises.get(i).getX(), lesErreurMarchandises.get(i).getY());
            lesErreurMarchandises.get(i).setXmap(carte.getX());
            lesErreurMarchandises.get(i).setYmap(carte.getY());
            lesErreurMarchandises.get(i).setCarte(carte);
            carte.addMarchandiseVille(lesErreurMarchandises.get(i));
            lesMarchandises.add(lesErreurMarchandises.get(i));
            lesMarchandisesReserves.remove(i);
        }
    }

    //permet d'ajouter les marchandise reserve envoyé par le serveur
    public static void addMarchandiseReserve(Color color, double width, double height) {
        lesMarchandisesReserves.add(new Marchandise(0, color, 0, 0, 0, 0, width, height));
    }

    //permet d'ajouter les image unviladate, validate...
    public static void dessineImageView(Group root) {
        //on ajoute les image qui vienne au dessus des carteMap
        root.getChildren().add(imgViewUnvalidate);
        root.getChildren().add(imgViewValidate);
        root.getChildren().add(imageFlechDroite);
        root.getChildren().add(imageFlechGauche);
        root.getChildren().add(imageferme);
        root.getChildren().add(selectLine);
    }

    //pemret d'ajouter une carte rail a l'interface
    public static void addCarteRail(Node e) {
        interfacePlateauMap.addNodeCarteRail(e);
    }

    //pemret d'ajouter une marchandise a l'interface
    public static void addMarchandise(Node e) {
        interfacePlateauMap.addNodeMarchandise(e);
    }

    //permet de recuperer la liste des Rail Contenue dans le fichier xml
    private static void decryptXmlCartesMap(Document racine) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        NodeList cartesRailNoeuds = racine.getElementsByTagName("carte");
        for (int i = 0; i < cartesRailNoeuds.getLength(); i++) {
            Element carte = (Element) cartesRailNoeuds.item(i).getChildNodes();
            String name = carte.getElementsByTagName("name").item(0).getTextContent();
            String key = carte.getElementsByTagName("key").item(0).getTextContent();
            String location = "file:" + System.getProperty("user.dir") + carte.getElementsByTagName("location").item(0).getTextContent();
            int niveau = Integer.parseInt(carte.getElementsByTagName("niveau").item(0).getTextContent());
            Color color = Color.valueOf(carte.getElementsByTagName("color").item(0).getTextContent());
            CarteVille carteVille = new CarteVille(name, location, niveau, 0, 0);
            carteVille.setColor(color);
            lesCartesMapXml.put(key, carteVille);
        }
    }

    public static Group getInterfacePlateauMap() {
        return interfacePlateauMap;
    }

    //permet de verifier l'insertion d'une carte
    protected static void verifValidateInsertCart() {
        if (validePointAttach(courantCarteMap.positionX, courantCarteMap.positionY)) {//si ok pour validation
            imgViewValidate.setTranslateX(courantImg.getTranslateX());
            imgViewValidate.setTranslateY(courantImg.getTranslateY());
            imgViewValidate.setVisible(true);
            imgViewUnvalidate.setVisible(false);

        } else {
            imgViewUnvalidate.setTranslateX(courantImg.getTranslateX());
            imgViewUnvalidate.setTranslateY(courantImg.getTranslateY());
            imgViewUnvalidate.setVisible(true);
            imgViewValidate.setVisible(false);
        }
    }

    //fonction permettant de definir si oui ou non on peux inseré une carte sur la position
    //dans un second plan, recupere les villes et rails qui match avec le rail courant
    private static boolean validePointAttach(int x, int y) {
        //on initialise les arraylist pour eviter les redondance
        attachCourantRails.clear();
        attachCourantVilles.clear();

        int carteRailHaut = 0;
        int carteRailBas = 0;
        int carteRailBasGauche = 0;
        int carteRailBasDroite = 0;
        int carteRailHautGauche = 0;
        int carteRailHautDroite = 0;
        //recuperation des carte alentour et verification du match possible
        CarteRail rail;
        CarteVille ville;
        if (y > 0) {
            if (map[y - 1][x].equalsIgnoreCase("R")) {
                rail = getCartePosRail(x, y - 1);
                if ((carteRailHaut = rail.getPointAttach(4)) == 1) {
                    attachCourantRails.add(rail);//permet de recuperer le rail match
                }
            } else if (map[y - 1][x].equalsIgnoreCase("V")) {
                carteRailHaut = getCartePosMap(x, y - 1).getConstruct();
            } else if (map[y - 1][x].contains("V")) {
                carteRailHaut = 1;
                attachCourantVilles.add(getCartePosVille(x, y - 1));//permet de recuperer la ville match
            }
        }

        if (y < map.length - 1) {
            if (map[y + 1][x].equalsIgnoreCase("R")) {
                rail = getCartePosRail(x, y + 1);
                if ((carteRailBas = rail.getPointAttach(1)) == 1) {
                    attachCourantRails.add(rail);//permet de recuperer le rail match
                }
            } else if (map[y + 1][x].equalsIgnoreCase("V")) {
                carteRailBas = getCartePosMap(x, y + 1).getConstruct();
            } else if (map[y + 1][x].contains("V")) {
                carteRailBas = 1;
                attachCourantVilles.add(getCartePosVille(x, y + 1));
            }
        }

        if (x > 0 && (y - ((x - 1) % 2)) > -1) {
            if (map[y - ((x - 1) % 2)][x - 1].equalsIgnoreCase("R")) {
                rail = getCartePosRail(x - 1, y - ((x - 1) % 2));
                if ((carteRailHautGauche = rail.getPointAttach(3)) == 1) {
                    attachCourantRails.add(rail);
                }
            } else if (map[y - ((x - 1) % 2)][x - 1].equalsIgnoreCase("V")) {
                carteRailHautGauche = getCartePosMap(x - 1, y - ((x - 1) % 2)).getConstruct();
            } else if (map[y - ((x - 1) % 2)][x - 1].contains("V")) {
                carteRailHautGauche = 1;
                attachCourantVilles.add(getCartePosVille(x - 1, y - ((x - 1) % 2)));
            }
        }

        if (x < map[0].length - 1 && (y - ((x - 1) % 2)) > -1) {
            if (map[y - (x - 1) % 2][x + 1].equalsIgnoreCase("R")) {
                rail = getCartePosRail(x + 1, y - (x - 1) % 2);
                if ((carteRailHautDroite = rail.getPointAttach(5)) == 1) {
                    attachCourantRails.add(rail);
                }
            } else if (map[y - (x - 1) % 2][x + 1].equalsIgnoreCase("V")) {
                carteRailHautDroite = getCartePosMap(x + 1, y - (x - 1) % 2).getConstruct();
            } else if (map[y - (x - 1) % 2][x + 1].contains("V")) {
                carteRailHautDroite = 1;
                attachCourantVilles.add(getCartePosVille(x + 1, y - (x - 1) % 2));
            }
        }

        if (x > 0 && (y + (x % 2)) < map.length) {
            if (map[y + (x % 2)][x - 1].equalsIgnoreCase("R")) {
                rail = getCartePosRail(x - 1, y + (x % 2));
                if ((carteRailBasGauche = rail.getPointAttach(2)) == 1) {
                    attachCourantRails.add(rail);
                }
            } else if (map[y + (x % 2)][x - 1].equalsIgnoreCase("V")) {
                //carteRailBasGauche = (getCartePosMap(x - 1, y + (x % 2)).getConstruct()|getCartePosRail(x - 1, y + (x % 2)).getPointAttach(2));
                carteRailBasGauche = getCartePosMap(x - 1, y + (x % 2)).getConstruct();
            } else if (map[y + (x % 2)][x - 1].contains("V")) {
                carteRailBasGauche = 1;
                attachCourantVilles.add(getCartePosVille(x - 1, y + (x % 2)));
            }
        }

        if (x < map[0].length - 1 && (y + (x % 2)) < map.length) {
            if (map[y + (x % 2)][x + 1].equalsIgnoreCase("R")) {
                rail = getCartePosRail(x + 1, y + (x % 2));
                if ((carteRailBasDroite = rail.getPointAttach(0)) == 1) {
                    attachCourantRails.add(rail);
                }
            } else if (map[y + (x % 2)][x + 1].equalsIgnoreCase("V")) {
                carteRailHaut = getCartePosMap(x + 1, y + (x % 2)).getConstruct();
            } else if (map[y + (x % 2)][x + 1].contains("V")) {
                carteRailBasDroite = 1;
                attachCourantVilles.add(getCartePosVille(x + 1, y + (x % 2)));
            }
        }

        //compare si oui ou non on peux attacher le rail
        return courantRail.comparePointAttach(new int[]{carteRailHautGauche, carteRailHaut, carteRailHautDroite, carteRailBasDroite, carteRailBas, carteRailBasGauche});

    }

    //permet d'afficher le pannel de demande croissance
    protected static Group croissancePannel(CarteVille carte) {
        //on affiche le panel si il y a encore des marchandises en reserves et la carte n'est pas full
        //if (lesMarchandisesReserves.size() > 0 /*&& carte.getNiveau() < carte.getMarchandiseVille()*/) {
        double x = carte.getImageViw().getTranslateX() + carte.getImageViw().getFitWidth() / 2;
        double y = carte.getImageViw().getTranslateY();
        return interfacePlateauMap.dessinePannelCroissance(lesMarchandisesReserves, carte.getNiveau(), x, y, new EventPressedCroissance(carte));
        /*}else{
         return new Group();
         }*/
    }

    //permet d'afficher une nouvelle marchandise
    protected static void afficheNewMarchandise(Marchandise laMarchandise, CarteVille laCarte, Group root) {
        interfacePlateauMap.dessineMarchandise(laMarchandise.dessinCubeVille(laCarte));
    }

    //permet de definir si oui ou non la marchandise est valide quand on la deplace
    protected static boolean valideDeplaceMarchandiseRail(Marchandise marchandise, CarteRail carte) {
        //on recupere l'abre pour la quel la marchandise a démaré
        int index = getIdArbreVille(marchandise.getCarteVilleDep());
        Arbre arbreville = lesArbresVilles.get(index);
        //on recupere le parcours possible de la marchandize pour la ville de depart
        ArrayList<ArrayList<Noeud>> parcourtPossible = arbreville.getParcourtCartes(marchandise.getColor());
        //on definie si oui ou non la prochaine carte peux se situer sur le chemin de la marchandise
        boolean valide = false;
        int i = 0, a = 0;
        while (i < parcourtPossible.size() && valide == false) {
            while (a < parcourtPossible.get(i).size() && valide == false) {
                if (parcourtPossible.get(i).get(a).containsIdCarteRail(carte.getId())) {
                    valide = true;
                }
            }
        }
        if (i < parcourtPossible.size() && a < parcourtPossible.get(i).size() && valide) {
            return true;
        } else {
            return false;
        }
    }

    protected static Group valideDeplaceMarchandiseCity(Marchandise marchandise) {
        //on recupere l'abre pour la quel la marchandise a démaré
        Arbre arbreville = lesArbresVilles.get(getIdArbreVille(marchandise.getCarteVilleDep()));
        //on recupere le parcours possible de la marchandize pour la ville de depart
        Noeud parcourtPossible = arbreville.getParcourtCartes2(marchandise.getColor());
        //on affiche les parcours possible sur la MAP
        //...
        if (parcourtPossible.getSizeFils() > 0) {//si il existe au moin un chemin
            return afficheLesCheminsPossibles(parcourtPossible);
        } else {
            return new Group();
        }
    }

    private static Group afficheLesCheminsPossibles(Noeud CheminsPossible) {
        lesCheminsGroupCourant = new ArrayList<Group>();
        lesCheminsCourant = new ArrayList<ArrayList<Carte>>();
        recursifAfficheLesCheminsGroupPossibles(CheminsPossible, new Group(), lesCheminsGroupCourant, new ArrayList<Carte>(), 0);
        Group chemins = new Group();
        for (int i = 0; i < lesCheminsGroupCourant.size(); i++) {
            Group chemin = new Group(lesCheminsGroupCourant.get(i).getChildren());
            //chemin.setId(lesCheminsGroupCourant.get(i).getId());
            chemin.setId(String.valueOf(i));
            //chemin.setTranslateX(i * 200);
            chemin.setOpacity(0.8);
            chemin.setOnMouseEntered(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {
                    Group chemin = ((Group) event.getSource());
                    chemin.setOpacity(1);
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

            });
            chemin.setOnMouseExited(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    Group chemin = ((Group) event.getSource());
                    chemin.setOpacity(0.8);
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

            });
            chemin.setOnMouseClicked(choixChemin);
            chemins.getChildren().add(chemin);
        }
        return chemins;
    }
        
    private static void recursifAfficheLesCheminsGroupPossibles(Noeud enCours, Group chemin, ArrayList<Group> lesCheminsGroups, ArrayList<Carte> lesCheminsCartes, int id) {
        //on ajoute la ville d'arrivé au groupe
        chemin.getChildren().add(enCours.getVille().duplicate());
        lesCheminsCartes.add(enCours.getVille());

        if (!enCours.isFeuille()) {
            //on ajoute toute les carte rail au groupe
            for (int i = 0; i < enCours.getSizeFils(); i++) {//liste des fils du noeud(villes)
                for (int a = 0; a < enCours.getSizeRail(i); a++) {//liste de chaque rail pour chaque ville
                    CarteRail carte = getIdCarteRailinMap(enCours.getIdRail(i, a));
                    lesCheminsCartes.add(carte);
                    chemin.getChildren().add(carte.duplicate());
                }
                Group newChemin = new Group(chemin.getChildren());
                ArrayList<Carte> newLesCheminsCartes = new ArrayList<Carte>(lesCheminsCartes);
                recursifAfficheLesCheminsGroupPossibles(enCours.getFils(i), newChemin, lesCheminsGroups, newLesCheminsCartes, id);
                lesCheminsCartes.clear();
            }
        } else {//si le noeud est uen feuille, donc nous avont atteint la ville d'arrivé
            chemin.setId(String.valueOf(id));
            lesCheminsGroups.add(chemin);
            lesCheminsCourant.add(lesCheminsCartes);
            id = id + 1;
        }
    }

    //permet de deplacer toute les marchandise en cours e deplacement
    public static void deplaceLesMarchandises() {
        for (int i = 0; i < lesMarchandisesDeplacelment.size(); i++) {
            moveCubeMarchandise(lesMarchandisesDeplacelment.get(i));
        }
    }

    private static void moveCubeMarchandise(Marchandise marchandise) {
        //deplacement de la marchandise selectionné
        //si le deplacement du chemin est supperieur au niveau de la locomotive, on deplcace la marchandise sur la ville d'arrivée
        if (marchandise.getDeplacementChemin() + unJoueur.getNivLocomotive() > 0) {
            if (marchandise.getDeplacementChemin() + unJoueur.getNivLocomotive() >= marchandise.getNbRailsChemin()) {
                //verifier que la ville n'est pas complettes
                if (!((CarteVille) marchandise.getRailChemin(marchandise.getNbRailsChemin())).isComplete()) {
                    deplaceVilleMarchandise(((CarteVille) marchandise.getRailChemin(marchandise.getNbRailsChemin())));
                    marchandise.seekChemin(marchandise.getNbRailsChemin());
                    //supression de la marchandise
                    //ajout des point au joueur
                } else {//si la ville est complette
                    int seek = marchandise.getDeplacementChemin() + unJoueur.getNivLocomotive();
                    deplaceRailMarchandise(((CarteRail) marchandise.getRailChemin(seek - 1)));
                    marchandise.seekChemin(marchandise.getNbRailsChemin() - 1);
                }
            } else {//si non on deplace sur la prochaine carte
                int seek = marchandise.getDeplacementChemin() + unJoueur.getNivLocomotive();
                Carte carte = marchandise.getRailChemin(seek);
                //si carte ville 
                if (carte.getClass().getTypeName().contains("CarteVille")) {
                    //verification que la ville n'est pas coplete
                    CarteVille carteVille = ((CarteVille) carte);
                    if (carteVille.isComplete()) {
                        deplaceRailMarchandise(((CarteRail) marchandise.getRailChemin(seek - 1)));
                        marchandise.seekChemin(seek - 1);
                    } else {
                        deplaceVilleMarchandise(carteVille);
                        marchandise.seekChemin(seek);
                    }
                } else {
                    deplaceRailMarchandise((CarteRail) carte);
                    marchandise.seekChemin(seek);
                }
            }
        }
    }

    private static void deplaceRailMarchandise(CarteRail rail) {
        StringBuffer message = new StringBuffer("MoveMarchandise:");
        courantMarchandise.setCarte(rail);
        message.append(rail.getId()+",");
        //on deplace le cube graphiquement
        courantMarchandise.getCube().setTranslateX(rail.getImageViw().getTranslateX()+rail.getImageViw().getFitWidth()/2-courantMarchandise.getCube().getWidth()/2);
        courantMarchandise.getCube().setTranslateY(rail.getImageViw().getTranslateY()+rail.getImageViw().getFitHeight()/2-courantMarchandise.getCube().getHeight()/2);
        message.append(rail.getPositionXmap()+","+rail.getPositionYmap());
        //envoyer le message au serveur
        new Thread(new Runnable() {
            @Override
            public void run() {
                Reseau.sendMessage(message.toString());
            }
        }).start();
    }

    private static void deplaceVilleMarchandise(CarteVille ville) {
        courantMarchandise.setCarte(ville);
        //on deplace le cube graphiquement
        ville.addMarchandiseVille(courantMarchandise);
        ville.MiseAJour();
    }

    private void suppressionMarchandise(Marchandise marchandise) {
        lesMarchandisesDeplacelment.remove(marchandise);
        lesMarchandises.remove(marchandise);
    }
}
