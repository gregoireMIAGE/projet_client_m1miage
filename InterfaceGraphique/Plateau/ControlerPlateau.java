/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Plateau;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import projet_java_gl_reseau.Arbre.Arbre;
import projet_java_gl_reseau.InterfaceGraphique.AttenteJoueur.ControlerWaitPannel;
import projet_java_gl_reseau.InterfaceGraphique.Connexion.ControlerConnexion;
import projet_java_gl_reseau.InterfaceGraphique.ControlerInterfaceGraphique;
import projet_java_gl_reseau.InterfaceGraphique.Information.ControlerInformation;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.InformationJoueur.ControlerPlateauInfoJoueur;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.infoJeu.ControlerPlateauInfoPartie;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.map.ControlerPlateauMap;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.naviguationPiece.ControlerNavBarPiece;
import projet_java_gl_reseau.Jeu.Carte;
import projet_java_gl_reseau.Jeu.Chemin;
import projet_java_gl_reseau.Jeu.Marchandise;
import projet_java_gl_reseau.Jeu.Partie;
import projet_java_gl_reseau.Jeu.lesCarte.CarteCity;
import projet_java_gl_reseau.Jeu.lesCarte.CarteMap;
import projet_java_gl_reseau.Jeu.lesCarte.CarteRail;
import projet_java_gl_reseau.Jeu.lesCarte.CarteVille;
import projet_java_gl_reseau.controler.Controler;
import projet_java_gl_reseau.player.Joueur;

/**
 *
 * @author greg
 */
public class ControlerPlateau {

    protected static boolean insertOk;
    protected static boolean pause;
    protected static boolean test;

    protected static boolean phase1;
    protected static boolean phase2;
    protected static boolean phase3;
    protected static boolean phase4;

    protected static String[][] map;
    protected static String[][] mapDebut; //la map de debut qui permet de recuperer les objet present a la base
    protected static int[][] mapRail;

    //les carte tuiles action
    private static HashMap<String, String> lesTuilesActions;

    //carte rail que le joeuur selectionne
    protected static CarteRail courantRail;
    protected static CarteCity courantCity;
    protected static CarteMap courantCarteMap;
    protected static Marchandise courantMarchandise;

    //permet de recuperer l'environement d'attach d'une carte courante sur le point d'être validé
    protected static ArrayList<CarteRail> attachCourantRails;
    protected static ArrayList<CarteVille> attachCourantVilles;

    protected static ArrayList<CarteVille> lesVilleInMap;//représente les villes sur le plateau
    protected static ArrayList<CarteRail> lesRailsInMap;//représente les railles sur le plateau
    protected static ArrayList<CarteMap> lesMapInMap;//représente les railles sur le plateau
    protected static ArrayList<Carte> LesRailPose;//représente les railles posé sur un tour, permet de calculer le solde
    protected static ArrayList<Chemin> lesCheminsTrouPrecednt;
    protected static ArrayList<Chemin> lesChemins;
    protected static ArrayList<Marchandise> lesMarchandises; //represente toute les marchandise sur la map
    protected static ArrayList<Marchandise> lesMarchandisesDeplacelment; //represente toute les marchandise sur la map
    protected static ArrayList<Marchandise> lesMarchandisesReserves; //represente toute les marchandise sur la map

    //les carte présente dans le jeux
    protected static LinkedList<CarteRail> lesCartesRails;
    protected static LinkedList<CarteCity> lesCartesCitys;

    protected static double orgSceneX, orgSceneY;
    protected static double orgTranslateX, orgTranslateY;

    protected static Joueur unJoueur;
    protected static Joueur courantAdversaire;

    //liste des arbres pour chaque ville avec au moin un chemin fini de la carte
    protected static ArrayList<Arbre> lesArbresVilles;

    public static void initControlerPlateau(Joueur joueurPar) {

        lesTuilesActions = new HashMap<String, String>();
        lesTuilesActions.put("ordre", "/src/projet_java_gl_reseau/config/images/carteTuile_ordreDuTour.png");
        lesTuilesActions.put("deplacement", "/src/projet_java_gl_reseau/config/images/carteTuile_premierDeplacement.png");
        lesTuilesActions.put("ingenieur", "/src/projet_java_gl_reseau/config/images/carteTuile_ingenieur.png");
        lesTuilesActions.put("construction", "/src/projet_java_gl_reseau/config/images/carteTuile_premiereConstruction.png");
        lesTuilesActions.put("croissance", "/src/projet_java_gl_reseau/config/images/carteTuile_croissanceCite.png");
        lesTuilesActions.put("locomotive", "/src/projet_java_gl_reseau/config/images/carteTuile_Locomotive.png");
        lesTuilesActions.put("urbanisation", "/src/projet_java_gl_reseau/config/images/carteTuile_urbanisation.png");

        unJoueur = joueurPar;
        lesVilleInMap = new ArrayList<CarteVille>();
        lesRailsInMap = new ArrayList<CarteRail>();
        lesMapInMap = new ArrayList<CarteMap>();
        lesChemins = new ArrayList<Chemin>();
        lesCheminsTrouPrecednt = new ArrayList<Chemin>();
        lesMarchandises = new ArrayList<Marchandise>();
        lesMarchandisesReserves = new ArrayList<Marchandise>();
        lesArbresVilles = new ArrayList<Arbre>();
        LesRailPose = new ArrayList<Carte>();
        lesMarchandisesDeplacelment = new ArrayList<Marchandise>();

        lesCartesCitys = new LinkedList<CarteCity>();
        lesCartesRails = new LinkedList<CarteRail>();

        pause = false;
    }

    //ACCESSEUR
    public static void addCarteRail(CarteRail carte) {
        lesCartesRails.add(carte);
    }

    public static void addCarteCity(CarteCity carte) {
        lesCartesCitys.add(carte);
    }

    public static void addVilleInMap(CarteVille carte) {
        lesVilleInMap.add(carte);
    }

    public static void addRailsInMap(CarteRail carte) {
        lesRailsInMap.add(carte);
    }

    public static void addMapInMap(CarteMap carte) {
        lesMapInMap.add(carte);
    }

    //METHODE
    public static void otherPhase() {
        phase2 = false;
        phase3 = false;
    }

    //permet de jouer la phase 2
    public static void phase2() {
        phase2 = true;
        phase3 = false;
        //on sauvegarde les chemin du tour precedent
        lesCheminsTrouPrecednt.clear();
        lesCheminsTrouPrecednt.addAll(lesChemins);
        //permet d'effaccer les dernier rail posé
        LesRailPose.clear();
    }

    //permet de jouer la phase 3
    public static void phase3() {
        phase2 = false;
        phase3 = true;
        //debut de phase 3, on construit les arbre de chaque ville
        constructAST();

    }

    //pemret de definir un ephase de test
    public static void test() {
        test = true;
    }

    //permet au joueur d'être en pause
    public static void pause() {
        pause = true;
        ControlerInformation.showInfo();
    }

    //permet d'arrêter le jeux
    public static void stop() {
        pause = true;
    }

    public static void hideInfo() {
        ControlerInformation.hideInfo();
    }

    //permet au joueur de jouer
    public static void joue() {
        pause = false;
        ControlerInformation.hideInfo();
        ControlerInformation.setTextInfo("à votre tour ");
    }

    //permet d'augmenter le niveau de al locomotive d'un joueur
    public static void miseJourInfo(String joueur, int point, int locomotive, int solde) {

        if (unJoueur.isEqualJoueur(joueur)) {
            unJoueur.setNivLocomotive(locomotive);
            unJoueur.setPointVictoire(point);
            unJoueur.setSolde(solde);
        } else {
            Joueur unJoueur = Controler.getIdJoueur(joueur);
            unJoueur.setNivLocomotive(locomotive);
            unJoueur.setPointVictoire(point);
            unJoueur.setSolde(solde);
        }
    }

    //permet d'augmenter le niveau de al locomotive d'un joueur
    public static void addLocomotive(String joueur) {
        unJoueur.addNivLocomotive();
    }

    public static void addPointVictoire(int pint) {
        unJoueur.setPointVictoire(pint);
    }

    public static void addSolde(int solde) {
        unJoueur.setSolde(solde);
    }

    //permet de mettre a jour le nombre de point
    public static void setPointJoueur(int point, String joueur) {
        int i = 0;
        while (i < Controler.getListJoueur().size() && !Controler.getListJoueur().get(i).isEqualJoueur(joueur)) {
            i++;
        }
        if (i < Controler.getListJoueur().size() && Controler.getListJoueur().get(i).isEqualJoueur(joueur)) {
            Controler.getListJoueur().get(i).addNivLocomotive();
        }
    }

    public static void dessinPlateau(Scene scene, String xmlMap, String xmlRail) {
        //effacer le dernier affchage
        ControlerInterfaceGraphique.effactePlateau();
        Group plateau = ControlerInterfaceGraphique.getRootPlateau();
        //bar d'info de la partie
        ControlerPlateauInfoPartie.initControlePlateauInfoPartie(plateau, scene);
        //affiche plateau
        ControlerPlateauMap.initControlerPlateauMap(scene.getWidth(), scene.getHeight(), xmlMap, xmlRail);
        ControlerPlateauMap.dessinerPlateau(plateau);
        plateau.getChildren().add(ControlerPlateauMap.getInterfacePlateauMap());
        ControlerNavBarPiece.initControleNavBarPiece(scene.getWidth(), scene.getHeight());
        plateau.getChildren().add(ControlerNavBarPiece.getInterfaceNavBarPiece());
        ControlerPlateauMap.dessineImageView(plateau);//ajoute des image au dessus des cartes du plateau
        //bar info du joueur
        ControlerPlateauInfoJoueur.initControlerPlateauInfoJoueur(scene.getWidth(), scene.getHeight());
        plateau.getChildren().add(ControlerPlateauInfoJoueur.dessinNavBarInfo());
        ControlerPlateauInfoJoueur.dessinInfoJoueur(Controler.getJoueurs());//permet de dessiner les informations des joueur présent sur la partie

        //affiche du plateau
        ControlerInterfaceGraphique.affichePlateau();
    }

    //permet de retrouver une carte placé sur le jeux grace a ça position
    public static CarteRail getCartePosRail(int x, int y) {
        int i = 0;
        while (i < lesRailsInMap.size() && lesRailsInMap.get(i).isEqualPosition(x, y) == false) {
            i++;
        }
        if (i < lesRailsInMap.size() && lesRailsInMap.get(i).isEqualPosition(x, y)) {
            return lesRailsInMap.get(i);
        } else {
            return null;
        }
    }

    //permet de retrouver une carte placé sur le jeux grace a ça position
    public static CarteMap getCartePosMap(int x, int y) {
        int i = 0;
        while (i < lesMapInMap.size() && lesMapInMap.get(i).isEqualPosition(x, y) == false) {
            i++;
        }
        if (i < lesMapInMap.size() && lesMapInMap.get(i).isEqualPosition(x, y)) {
            return lesMapInMap.get(i);
        } else {
            return null;
        }
    }

    //permet de retrouver une carte placé sur le jeux grace a ça position
    public static CarteVille getCartePosVille(int x, int y) {
        int i = 0;
        while (i < lesVilleInMap.size() && lesVilleInMap.get(i).isEqualPosition(x, y) == false) {
            i++;
        }
        if (i < lesVilleInMap.size() && lesVilleInMap.get(i).isEqualPosition(x, y)) {
            return lesVilleInMap.get(i);
        } else {
            return null;
        }
    }

    //cette fnction permet de retrouver la carte rail correspondant a l'image
    protected static CarteRail getIdCarteRail(ImageView imageView) {
        int i = 0;
        while (i < lesCartesRails.size() && lesCartesRails.get(i).isEqualImageView(imageView) == false) {
            i++;
        }
        if (i < lesCartesRails.size() && lesCartesRails.get(i).isEqualImageView(imageView)) {
            return lesCartesRails.get(i);
        } else {
            return null;
        }
    }

    //cette fnction permet de retrouver la carte rail correspondant a l'image
    protected static CarteRail getIdCarteRailinMap(ImageView imageView) {
        int i = 0;
        while (i < lesRailsInMap.size() && lesRailsInMap.get(i).isEqualImageView(imageView) == false) {
            i++;
        }
        if (i < lesRailsInMap.size() && lesRailsInMap.get(i).isEqualImageView(imageView)) {
            return lesRailsInMap.get(i);
        } else {
            return null;
        }
    }

    //cette fnction permet de retrouver la carte rail correspondant a l'image
    protected static CarteRail getIdCarteRailinMap(int id) {
        int i = 0;
        while (i < lesRailsInMap.size() && lesRailsInMap.get(i).isEqualIdRail(id) == false) {
            i++;
        }
        if (i < lesRailsInMap.size() && lesRailsInMap.get(i).isEqualIdRail(id)) {
            return lesRailsInMap.get(i);
        } else {
            return null;
        }
    }

    //cette fnction permet de retrouver la carte rail correspondant a l'image
    /*protected static CarteRail getIdCarteRailinMap(ImageView imageView) {
     int i = 0;
     while (i < lesRailsInMap.size() && lesRailsInMap.get(i).isEqualImageView(imageView) == false) {
     i++;
     }
     if (i < lesRailsInMap.size() && lesRailsInMap.get(i).isEqualImageView(imageView)) {
     return lesRailsInMap.get(i);
     } else {
     return null;
     }
     }

     //cette fnction permet de retrouver la carte rail correspondant a l'image
     protected static CarteRail getIdCarteRailinMap(int id) {
     int i = 0;
     while (i < lesRailsInMap.size() && lesRailsInMap.get(i).isEqualIdRail(id) == false) {
     i++;
     }
     if (i < lesRailsInMap.size() && lesRailsInMap.get(i).isEqualIdRail(id)) {
     return lesRailsInMap.get(i);
     } else {
     return null;
     }
     }*/
    //cette fnction permet de retrouver la carte rail correspondant a l'image
    protected static CarteRail getIdCarteRail(String name) {
        int i = 0;
        while (i < lesCartesRails.size() && lesCartesRails.get(i).isEqualName(name) == false) {
            i++;
        }
        if (i < lesCartesRails.size() && lesCartesRails.get(i).isEqualName(name)) {
            return lesCartesRails.get(i);
        } else {
            return null;
        }
    }

    //cette fnction permet de retrouver la carte rail correspondant a son id
    protected static CarteRail getIdCarteRail(int id) {
        int i = 0;
        while (i < lesCartesRails.size() && lesCartesRails.get(i).isEqualIdRail(id) == false) {
            i++;
        }
        if (i < lesCartesRails.size() && lesCartesRails.get(i).isEqualIdRail(id)) {
            return lesCartesRails.get(i);
        } else {
            return null;
        }
    }

    //cette fnction permet de retrouver la carte rail correspondant a son id
    protected static CarteCity getIdCarteCity(int id) {
        int i = 0;
        while (i < lesCartesCitys.size() && lesCartesCitys.get(i).isEqualIdCity(id) == false) {
            i++;
        }
        if (i < lesCartesCitys.size() && lesCartesCitys.get(i).isEqualIdCity(id)) {
            return lesCartesCitys.get(i);
        } else {
            return null;
        }
    }

    //cette fnction permet de retrouver la carte city(CarteVille) correspondant a l'image
    protected static CarteCity getIdCarteCity(ImageView imageView) {
        int i = 0;
        while (i < lesCartesCitys.size() && lesCartesCitys.get(i).isEqualImageView(imageView) == false) {
            i++;
        }
        if (i < lesCartesCitys.size() && lesCartesCitys.get(i).isEqualImageView(imageView)) {
            return lesCartesCitys.get(i);
        } else {
            return null;
        }
    }

    //permet de rtourné la carte correspondant a l'image passé en parmètre
    protected static CarteMap getIdCarteMap(ImageView imageView) {
        int i = 0;
        while (i < lesMapInMap.size() && lesMapInMap.get(i).isEqualImageViewMap(imageView) == false) {
            i++;
        }
        if (i < lesMapInMap.size() && lesMapInMap.get(i).isEqualImageViewMap(imageView)) {
            return lesMapInMap.get(i);
        } else {
            return null;
        }
    }

    //permet de rtourné la carte correspondant a l'image passé en parmètre
    protected static CarteVille getIdCarteVille(ImageView imageView) {
        int i = 0;
        while (i < lesVilleInMap.size() && lesVilleInMap.get(i).isEqualImageViewCarteVille(imageView) == false) {
            i++;
        }
        if (i < lesVilleInMap.size() && lesVilleInMap.get(i).isEqualImageViewCarteVille(imageView)) {
            return lesVilleInMap.get(i);
        } else {
            return null;
        }
    }

    //permet de retrouver un cube de marchandise grace a ça position
    public static Marchandise getMarchandise(double x, double y) {
        int i = 0;
        while (i < lesMarchandises.size() && lesMarchandises.get(i).isEqualPosition(x, y) == false) {
            i++;
        }
        if (i < lesMarchandises.size() && lesMarchandises.get(i).isEqualPosition(x, y)) {
            return lesMarchandises.get(i);
        } else {
            return null;
        }
    }

    //permet de retrouver un cube de marchandise grace a ça position
    public static Marchandise getIdMarchandise(Rectangle cube) {
        int i = 0;
        while (i < lesMarchandises.size() && lesMarchandises.get(i).isEqualRectangle(cube) == false) {
            i++;
        }
        if (i < lesMarchandises.size() && lesMarchandises.get(i).isEqualRectangle(cube)) {
            return lesMarchandises.get(i);
        } else {
            return null;
        }
    }

    //pemret de construire les arbres au debit de chaque tour
    public static void constructAST() {
        ArrayList<Chemin> lesCheminFini = getCheminFini();
        if (lesCheminFini.size() > 0) {
            //construction des nouveau arbre pour chaque ville
            if (lesCheminsTrouPrecednt.containsAll(lesCheminFini) == false) { //si aucun chemin ajouté alors on ne construit pas de nouvelle arbre
                //on refait tous les arbres de chaque ville du plateau
                lesArbresVilles.clear();
                //on reconstruit les arbre de toute les villes.
                ArrayList<CarteVille> villeParcourue = new ArrayList<CarteVille>();
                for (int i = 0; i < lesCheminFini.size(); i++) {
                    //on verifie que l'arbre de la ville n'a pas deja etait construit
                    CarteVille villeDep = lesCheminFini.get(i).getVilleDep();
                    CarteVille villeArr = lesCheminFini.get(i).getVilleArr();
                    if (villeParcourue.contains(villeDep) == false) {
                        //construction de l'arbre pour la ville de dep
                        Arbre AST_VilleDep = new Arbre();
                        lesArbresVilles.add(AST_VilleDep);
                        AST_VilleDep.constructArbre(lesCheminFini, villeDep);
                        villeParcourue.add(villeDep);
                    }
                    if (villeParcourue.contains(villeArr) == false) {
                        //construction de l'arbre pour la ville d'arrivé
                        Arbre AST_VilleArr = new Arbre();
                        lesArbresVilles.add(AST_VilleArr);
                        AST_VilleArr.constructArbre(lesCheminFini, villeArr);
                        villeParcourue.add(villeArr);//ajout de villeDep et non pas villeAr, car les chemins on etait renverser pendant la construction de l'arbre
                    }
                    //On met a jour la liste des arbre pour les villes construite
                }
            }
        }
    }

    //permet de recuperer les chemin fini (villeArr + villeDep)
    private static ArrayList<Chemin> getCheminFini() {
        ArrayList<Chemin> lesCheminFini = new ArrayList<>();
        for (int i = 0; i < lesChemins.size(); i++) {
            if (lesChemins.get(i).getVilleArr() != null && lesChemins.get(i).getVilleDep() != null) {
                lesCheminFini.add(lesChemins.get(i));
            }
        }
        return lesCheminFini;
    }

    //permet de retrouver un cube de marchandise grace a ça position
    //permet de recuperer la carteTuile correspondante
    public static String getCarteTuile(String carte) {
        return lesTuilesActions.get(carte);
    }
    public static void fini() {
        pause = true;
        ControlerInformation.setTextInfo("fin du tour !");
        ControlerInformation.showInfo();
        sendMessage("fin");
    }

    //envoie d'un message au serveur
    protected static void sendMessage(String message) {
        //permet d'envoyer un message au serveur
        Controler.sendMessageServeur(message);
    }

    protected static int getIdArbreVille(CarteVille ville) {
        int i = 0;
        while (i < lesArbresVilles.size() && lesArbresVilles.get(i).isEqualVille(ville) == false) {
            i++;
        }
        if (i < lesArbresVilles.size() && lesArbresVilles.get(i).isEqualVille(ville)) {
            return i;
        } else {
            return -1;
        }
    }

    protected static void addPoseRail(CarteRail carteRail, CarteMap carteMap) {
        LesRailPose.add(carteMap);
        LesRailPose.add(carteRail);
    }

    protected static void addPoseCity(CarteCity carteCity, CarteMap carteMap) {
        LesRailPose.add(carteMap);
        LesRailPose.add(carteCity);
    }

    public static int calculPaiement() {
        //calcule du paiment du a la banque
        int solde = 0;
        for (int i = 0; i < LesRailPose.size(); i++) {
            if (i % 2 == 0) {
                if (LesRailPose.get(i).getName().equals("plaine")==false) {
                    solde = solde + 2;
                }
            } else {
                if (LesRailPose.get(i).getName().contains("rail")) {
                    CarteRail carte = ((CarteRail) LesRailPose.get(i));
                    solde = solde + (carte.getNbSortie()-1);
                }else if (LesRailPose.get(i).getName().contains("city")){
                    solde = solde + 3;
                }
            }
        }
        solde = unJoueur.getSolde() - solde;
        unJoueur.setSolde(solde);
        return solde;
    }
    public static int getLevelLocomotive(){
        return unJoueur.getNivLocomotive();
    }
    public static int getNbPointVictoire(){
        return unJoueur.getPointVictoire();
    }
}
