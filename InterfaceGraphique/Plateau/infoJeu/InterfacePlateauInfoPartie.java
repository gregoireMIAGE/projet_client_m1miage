/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Plateau.infoJeu;

import java.util.ArrayList;
import java.util.LinkedList;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import projet_java_gl_reseau.player.Joueur;

/**
 *
 * @author greg
 */
public class InterfacePlateauInfoPartie extends Group {

    private double widthFenetre;
    private double heightFenetre;

    private Text phase;
    private Text tour;

    private Group ordreTour;

    //rectangle d'inofmration nav bar
    private Rectangle infoPartie;

    public InterfacePlateauInfoPartie(double widthFenetre, double heightFenetre) {
        this.widthFenetre = widthFenetre;
        this.heightFenetre = heightFenetre;

        this.infoPartie = new Rectangle();
        this.infoPartie.setWidth(widthFenetre);
        this.infoPartie.setHeight(60);
        this.infoPartie.setX(0);
        this.infoPartie.setY(0);
        this.infoPartie.setFill(Color.color(0, 0, 0));
        //this.infoPartie.setOpacity(0.8);
        this.infoPartie.setStrokeWidth(5);

        this.phase = new Text("phase : 1");
        this.phase.setFill(Color.WHITE);
        this.phase.setFont(Font.font(30));
        this.phase.minHeight(20);
        this.phase.setTranslateX(this.infoPartie.getWidth()*0.80);
        this.phase.setTranslateY(40);

        this.tour = new Text("tour : 1");
        this.tour.setFill(Color.WHITE);
        this.tour.setFont(Font.font(30));
        this.tour.minHeight(20);
        this.tour.setTranslateX(this.infoPartie.getWidth()*0.60);
        this.tour.setTranslateY(40);

        this.ordreTour = new Group();
    }

    public void dessinInfoBarPartie() {
        //dessin bar info partie
        this.getChildren().add(this.infoPartie);
        this.getChildren().add(this.phase);
        this.getChildren().add(this.tour);
        this.getChildren().add(this.ordreTour);
    }

    //permet d'afficher les tour
    public void afficheTour(String tour) {
        //affiche image tour
        this.tour.setText("Tour : "+tour);
    }

    //permet d'afficher les tour
    public void affichePhase(String name) {
        phase.setText("Phase : "+name);
    }

    //pemret d'afficher l'ordre du tour
    public void afficheOrdreTour(LinkedList<Joueur> lesJoueur) {
        for (int i = 0; i < lesJoueur.size(); i++) {
            ImageView perso = new ImageView(new Image("file:" + System.getProperty("user.dir") + "/src/projet_java_gl_reseau/config/images/perso"+String.valueOf(i+1)+".png"));
            perso.setFitWidth(60);
            perso.setFitHeight(60);
            perso.setTranslateX(10 + 100*i);
            perso.setTranslateY(20);
            perso.setVisible(true);
            
            Text name = new Text(lesJoueur.get(i).getName());
            name.setFont(Font.font(16));
            name.setFill(Color.WHITE);
            name.setTranslateX(10 + 100*i);
            name.setTranslateY(14);

            this.ordreTour.getChildren().add(perso);
            this.ordreTour.getChildren().add(name);
        }
    }
}
