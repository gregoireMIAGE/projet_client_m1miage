/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Plateau.infoJeu;

import java.util.ArrayList;
import java.util.LinkedList;
import javafx.scene.Group;
import javafx.scene.Scene;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.ControlerPlateau;
import projet_java_gl_reseau.player.Joueur;

/**
 *
 * @author greg
 */
public class ControlerPlateauInfoPartie extends ControlerPlateau{
   private static InterfacePlateauInfoPartie  interfacePlateauInfoPartie;
   
   public static void initControlePlateauInfoPartie(Group root, Scene scene){
       interfacePlateauInfoPartie = new InterfacePlateauInfoPartie(scene.getWidth(), scene.getHeight());
       dessineControlerPlateauInfoPartie();
       //root.getChildren().add(interfacePlateauInfoPartie);
   }
   public static void dessineControlerPlateauInfoPartie(){
       interfacePlateauInfoPartie.dessinInfoBarPartie();
   }
   
   public static Group getInterfacePlateauInfoPartie(){
       return interfacePlateauInfoPartie;
   }
   
   public static void setOrdreTour(LinkedList<Joueur> lesJoueur){
       interfacePlateauInfoPartie.afficheOrdreTour(lesJoueur);
   }
}
