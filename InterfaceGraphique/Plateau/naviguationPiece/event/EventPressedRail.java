/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Plateau.naviguationPiece.event;

import java.util.LinkedList;
import javafx.event.EventHandler;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.naviguationPiece.ControlerNavBarPiece;
import projet_java_gl_reseau.Jeu.lesCarte.CarteCity;
import projet_java_gl_reseau.Jeu.lesCarte.CarteRail;

/**
 *
 * @author greg
 */
public class EventPressedRail extends ControlerNavBarPiece implements EventHandler<MouseEvent>{
    
    @Override
    public void handle(MouseEvent event) {
        if (!pause && phase2) {
                    //recuperation de la carte 
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            CarteRail carte = getIdCarteRail(((ImageView) (event.getSource())));
            courantRail = carte;
            orgSceneX = event.getSceneX();
            orgSceneY = event.getSceneY();
            orgTranslateX = ((ImageView) (event.getSource())).getTranslateX();//pour move
            orgTranslateY = ((ImageView) (event.getSource())).getTranslateY();//pour move

            if (courantRail.getImageViw().getFitHeight() == 50) {
                //on deplace l'image select de la carte pressé
                interfaceNavBarPiece.moveImageSelect(courantRail.getImageViw().getTranslateX(), courantRail.getImageViw().getTranslateY());
                interfaceNavBarPiece.showImageSelect();
            }
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
    

    
}
