/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Plateau.naviguationPiece.event;

import javafx.event.EventHandler;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.naviguationPiece.ControlerNavBarPiece;

/**
 *
 * @author greg
 */
public class EventDraggedCity extends ControlerNavBarPiece implements EventHandler<MouseEvent>{

    @Override
    public void handle(MouseEvent event) {
            if (!pause) {
                double offsetX = event.getSceneX() - orgSceneX;
                double offsetY = event.getSceneY() - orgSceneY;
                double newTranslateX = orgTranslateX + offsetX;
                double newTranslateY = orgTranslateY + offsetY;

                ((ImageView) (event.getSource())).setTranslateX(newTranslateX);
                ((ImageView) (event.getSource())).setTranslateY(newTranslateY);
                courantCity = getIdCarteCity(((ImageView) (event.getSource())));
            }
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
