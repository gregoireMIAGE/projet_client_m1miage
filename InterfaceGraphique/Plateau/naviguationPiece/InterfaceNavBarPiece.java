/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Plateau.naviguationPiece;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import static projet_java_gl_reseau.InterfaceGraphique.Plateau.naviguationPiece.ControlerNavBarPiece.imgRailCarteMoveMarchandise;
import projet_java_gl_reseau.Jeu.lesCarte.CarteCity;
import projet_java_gl_reseau.Jeu.lesCarte.CarteRail;

/**
 *
 * @author greg
 */
public class InterfaceNavBarPiece extends Group {

    private double widthFenetre;
    private double heightFenetre;

    //hashmap des text correspondant aux nombre de carte pour chaque piece
    private HashMap<String, Text> lesNbCartes;
    private HashMap<String, Circle> lesCirclesCartes;

    //image de selection qui apparit lorqu'on selectionne une carte
    private ImageView imgViewSelect;

    public InterfaceNavBarPiece(double widthFenetre, double heightFenetre) {
        this.widthFenetre = widthFenetre;
        this.heightFenetre = heightFenetre;
        this.lesNbCartes = new HashMap<>();
        this.lesCirclesCartes = new HashMap<>();
        //image de selecttion de la map; en premier car en dessous de toutes les autres carte
        this.imgViewSelect = new ImageView(new Image("file:" + System.getProperty("user.dir") + "/src/projet_java_gl_reseau/config/images/partie_plateau_select.png"));
        this.imgViewSelect.setFitHeight(50);
        this.imgViewSelect.setFitWidth(50);
        this.imgViewSelect.setRotate(this.imgViewSelect.getRotate() + 30);
        this.imgViewSelect.setVisible(false);

        //pemret de dessiner le rectangle
        dessine();
    }

    //ACCESSEUR
    public void setTextNbCarte(String nameCarte) {
        int nb = Integer.parseInt(this.lesNbCartes.get(nameCarte).getText()) - 1;
        this.lesNbCartes.get(nameCarte).setText(String.valueOf(nb));
    }

    //permet de bouger l'image select carte
    public void moveImageSelect(double x, double y) {
        this.imgViewSelect.setTranslateX(x);
        this.imgViewSelect.setTranslateY(y);
    }

    //permet d'afficher l'image select
    public void showImageSelect() {
        this.imgViewSelect.setVisible(true);
    }

    //permet d'afficher l'image select
    public void hideImageSelect() {
        this.imgViewSelect.setVisible(false);
    }

    //METHDOE
    private void dessine() {
        Rectangle navBar = new Rectangle();
        navBar.setWidth(100);
        navBar.setHeight(this.heightFenetre);
        navBar.setX(Screen.getPrimary().getBounds().getWidth() - navBar.getWidth());
        navBar.setY(0);
        navBar.setFill(Color.color(0, 0, 0));
        //navBar.setOpacity(0.8);
        navBar.setStrokeWidth(5);
        navBar.setArcHeight(10);
        navBar.setArcWidth(10);

        this.getChildren().add(navBar);
    }

    //permet d'ajouter toutes les cartes rails

    public void dessineCartesRails(LinkedList<CarteRail> lesCartesRailsXml) {
        for (int i = 0; i < lesCartesRailsXml.size(); i++) {
            ImageView image = createImageViewRails(lesCartesRailsXml.get(i));
            //ajout d'une carte rail dans le jeux
            //CarteRail carte = new CarteRail(lesCartesRailsXml.get(i).getName(), lesCartesRailsXml.get(i).getLocation(), lesCartesRailsXml.get(i).getPlace(), lesCartesRailsXml.get(i).getPointAttach());
            //carte.setImageViewCarteRail(image);
            //ControleNavBarPiece.addCarteRail(carte);//on ajoute la carte a la liste de carte rail présent dan sle jeux
            //on ajoute l'image a afficher
            this.getChildren().add(image);
        }
    }

    //permet d'ajouter toutes les cartes citys
    public void dessineCartesCitys(LinkedList<CarteCity> lesCartesCitysXml) {
        for (int i = 0; i < lesCartesCitysXml.size(); i++) {
            ImageView image = createImageViewCitys(lesCartesCitysXml.get(i));
            //ajout d'une carte city dans le jeux
            //CarteCity carte = new CarteCity(lesCartesCitysXml.get(i).getName(), lesCartesCitysXml.get(i).getLocation(), lesCartesCitysXml.get(i).getNiveau(), lesCartesCitysXml.get(i).getPlace());
            //carte.setImageViewCarteCity(image);
            //ControleNavBarPiece.addCarteCity(carte);//on ajoute la carte a la liste de carte rail présent dan sle jeux
            //on ajoute l'image a afficher
            this.getChildren().add(image);
        }
    }

    //permet de dessiner tous les circle et text 
    public void dessineCircleText() {
        ArrayList<Circle> lesCircle = new ArrayList<Circle>();
        lesCircle.addAll(this.lesCirclesCartes.values());
        ArrayList<Text> lesText = new ArrayList<Text>();
        lesText.addAll(this.lesNbCartes.values());

        for (int i = 0; i < this.lesCirclesCartes.size(); i++) {
            this.getChildren().add(lesCircle.get(i));
            this.getChildren().add(lesText.get(i));
        }
    }

    //permet de crer les cercle et texte nombre de chaque rail affiché
    public void createCircleTextNbRail(String nameCarte, int i, String nbCarte) {
        Circle cercleRailDroit = new Circle(this.widthFenetre - 15, 40 + (i * 50), 10);
        cercleRailDroit.setFill(Color.WHITE);
        this.lesCirclesCartes.put(nameCarte, cercleRailDroit);

        //ajout du texte
        this.createText(nameCarte, cercleRailDroit.getCenterX(), cercleRailDroit.getCenterY(), nbCarte);
    }

    public void createCircleTextNbCity(String nameCarte, int i, String nbCarte) {
        Circle cercleRailDroit = new Circle(this.widthFenetre - 65, 40 + (i * 50), 10);
        cercleRailDroit.setFill(Color.WHITE);
        this.lesCirclesCartes.put(nameCarte, cercleRailDroit);

        //ajout du texte
        this.createText(nameCarte, cercleRailDroit.getCenterX(), cercleRailDroit.getCenterY(), nbCarte);
    }

    private void createText(String nameCarte, double x, double y, String nbCarte) {
        Text textNb = new Text(x - 5, y + 5, nbCarte);
        this.lesNbCartes.put(nameCarte, textNb);
    }

    //permet de crer l'image du rail a afficher sur la map
    private ImageView createImageViewRails(CarteRail carte) {
        //carte rail du plateau
        ImageView imageView = carte.dessinerCartes(this.widthFenetre - 50, 50, 50);
        imageView.setRotate(imageView.getRotate() + 15);
        imageView.setOnMousePressed(ControlerNavBarPiece.imgRailOnMousePressedEventHandler);
        imageView.setOnMouseDragged(ControlerNavBarPiece.imgRailOnMouseDraggedEventHandler);
        imageView.setOnMouseEntered(ControlerNavBarPiece.imgRailCarteMoveMarchandise);
        carte.setImageViewCarteRail(imageView); //ajout de l'image conrespondant a la carte 
        return imageView;
    }

    //permet de crer l'image du rail a afficher sur la map
    private ImageView createImageViewCitys(CarteCity carte) {
        //carte rail du plateau
        ImageView imageView = carte.dessinerCartes(this.widthFenetre - 100, 50, 50);
        imageView.setOnMousePressed(ControlerNavBarPiece.imgCityOnMousePressedEventHandler);
        imageView.setOnMouseDragged(ControlerNavBarPiece.imgCityOnMouseDraggedEventHandler);
        carte.setImageViewCarteCity(imageView); //ajout de l'image conrespondant a la carte 
        return imageView;
    }

    //permet d'ajouter l'image de selection
    public void addImageViewSelect() {
        this.getChildren().add(this.imgViewSelect);
    }

    //mise a jour du nombre de carte
    public void updateNbCarte(String carteName, String op) {
        Text textNb = this.lesNbCartes.get(carteName);
        textNb.setText(String.valueOf(Integer.parseInt(textNb.getText()) + Integer.parseInt(op + "1")));
    }

    //ajout des cubes sur les carte city
    public void dessineMarchandise(Rectangle rectangle) {
        this.getChildren().add(rectangle);
    }

}
