/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Plateau.naviguationPiece;

import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.Element;
import java.util.ArrayList;
import java.util.List;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.naviguationPiece.event.EventPressedRail;
import javafx.scene.Group;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.ControlerPlateau;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.map.ControlerPlateauMap;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.naviguationPiece.event.EventDraggedCity;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.naviguationPiece.event.EventDraggedRail;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.map.event.EventEnteredCarteRailMoveMarchandise;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.naviguationPiece.event.EventPressedCity;
import projet_java_gl_reseau.Jeu.Chemin;
import projet_java_gl_reseau.Jeu.lesCarte.CarteCity;
import projet_java_gl_reseau.Jeu.lesCarte.CarteMap;
import projet_java_gl_reseau.Jeu.lesCarte.CarteRail;
import projet_java_gl_reseau.Jeu.lesCarte.CarteVille;
import projet_java_gl_reseau.XML.LectureXML;
import projet_java_gl_reseau.outil.ConvertArray;

/**
 *
 * @author greg
 */
public class ControlerNavBarPiece extends ControlerPlateau {

    //ATTRIBUT
    protected static InterfaceNavBarPiece interfaceNavBarPiece;

    //event hendler pressed
    protected static EventPressedRail imgRailOnMousePressedEventHandler;
    protected static EventDraggedRail imgRailOnMouseDraggedEventHandler;
    protected static EventPressedCity imgCityOnMousePressedEventHandler;
    protected static EventDraggedCity imgCityOnMouseDraggedEventHandler;
    protected static EventEnteredCarteRailMoveMarchandise imgRailCarteMoveMarchandise;

    //METHODE
    public static void initControleNavBarPiece(double width, double height) {
        interfaceNavBarPiece = new InterfaceNavBarPiece(width, height);
        imgRailOnMousePressedEventHandler = new EventPressedRail();
        imgRailOnMouseDraggedEventHandler = new EventDraggedRail();
        imgCityOnMousePressedEventHandler = new EventPressedCity();
        imgCityOnMouseDraggedEventHandler = new EventDraggedCity();
        imgRailCarteMoveMarchandise = new EventEnteredCarteRailMoveMarchandise();

        //recuperer les crates rails et citys depuis le fichier xml
        decryptXmlCartesRails(LectureXML.lireXmlCartesRails(), 30);
        decryptXmlCartesCitys(LectureXML.lireXmlCartesCitys(), 3);
        //dessiner les cartes recuperé
        interfaceNavBarPiece.dessineCartesRails(lesCartesRails);
        interfaceNavBarPiece.dessineCartesCitys(lesCartesCitys);
        interfaceNavBarPiece.addImageViewSelect();
        interfaceNavBarPiece.dessineCircleText();

    }

    //permet de recuperer la liste des Rail Contenue dans le fichier xml
    private static void decryptXmlCartesRails(Document racine, int nb) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        NodeList cartesRailNoeuds = racine.getElementsByTagName("carteRail");

        for (int i = 0; i < cartesRailNoeuds.getLength(); i++) {
            CarteRail carteRail = null;
            Element carte = (Element) cartesRailNoeuds.item(i).getChildNodes();
            String name = carte.getElementsByTagName("name").item(0).getTextContent();
            String location = "file:" + System.getProperty("user.dir") + carte.getElementsByTagName("location").item(0).getTextContent();
            int[] attach = ConvertArray.convertToIntArray(carte.getElementsByTagName("attach").item(0).getTextContent().split(","));
            //String gare = carte.getNextSibling().getNodeValue();
            String place = carte.getElementsByTagName("place").item(0).getTextContent();
            for (int a = 0; a < nb; a++) {
                carteRail = new CarteRail(name, location, i, attach, i * 10 + a);
                //ajout de la carte a la liste
                lesCartesRails.add(carteRail);
            }
            //permet de crer les cercle te texte avec les cartes Rails
            interfaceNavBarPiece.createCircleTextNbRail(carteRail.getName(), i, String.valueOf(nb));
        }
    }

    //permet de recuperer la liste des Cite Contenue dans le fichier xml
    private static void decryptXmlCartesCitys(Document racine, int nb) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        NodeList cartesCityss = racine.getElementsByTagName("carte");
        CarteCity carteCity = null;
        for (int i = 0; i < cartesCityss.getLength(); i++) {
            Element carte = (Element) cartesCityss.item(i).getChildNodes();
            String key = carte.getElementsByTagName("key").item(0).getTextContent();
            String name = carte.getElementsByTagName("name").item(0).getTextContent();
            String location = "file:" + System.getProperty("user.dir") + carte.getElementsByTagName("location").item(0).getTextContent();
            int niveau = Integer.parseInt(carte.getElementsByTagName("niveau").item(0).getTextContent());
            //String gare = carte.getNextSibling().getNodeValue();
            String place = carte.getElementsByTagName("place").item(0).getTextContent();
            //insertion des carte citys
            Color color = Color.valueOf(carte.getElementsByTagName("color").item(0).getTextContent());
            for (int a = 0; a < nb; a++) {
                carteCity = new CarteCity(name, location, niveau, i, key, i * 10 + a);
                carteCity.setColor(color);
                lesCartesCitys.add(carteCity);
            }
            interfaceNavBarPiece.createCircleTextNbCity(carteCity.getName(), i, String.valueOf(nb));
        }
    }

    //permet de mettre a jour le nombre de carte 
    public static void miseJourNbCarte(String carteName) {
        interfaceNavBarPiece.setTextNbCarte(carteName);
    }

    //permet de retourner l'affichage complet 
    public static Group getInterfaceNavBarPiece() {
        return interfaceNavBarPiece;
    }

    //permet de mettre en pause
    public static void pause() {
        pause = true;
    }

    //permet de mettre en pause
    public static void joue() {
        pause = false;
    }

    public static void hideImageSelect() {
        interfaceNavBarPiece.hideImageSelect();
    }

    public static void updateNbCarte(String carte, String op) {
        interfaceNavBarPiece.updateNbCarte(carte, op);
    }

    public static void moveCard(String typeCard, int idCard, int translatex, int translatey, double widthHeight) {
        System.out.println(idCard + "," + translatex + "," + translatey + "," + widthHeight);
        if (typeCard.equals("rail")) {
            CarteRail carteRail = getIdCarteRail(idCard);
            CarteMap carteMap = getCartePosMap(translatex, translatey);

            courantCarteMap = getCartePosMap(translatex, translatey);
            courantRail = getIdCarteRail(idCard);
            //permet de modifier la carte
            carteRail.getImageViw().setTranslateX(carteMap.getImageViw().getTranslateX());
            carteRail.getImageViw().setTranslateY(carteMap.getImageViw().getTranslateY());
            carteRail.getImageViw().setFitHeight(widthHeight);
            carteRail.getImageViw().setFitWidth(widthHeight);

            validePointAttach(translatex, translatey);
        } else {
            courantCarteMap = getCartePosMap(translatex, translatey);
            courantCity = getIdCarteCity(idCard);

            courantCity.getImageViw().setTranslateX(courantCarteMap.getImageViw().getTranslateX());
            courantCity.getImageViw().setTranslateY(courantCarteMap.getImageViw().getTranslateY());
            courantCity.getImageViw().setFitHeight(widthHeight);
            courantCity.getImageViw().setFitWidth(widthHeight);
        }
    }

    private static boolean validePointAttach(int x, int y) {
        //on initialise les arraylist pour eviter les redondance
        attachCourantRails.clear();
        attachCourantVilles.clear();

        int carteRailHaut = 0;
        int carteRailBas = 0;
        int carteRailBasGauche = 0;
        int carteRailBasDroite = 0;
        int carteRailHautGauche = 0;
        int carteRailHautDroite = 0;
        //recuperation des carte alentour et verification du match possible
        CarteRail rail;
        CarteVille ville;
        if (y > 0) {
            if (map[y - 1][x].equalsIgnoreCase("R")) {
                rail = getCartePosRail(x, y - 1);
                if ((carteRailHaut = rail.getPointAttach(4)) == 1) {
                    attachCourantRails.add(rail);//permet de recuperer le rail match
                }
            } else if (map[y - 1][x].equalsIgnoreCase("V")) {
                carteRailHaut = getCartePosMap(x, y - 1).getConstruct();
            } else if (map[y - 1][x].contains("V")) {
                carteRailHaut = 1;
                attachCourantVilles.add(getCartePosVille(x, y - 1));//permet de recuperer la ville match
            }
        }

        if (y < map.length - 1) {
            if (map[y + 1][x].equalsIgnoreCase("R")) {
                rail = getCartePosRail(x, y + 1);
                if ((carteRailBas = rail.getPointAttach(1)) == 1) {
                    attachCourantRails.add(rail);//permet de recuperer le rail match
                }
            } else if (map[y + 1][x].equalsIgnoreCase("V")) {
                carteRailBas = getCartePosMap(x, y + 1).getConstruct();
            } else if (map[y + 1][x].contains("V")) {
                carteRailBas = 1;
                attachCourantVilles.add(getCartePosVille(x, y + 1));
            }
        }

        if (x > 0 && (y - ((x - 1) % 2)) > -1) {
            if (map[y - ((x - 1) % 2)][x - 1].equalsIgnoreCase("R")) {
                rail = getCartePosRail(x - 1, y - ((x - 1) % 2));
                if ((carteRailHautGauche = rail.getPointAttach(3)) == 1) {
                    attachCourantRails.add(rail);
                }
            } else if (map[y - ((x - 1) % 2)][x - 1].equalsIgnoreCase("V")) {
                carteRailHautGauche = getCartePosMap(x - 1, y - ((x - 1) % 2)).getConstruct();
            } else if (map[y - ((x - 1) % 2)][x - 1].contains("V")) {
                carteRailHautGauche = 1;
                attachCourantVilles.add(getCartePosVille(x - 1, y - ((x - 1) % 2)));
            }
        }

        if (x < map[0].length - 1 && (y - ((x - 1) % 2)) > -1) {
            if (map[y - (x - 1) % 2][x + 1].equalsIgnoreCase("R")) {
                rail = getCartePosRail(x + 1, y - (x - 1) % 2);
                if ((carteRailHautDroite = rail.getPointAttach(5)) == 1) {
                    attachCourantRails.add(rail);
                }
            } else if (map[y - (x - 1) % 2][x + 1].equalsIgnoreCase("V")) {
                carteRailHautDroite = getCartePosMap(x + 1, y - (x - 1) % 2).getConstruct();
            } else if (map[y - (x - 1) % 2][x + 1].contains("V")) {
                carteRailHautDroite = 1;
                attachCourantVilles.add(getCartePosVille(x + 1, y - (x - 1) % 2));
            }
        }

        if (x > 0 && (y + (x % 2)) < map.length) {
            if (map[y + (x % 2)][x - 1].equalsIgnoreCase("R")) {
                rail = getCartePosRail(x - 1, y + (x % 2));
                if ((carteRailBasGauche = rail.getPointAttach(2)) == 1) {
                    attachCourantRails.add(rail);
                }
            } else if (map[y + (x % 2)][x - 1].equalsIgnoreCase("V")) {
                //carteRailBasGauche = (getCartePosMap(x - 1, y + (x % 2)).getConstruct()|getCartePosRail(x - 1, y + (x % 2)).getPointAttach(2));
                carteRailBasGauche = getCartePosMap(x - 1, y + (x % 2)).getConstruct();
            } else if (map[y + (x % 2)][x - 1].contains("V")) {
                carteRailBasGauche = 1;
                attachCourantVilles.add(getCartePosVille(x - 1, y + (x % 2)));
            }
        }

        if (x < map[0].length - 1 && (y + (x % 2)) < map.length) {
            if (map[y + (x % 2)][x + 1].equalsIgnoreCase("R")) {
                rail = getCartePosRail(x + 1, y + (x % 2));
                if ((carteRailBasDroite = rail.getPointAttach(0)) == 1) {
                    attachCourantRails.add(rail);
                }
            } else if (map[y + (x % 2)][x + 1].equalsIgnoreCase("V")) {
                carteRailHaut = getCartePosMap(x + 1, y + (x % 2)).getConstruct();
            } else if (map[y + (x % 2)][x + 1].contains("V")) {
                carteRailBasDroite = 1;
                attachCourantVilles.add(getCartePosVille(x + 1, y + (x % 2)));
            }
        }

        //compare si oui ou non on peux attacher le rail
        return courantRail.comparePointAttach(new int[]{carteRailHautGauche, carteRailHaut, carteRailHautDroite, carteRailBasDroite, carteRailBas, carteRailBasGauche});

    }

    public static void validateCard(String typeCarte, int idCard, int translatex, int translatey) {
        //on recupere la carte

        CarteMap carteMap = getCartePosMap(translatex, translatey);

        //ajout de la carte aux chemins 
        //...
        if (typeCarte.equals("rail")) {
            if (courantRail != null && courantCarteMap != null) {
                CarteRail carteRail = getIdCarteRail(idCard);
                miseAjourNbCarteRail(carteRail);
                miseAjourMapRail(carteRail, carteMap, translatex, translatey);
                //mise a jour du chemin
                updateChemin();

                courantRail.setPose();
                courantRail = null;
                courantCarteMap = null;
            }
        } else if (typeCarte.equals("city")) {
            CarteCity carteCity = getIdCarteCity(idCard);
            miseAjourNbCarteCity(carteCity);
            miseAjourMapCity(carteCity, translatex, translatey);
            int index = addCarteCity(carteCity, carteMap, translatex, translatey);
            //addCubeCity(index);
            carteCity.setPose();
            courantCity = null;
            courantCarteMap = null;
        }

    }

    public static void rotateCard(int idCard, double rotate, String vector) {
        System.out.println(idCard + "," + rotate);

        CarteRail carteRail = getIdCarteRail(idCard);
        carteRail.getImageViw().setRotate(carteRail.getImageViw().getRotate() + rotate);
        carteRail.moveToArray(vector);
    }

    public static void closeCard(String typeCarte, int id) {
        if (typeCarte.equals("rail")) {
            CarteRail carteRail = getIdCarteRail(id);
            if (carteRail != null) {
                if (carteRail.getPose() == false) { //si le rail n'a pas encore etait posé
                    carteRail.getImageViw().setTranslateX(carteRail.getX());
                    carteRail.getImageViw().setTranslateY(carteRail.getY());
                    carteRail.getImageViw().setFitHeight(50);
                    carteRail.getImageViw().setFitWidth(50);
                    //mise a jou du nombre de carte
                    if (carteRail.getValidate() == true) {
                        carteRail.unvalidate();
                        updateNbCarte(carteRail.getNameRail(), "+");
                    }
                    //modification matrice
                    map[carteRail.getPositionYmap()][carteRail.getPositionXmap()] = mapDebut[carteRail.getPositionYmap()][carteRail.getPositionXmap()];
                    courantRail = null;
                    courantCarteMap = null;
                } else {//si le rail a deja etait posé
                    carteRail.getImageViw().setTranslateX(carteRail.getX());
                    carteRail.getImageViw().setTranslateY(carteRail.getY());
                }
                //suprime la carte de l'array list
                lesRailsInMap.remove(carteRail);
            }
        } else {
            //on replace la carte sur le coté
            CarteCity carteCity = getIdCarteCity(id);
            carteCity.getImageViw().setTranslateX(courantCity.getX());
            carteCity.getImageViw().setTranslateY(courantCity.getY());
            carteCity.getImageViw().setFitHeight(50);
            carteCity.getImageViw().setFitWidth(50);
            courantCity = null;
        }
    }

    //permet de mettre a jour le nombre de carte a afficher 
    private static void miseAjourNbCarteRail(CarteRail carte) {
        if (carte.getValidate() == false) {
            //permet de mettre a jour le nombre de carte 
            ControlerNavBarPiece.updateNbCarte(carte.getNameRail(), "-");
            carte.validate();
        }
    }

    //pemret mettre a jour les carte city
    private static void miseAjourNbCarteCity(CarteCity carte) {
        //permet de mettre a jour le nombre de carte 
        ControlerNavBarPiece.updateNbCarte(carte.getName(), "-");
    }

    //pemret de mettre a jour la map
    private static void miseAjourMapCity(CarteCity carte, int xMap, int yMap) {
        map[yMap][xMap] = carte.getKey();
        carte.setPositionXmap(xMap);
        carte.setPositionYmap(yMap);
    }

    //permet de mettre a jour les info de la map
    private static void miseAjourMapRail(CarteRail carteRail, CarteMap carteMap, int xMap, int yMap) {
        //modification de la map, mise a jour de la mapRail
        if (courantCarteMap != null) {//car si non ça veux dire que l'on a pas bougé la piece
                    /*if (courantRail.getPositionYmap() > -1 && courantCarteMap.getPositionX() > -1) {//si la piece a etait deplacé, on supprime de son ancien emplacement
             map[courantRail.getPositionYmap()][courantRail.getPositionXmap()] = "P";
             }*/
            map[yMap][xMap] = "R";
            courantRail.setPositionXmap(xMap);
            courantRail.setPositionYmap(yMap);
            //si la carte n'est pas deja dedans, si non cause d'erreur
            if (lesRailsInMap.contains(carteRail) == false) {
                lesRailsInMap.add(carteRail);
            }
        }
    }

    //permet d'ajouter, finaliser un chemin
    private static void updateChemin() {
        //creation ou recuperation du chemin coorespondant au rail
        if (attachCourantRails.size() > 0) {
            //donc on recupere le chemin existant pour un autre rail
            ArrayList<Chemin> chemins = new ArrayList<Chemin>();
            for (int i = 0; i < attachCourantRails.size(); i++) {
                chemins.addAll(attachCourantRails.get(i).getCheminAll());
            }

            //mise  jour des chemins et des rails
            if (attachCourantVilles.size() > 0) {
                //le chemin est un chemin terminé, on luis ajoute la ville (une seul ville possible dans l'arrayList)
                for (int i = 0; i < chemins.size(); i++) {
                    chemins.get(i).setVilleArrivee(attachCourantVilles.get(0));//mise a jour chemin
                    chemins.get(i).addCarteRail(courantRail);
                    //chemins.get(i).finished();
                    courantRail.addChemin(chemins.get(i));//ajout du chemin(pere)
                }
            } else {
                //le chemin continu a être construit, on lui ajoute le rail
                for (int i = 0; i < chemins.size(); i++) {
                    chemins.get(i).addCarteRail(courantRail);
                    courantRail.addChemin(chemins.get(i));
                }

            }
        } else if (attachCourantVilles.size() > 0) {
            //on crer un nouveau chemin avec une ville de depart
            if (attachCourantVilles.size() == 2) {
                //le chemin est un chemin fini (donc un rail deux ville)
                Chemin chemin = new Chemin(attachCourantVilles.get(0), attachCourantVilles.get(1), courantRail);
                //chemin.finished();
                lesChemins.add(chemin);
                //ajout du chemin au aurail (du pere au fils)
                courantRail.addChemin(chemin);

            } else {
                //le chemin est un chemin de depart (l'arraylist ville ne peux contenir qu'un chemin
                Chemin chemin = new Chemin(attachCourantVilles.get(0), courantRail);
                lesChemins.add(chemin);
                //ajout du chemin au aurail (du pere au fils)
                courantRail.addChemin(chemin);
            }
        }
    }

    //permet d'ajouter la carte a la liste des ville
    private static int addCarteCity(CarteCity carte, CarteMap carteMap, int xMap, int yMap) {
        CarteVille carteVille = new CarteVille(carte.getName(), carte.getLocation(), carte.getNiveau(), xMap, yMap);
        carteVille.setImageViewCarteVille(carte.getImageViw());
        lesVilleInMap.add(carteVille);
        carteMap.setConstruct();//indique que la carte city a etait construite (pour valide rail
        return lesVilleInMap.size() - 1;
    }

    private static void addCubeCity(int indexCrateVille) {
        if (lesMarchandisesReserves.size() > 0) {//si il reste des marchandise en reserve
            //suivat le niveau de la carte city, on ajoute les cubes
            CarteVille carteVille = lesVilleInMap.get(indexCrateVille);
            for (int i = 0; i < carteVille.getNiveau(); i++) {
                carteVille.addMarchandiseVille(lesMarchandisesReserves.get(0));
                lesMarchandisesReserves.get(0).setCarte(carteVille);

                lesMarchandises.add(lesMarchandisesReserves.get(0));//permet d'ajouter la marchandise reserves dans la marchandise active
                lesMarchandisesReserves.remove(0);
            }
            //dessin des marchandises. 
            ControlerPlateauMap.dessinMarchandiseCarte(carteVille);
        }
    }

    //permet de dessiner les marchandise sur la map de la carte en paramètre
    public static void dessinMarchandiseCarte(CarteVille carte) {
        //on dessine les cube de marchandise sur la carte
        ArrayList<Rectangle> lesRectangles = carte.dessinCubes();
        for (int a = 0; a < lesRectangles.size(); a++) {
            interfaceNavBarPiece.dessineMarchandise(lesRectangles.get(a));
        }
    }

}
