/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Connexion.event;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import projet_java_gl_reseau.InterfaceGraphique.AttenteJoueur.ControlerWaitPannel;
import projet_java_gl_reseau.InterfaceGraphique.Connexion.ControlerConnexion;
import projet_java_gl_reseau.InterfaceGraphique.ControlerInterfaceGraphique;
import projet_java_gl_reseau.controler.Controler;

/**
 *
 * @author greg
 */
public class EventPressedButtonConnexion extends ControlerConnexion implements EventHandler<MouseEvent> {

    public EventPressedButtonConnexion(){
        
    }
    @Override
    public void handle(MouseEvent event) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if (ControlerConnexion.connexion(joueur)) {
            //ajoute la nouvelle affichage
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    //permet d'afficher le pannel wait
                    ControlerInterfaceGraphique.afficheMenu();
                    Controler.start();//on lance le deamon pour recevoir les messages du serveur
                }
            });
        } else {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    interfaceConnexion.setVisibleLabelErreur(true);
                }
            });
        }
    }

}
