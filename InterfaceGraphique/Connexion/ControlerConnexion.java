/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Connexion;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import projet_java_gl_reseau.InterfaceGraphique.AttenteJoueur.ControlerWaitPannel;
import projet_java_gl_reseau.InterfaceGraphique.Connexion.event.EventPressedButtonConnexion;
import projet_java_gl_reseau.InterfaceGraphique.ControlerInterfaceGraphique;
import projet_java_gl_reseau.Projet_java_gl_reseau;
import projet_java_gl_reseau.player.Joueur;
import projet_java_gl_reseau.reseaux.Cryptage;
import projet_java_gl_reseau.reseaux.Reseau;
import static sun.security.jgss.GSSUtil.login;

/**
 *
 * @author greg
 */
public class ControlerConnexion {

    protected static InterfaceConnexion interfaceConnexion;
    protected static boolean connexionServeur;
    protected static Joueur joueur;
    protected static EventPressedButtonConnexion btnConnexion;

    //initialisation de l'a première page
    public static void init(double width, double height, Joueur joueurPar) {
        btnConnexion = new EventPressedButtonConnexion();
        interfaceConnexion = new InterfaceConnexion(width, height);
        ControlerInterfaceGraphique.getRootConnexion().getChildren().add(interfaceConnexion);
        joueur = joueurPar;
    }

    //permet de se connecter au serveur
    public static boolean connexion(Joueur joueur) {
        boolean ok = false;
        //si le serveur n'est pas disponible, on tente de se reconnecter
        if (connexionServeur == false) {
            initConnexionServeur();//on tente de se reconnecter
        }
        if (connexionServeur == true) {
            try {
                ok = Reseau.connexionServeur(interfaceConnexion.getLogin(), Cryptage.base64encode(interfaceConnexion.getPass()));
                joueur.setName(interfaceConnexion.getLogin());
            } catch (IOException ex) {
                interfaceConnexion.setTextLabelErreur("Login ou mot de pass incorecte !");
                //Logger.getLogger(Projet_java_gl_reseau.class.getName()).log(Level.SEVERE, null, ex);
                ok = false;

            } catch (ClassNotFoundException ex) {
                interfaceConnexion.setTextLabelErreur("Login ou mot de pass incorecte !");
                //Logger.getLogger(Projet_java_gl_reseau.class.getName()).log(Level.SEVERE, null, ex);
                ok = false;
            }
        }
        return ok;
    }

    //permet de se connecter au serveur
    private static void initConnexionServeur() {
        try {
            Reseau.initConnexionServeur();
            connexionServeur = true;
        } catch (IOException ex) {
            // Logger.getLogger(Projet_java_gl_reseau.class.getName()).log(Level.SEVERE, null, ex);
            interfaceConnexion.setTextLabelErreur("Serveur indisponible pour le moment. Veuillez réessayer plus tard !");
            interfaceConnexion.setVisibleLabelErreur(true);
            connexionServeur = false;
        }
    }
}
