/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Connexion;

import com.sun.javafx.scene.control.skin.ComboBoxListViewSkin;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Border;
import javafx.scene.text.Font;
import projet_java_gl_reseau.InterfaceGraphique.Connexion.event.EventPressedButtonConnexion;

/**
 *
 * @author greg
 */
public class InterfaceConnexion extends Group {

    private double widthFenetre;
    private double heightFenetre;

    private ImageView backGround;
    private Button btn;
    private PasswordField pass;
    private TextField login;
    private Label label;

    public InterfaceConnexion(double widthFenetre, double heightFenetre) {
        this.heightFenetre = heightFenetre;
        this.widthFenetre = widthFenetre;
        
        initBackGround();
        initTextFieldPass();
        initTextFieldLogin();
        initBtn();
        initLabel();
    }

    //permet d'initialiser le background
    private void initBackGround() {
        backGround = new ImageView(new Image("file:" + System.getProperty("user.dir") + "/src/projet_java_gl_reseau/config/images/background_pannel.jpg"));
        backGround.setFitHeight(this.heightFenetre);
        backGround.setFitWidth(this.widthFenetre);
        this.getChildren().add(this.backGround);
    }

    //permet d'initialiser le label
    private void initLabel() {
        label = new Label("Verifiez votre mot de passe et/ou login !");
        label.setFont(Font.font(20));
        final String cssDefaultLabel = "-fx-text-alignment: center;\n"
                + "-fx-text-fill: red;\n";
        label.setStyle(cssDefaultLabel);
        label.setTranslateX(this.widthFenetre / 2);
        label.setTranslateY(this.heightFenetre / 2 + 100);
        label.setVisible(false);
        this.getChildren().add(this.label);
    }

    //iniatialisation du bouton de connexion

    private void initBtn() {
        btn = new Button();
        btn.setText("Connexion");
        btn.setBorder(Border.EMPTY);
        btn.setMaxHeight(10);
        btn.setMaxWidth(200);
        btn.setTranslateX(this.widthFenetre / 2);
        btn.setTranslateY(this.heightFenetre / 2 + 60);
        final String cssDefault = "-fx-background-color: #767676;\n"
                + "-fx-text-alignment: center;\n"
                + "-fx-text-fill: white;\n";
        btn.setStyle(cssDefault);
        ControlerConnexion.btnConnexion = new EventPressedButtonConnexion();
        btn.setOnMouseClicked(ControlerConnexion.btnConnexion);

        this.getChildren().add(btn);
    }

    //initalise le textField du pass
    private void initTextFieldPass() {
        pass = new PasswordField();
        pass.setBorder(Border.EMPTY);
        pass.setMaxHeight(10);
        pass.setMaxWidth(200);
        pass.setTranslateX(this.widthFenetre / 2);
        pass.setTranslateY(this.heightFenetre / 2 + 20);
        pass.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                if (label.isVisible()) {
                    label.setVisible(false);
                }
            }

        });
        this.getChildren().add(pass);
    }

    //initalise le textField du login

    private void initTextFieldLogin() {
        login = new ComboBoxListViewSkin.FakeFocusTextField();
        login.setMaxHeight(10);
        login.setMaxWidth(200);
        login.setTranslateX(this.widthFenetre / 2);
        login.setTranslateY(this.heightFenetre / 2 - 20);
        login.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                if (label.isVisible()) {
                    label.setVisible(false);
                }
            }

        });
        this.getChildren().add(login);
    }

    //ACCESSEUR
    public String getPass() {
        return this.pass.getText();
    }
    public String getLogin() {
        return this.login.getText();
    }
    public void setTextLabelErreur(String text){
        this.label.setText(text);
    }
    public void setVisibleLabelErreur(boolean val){
        this.label.setVisible(val);
    }
}
