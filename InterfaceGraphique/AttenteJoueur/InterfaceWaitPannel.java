/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.AttenteJoueur;

import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Screen;

/**
 *
 * @author greg
 */
public class InterfaceWaitPannel extends Group {

    private double heightFenetre;
    private double widthFenetre;

    private ImageView background;
    private Rectangle navBar;
    private Label labelWait;
    private ProgressIndicator loading;

    public InterfaceWaitPannel(double widthFenetre, double heightFenetre) {
        this.widthFenetre = widthFenetre;
        this.heightFenetre = heightFenetre;

        initBackGround();
        initNavBar();
        initLabelWait();
        initLoading();
    }

    private void initBackGround() {
        background = new ImageView(new Image("file:" + System.getProperty("user.dir") + "/src/projet_java_gl_reseau/config/images/background_pannel.jpg"));
        background.setFitHeight(this.heightFenetre);
        background.setFitWidth(this.widthFenetre);
        this.getChildren().add(background);
    }

    private void initNavBar() {
        navBar = new Rectangle(this.widthFenetre * 0.5, this.heightFenetre);
        navBar.setX(Screen.getPrimary().getBounds().getWidth() / 2 - (this.widthFenetre * 0.50) / 2);
        navBar.setY(this.heightFenetre / 4 + 60);
        navBar.setFill(Color.color(0, 0, 0));
        navBar.setOpacity(0.8);
        navBar.setStrokeWidth(5);
        navBar.setArcHeight(40);
        navBar.setArcWidth(40);
        this.getChildren().add(navBar);
    }

    private void initLabelWait() {
        labelWait = new Label("Attente d'autre joueur : ");
        labelWait.setFont(Font.font(20));
        final String cssDefaultLabel = "-fx-text-alignment: center;\n"
                + "-fx-text-fill: White;\n";
        labelWait.setStyle(cssDefaultLabel);
        labelWait.setMinHeight(20);
        labelWait.setMinWidth(100);
        labelWait.setTranslateX(this.widthFenetre / 2 - labelWait.getMinWidth());
        labelWait.setTranslateY(this.heightFenetre / 4);
        this.getChildren().add(labelWait);
    }

    private void initLoading() {
        loading = new ProgressIndicator(-1.0);
        loading.setTranslateX(this.widthFenetre / 2 + labelWait.getMinWidth() + 40);
        loading.setTranslateY(this.heightFenetre / 4);
        loading.setVisible(true);
        this.getChildren().add(loading);
    }

    public void joueurRectangle(int nbPlayer, String namePlayer, Color color) {
        Rectangle player = new Rectangle(this.widthFenetre * 0.5 - 20, 80);
        player.setX(Screen.getPrimary().getBounds().getWidth() / 2 - (this.widthFenetre * 0.50) / 2 + 10);
        player.setY(this.heightFenetre / 4 + 70 + (81 * nbPlayer));
        player.setFill(color);
        player.setOpacity(0.8);
        player.setStrokeWidth(5);
        player.setArcHeight(10);
        player.setArcWidth(10);

        Label labelName = new Label(namePlayer);
        labelName.setFont(Font.font(20));
        final String cssDefaultLabel = "-fx-text-alignment: center;\n"
                + "-fx-text-fill: White;\n";
        labelName.setStyle(cssDefaultLabel);
        labelName.setMinHeight(20);
        labelName.setMinWidth(100);
        labelName.setTranslateX(player.getX() + player.getWidth() / 2 - labelName.getMinWidth());
        labelName.setTranslateY(player.getY() + player.getHeight() / 2 - labelName.getMinHeight() / 2);
        //code modification affichage 
        this.getChildren().add(player);
        this.getChildren().add(labelName);
    }
}
