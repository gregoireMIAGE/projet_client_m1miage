/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.AttenteJoueur;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import projet_java_gl_reseau.InterfaceGraphique.ControlerInterfaceGraphique;

/**
 *
 * @author greg
 */
public class ControlerWaitPannel {
    private static InterfaceWaitPannel interfaceWaitPannel;
    
    //permet d'initialiser le controleur et 'affichage
    public static void init(double width, double height){
        interfaceWaitPannel = new InterfaceWaitPannel(width, height);
        ControlerInterfaceGraphique.getRootWait().getChildren().add(interfaceWaitPannel);
    } 
    //permet d'ajouter les joeuur ne attente
    public static void addJoueur(int nbPlayer, String namePlayer, Color color){
        interfaceWaitPannel.joueurRectangle(nbPlayer, namePlayer, color);
    }
}
