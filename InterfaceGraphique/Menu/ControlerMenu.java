/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Menu;

import projet_java_gl_reseau.InterfaceGraphique.Connexion.InterfaceConnexion;
import projet_java_gl_reseau.InterfaceGraphique.Connexion.event.EventPressedButtonConnexion;
import projet_java_gl_reseau.InterfaceGraphique.ControlerInterfaceGraphique;
import projet_java_gl_reseau.InterfaceGraphique.Menu.Event.EventClickJouer;
import projet_java_gl_reseau.InterfaceGraphique.Menu.Event.EventClickQuit;
import projet_java_gl_reseau.player.Joueur;

/**
 *
 * @author greg
 */
public class ControlerMenu {
    protected static InterfaceMenu interfaceMenu;
    
    protected static EventClickQuit eventClickQuit;
    protected static EventClickJouer eventClickJouer;
    
        //initialisation de l'a première page
    public static void init(double widthFenetre, double heightFenetre) {
        interfaceMenu = new InterfaceMenu(widthFenetre, heightFenetre);
        
        eventClickJouer = new EventClickJouer();
        eventClickQuit = new EventClickQuit();
        
        interfaceMenu.dessinMenu();
        ControlerInterfaceGraphique.getRootMenu().getChildren().add(interfaceMenu);
    }
}
