/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Menu;

import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import static projet_java_gl_reseau.InterfaceGraphique.Menu.ControlerMenu.eventClickJouer;
import static projet_java_gl_reseau.InterfaceGraphique.Menu.ControlerMenu.eventClickQuit;

/**
 *
 * @author franckpichot
 */
public class InterfaceMenu extends Group {
    private double widthFenetre;
    private double heightFenetre;

    public InterfaceMenu(double widthFenetre, double heightFenetre) {
        this.widthFenetre = widthFenetre;
        this.heightFenetre = heightFenetre;
    }

    public void dessinMenu() {

        //image
        ImageView backGround = new ImageView(new Image("file:" + System.getProperty("user.dir") + "/src/projet_java_gl_reseau/config/images/background_pannel.jpg"));
        backGround.setFitHeight(heightFenetre);
        backGround.setFitWidth(widthFenetre);
        this.getChildren().add(backGround);

        //button jouer
        Button btn = new Button();
        btn.setLayoutX(190);
        btn.setLayoutY(320);
        btn.setStyle("-fx-background-color: #767676;\n"
                + "-fx-text-alignment: center;\n"
                + "-fx-text-fill: white;\n"
        );
        btn.setText("Jouer");
        btn.setOnMouseClicked(eventClickJouer);
        //button parametre
        Button btn1 = new Button();
        btn1.setLayoutX(357);
        btn1.setLayoutY(320);
        btn1.setStyle("-fx-background-color: #767676;\n"
                + "-fx-text-alignment: center;\n"
                + "-fx-text-fill: white;\n"
        );
        btn1.setText("Paramètres");
        //button score
        Button btn2 = new Button();
        btn2.setLayoutX(570);
        btn2.setLayoutY(320);
        btn2.setStyle("-fx-background-color: #767676;\n"
                + "-fx-text-alignment: center;\n"
                + "-fx-text-fill: white;\n"
        );
        btn2.setText("Score");

        //button quitter
        Button btn3 = new Button();
        btn3.setLayoutX(757);
        btn3.setLayoutY(320);
        btn3.setStyle("-fx-background-color: #767676;\n"
                + "-fx-text-alignment: center;\n"
                + "-fx-text-fill: white;\n"
        );
        btn3.setText("Quitter");
        btn3.setOnMouseClicked(eventClickQuit);

        //mise en page du panel
        this.getChildren().add(btn);
        this.getChildren().add(btn1);
        this.getChildren().add(btn2);
        this.getChildren().add(btn3);
    }
}
