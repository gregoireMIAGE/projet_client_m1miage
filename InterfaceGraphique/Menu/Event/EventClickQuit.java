/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Menu.Event;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import projet_java_gl_reseau.InterfaceGraphique.Connexion.ControlerConnexion;
import projet_java_gl_reseau.InterfaceGraphique.ControlerInterfaceGraphique;
import projet_java_gl_reseau.reseaux.Reseau;

/**
 *
 * @author greg
 */
public class EventClickQuit extends ControlerConnexion implements EventHandler<MouseEvent>{

    @Override
    public void handle(MouseEvent event) {
         new Thread(new Runnable() {
            @Override
            public void run() {
                //envoyer deconnexion au serveur
                Reseau.sendMessage("Deconnexion");
            }
        }).start();
        ControlerInterfaceGraphique.afficheConnexion();
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
