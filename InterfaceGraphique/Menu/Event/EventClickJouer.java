/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.InterfaceGraphique.Menu.Event;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import projet_java_gl_reseau.InterfaceGraphique.Connexion.ControlerConnexion;
import projet_java_gl_reseau.InterfaceGraphique.ControlerInterfaceGraphique;
import projet_java_gl_reseau.reseaux.Reseau;

/**
 *
 * @author greg
 */
public class EventClickJouer extends ControlerConnexion implements EventHandler<MouseEvent> {

    @Override
    public void handle(MouseEvent event) {
        //deconnexion
        //on envoie la demande de deconexion 
        new Thread(new Runnable() {
            @Override
            public void run() {
                //envoyer deconnexion au serveur
                Reseau.sendMessage("JouerPartie");
            }
        }).start();
        ControlerInterfaceGraphique.afficheWait();
    }

}
