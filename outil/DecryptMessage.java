/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.outil;

import java.util.ArrayList;
import javafx.scene.paint.Color;
import projet_java_gl_reseau.player.Compte;
import projet_java_gl_reseau.player.Joueur;

/**
 *
 * @author greg
 */
public class DecryptMessage {
     public static ArrayList<Joueur> decryptInforJoueurServeur(String message) {
        String[] tabJoueur = message.split(";");

        ArrayList<Joueur> lesJoueur = new ArrayList<Joueur>();
        for (int i = 0; i < tabJoueur.length; i++) {
            Color color = Color.valueOf(tabJoueur[i].split(",")[1]);
            int solde = Integer.parseInt(tabJoueur[i].split(",")[2]);
            int levelLocomotive = Integer.parseInt(tabJoueur[i].split(",")[3]);
            lesJoueur.add(new Joueur(tabJoueur[i].split(",")[0], color, new Compte(solde), levelLocomotive, 0));
        }
        return lesJoueur;
    }
}
