/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.outil;

/**
 *
 * @author greg
 */
public class ConvertArray {
        //convertion map string to int[]
    public static int[][] convertToIntArrayMap(String values) {

        String[] tab = values.split(";");
        String[] tab2 = tab[0].split(",");
        int[][] tabInt = new int[tab.length][tab2.length];
        for (int i = 0; i < tab.length; i++) {
            tab2 = tab[i].split(",");
            for (int a = 0; a < tab2.length; a++) {
                tabInt[i][a] = Integer.parseInt(tab2[a]);
            }
        }
        return tabInt;
    }

    //convertion map string to int[]
    public static String[][] convertToStringArrayMap(String values) {

        String[] tab = values.split(";");
        String[] tab2 = tab[0].split(",");
        String[][] tabString = new String[tab.length][tab2.length];
        for (int i = 0; i < tab.length; i++) {
            tab2 = tab[i].split(",");
            for (int a = 0; a < tab2.length; a++) {
                tabString[i][a] = tab2[a];
            }
        }
        return tabString;
    }
    //permet de convertir un ArrayString en ArrayInt

    public static int[] convertToIntArray(String[] array) {
        int[] retour = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            retour[i] = Integer.parseInt(array[i].trim());
        }
        return retour;
    }
}
