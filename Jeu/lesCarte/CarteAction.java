/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.Jeu.lesCarte;

import projet_java_gl_reseau.Jeu.Carte;

/**
 *
 * @author greg
 */
public class CarteAction extends Carte {

    //ATTRIBUTS
    private int niv;

    //CONSTRUCTEUR
    public CarteAction(int niv, String name, String location) {
        super(name, location);
        this.name = name;
        this.niv = niv;
    }

    //METHODES
    public int getNiveau() {
        return this.niv;
    }

    public String getName() {
        return this.name;
    }

}
