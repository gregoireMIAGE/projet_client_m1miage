/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.Jeu.lesCarte;

import java.util.ArrayList;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import projet_java_gl_reseau.Jeu.Carte;
import projet_java_gl_reseau.Jeu.Marchandise;

/**
 *
 * @author greg
 */
public class CarteVille extends Carte {

    private int niveau;
    private int positionX;
    private int positionY;
    private String key; //represente la clée de la ville
    private Color color;

    public CarteVille(String name, String location, int niveau, int positionX, int positionY) {
        super(name, location);
        this.niveau = niveau;
        this.positionX = positionX;
        this.positionY = positionY;
    }
    
    public CarteVille(CarteCity carte){
        super(carte.getName(), carte.getLocation());
        this.niveau = carte.getNiveau();
        this.positionX = carte.getPositionXMap();
        this.positionY = carte.getPositionYMap();
        this.x = carte.getImageViw().getTranslateX();
        this.y = carte.getImageViw().getTranslateY();
        this.imageView = carte.getImageViw();
        this.key = carte.getKey();
        this.color = carte.getColor();
    }

    public CarteVille(String name, String location, int niveau, int positionX, int positionY, String key) {
        super(name, location);
        this.niveau = niveau;
        this.positionX = positionX;
        this.positionY = positionY;
        this.key = key;
    }

    public CarteVille(String name, String location, int niveau, int positionX, int positionY, double x, double y) {
        super(name, location);
        this.niveau = niveau;
        this.positionX = positionX;
        this.positionY = positionY;
        this.x = x;
        this.y = y;
    }

    //ACCESSEUR
    public Color getColor(){
        return this.color;
    }
    public void setColor(Color color){
        this.color = color;
    }
            
    public String getKey() {
        return this.key;
    }

    public int getNiveau() {
        return this.niveau;
    }

    public int getPositionX() {
        return this.positionX;
    }

    public int getPositionY() {
        return this.positionY;
    }

    public void setPositionX(int x) {
        this.positionX = x;
    }

    public void setPositionY(int y) {
        this.positionY = y;
    }

    public int getNbMarchandiseVille() {
        return this.lesMarchandise.size();
    }

    public boolean isEqualPosition(int x, int y) {
        //permet de retouerner si la postion correspond a la carte
        return (this.positionX == x && this.positionY == y);
    }

    public int addMarchandiseVille(Marchandise marchandise) {
        if (this.lesMarchandise.size() < this.niveau) {
            this.addMarchandise(marchandise);
            return 1;
        } else {
            return 0;
        }
    }

    public void setImageViewCarteVille(ImageView carteRail) {
        this.setImageView(carteRail);
    }

    public boolean isEqualImageViewCarteVille(ImageView imageView) {
        return this.isEqualImageView(imageView);
    }

    //METHODE
    public ArrayList<Rectangle> dessinCubes() {
        ArrayList<Rectangle> lesRectangles = new ArrayList<Rectangle>();
        for (int i = 0; i < this.lesMarchandise.size(); i++) {
            double xAffiche = this.imageView.getTranslateX() + this.lesMarchandise.get(i).getWidth() + ((i % 2) * this.lesMarchandise.get(i).getWidth() + (((i % 2) * 4)));//pemret de placer les cube sur la ville
            double yAffiche = this.imageView.getTranslateY() + this.lesMarchandise.get(i).getHeight() + ((i / 2) * this.lesMarchandise.get(i).getHeight() + (((i / 2) * 4)));//permet de placer le scube sur la ville 
            Rectangle cube = this.lesMarchandise.get(i).dessinCube(xAffiche, yAffiche, this);
            lesRectangles.add(cube);
        }
        return lesRectangles;
    }

    //pemret de deplacer la marchandise au bonne endroit
    public Rectangle MiseAJour() {
        int i = lesMarchandise.size() - 2;//-2 car mise a jour de la carte aprés ajout du cube
        double xAffiche = this.x + ((i % 2) * this.lesMarchandise.get(i).getWidth() + (((i % 2) * 4)));//pemret de placer les cube sur la ville
        double yAffiche = this.y + ((i / 2) * this.lesMarchandise.get(i).getHeight() + (((i / 2) * 4)));//permet de placer le scube sur la ville 
        Rectangle cube = this.lesMarchandise.get(i + 1).dessinCube(xAffiche, yAffiche, this);
        return cube;
    }
    
    public boolean isComplete(){
        return this.niveau == this.lesMarchandise.size();
    }
}
