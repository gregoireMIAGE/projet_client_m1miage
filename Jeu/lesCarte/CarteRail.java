/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.Jeu.lesCarte;

import java.util.ArrayList;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import projet_java_gl_reseau.Jeu.Carte;
import projet_java_gl_reseau.Jeu.Chemin;

/**
 *
 * @author greg
 */
    
public class CarteRail extends Carte {

    /*ATTRIBUT*/
    //permet de connaitre les point de ratachement du rail
    private int pointAttach[];
    private int positionXmap;//position tableau
    private int positionYmap;//position tableau
    private int place;//place de la carte dans la bar
    private final int id;
    private boolean validate;//permet de definir si la carte a teait validé
    private String locationChemin;//permet de recuperer l'imagepour tracer un chemin 
    private ArrayList<Chemin> lesChemins; //represente le ou les chemin au quel appartien le rail (le père)

    /*CONSTRUCTEUR*/
    public CarteRail(String name, String location, int place, int pointAttach[], int id) {
        super(name, location);
        this.lesChemins = new ArrayList<Chemin>();
        this.pointAttach = pointAttach;
        this.validate = false;
        this.place = place;
        this.id = id;
    }

    public CarteRail(String name, String location, int place, int pointAttach[], String locationChemin) {
        super(name, location);
        this.lesChemins = new ArrayList<Chemin>();
        this.pointAttach = pointAttach;
        this.validate = false;
        this.place = place;
        this.locationChemin = locationChemin;
        this.id = 0;
    }

    /*ACCESSEUR*/
    public int getPlace(){
        return this.place;
    }
    public int getId(){
        return this.id;
    }
    public String getNameRail() {
        return name;
    }

    public boolean getValidate() {
        return this.validate;
    }

    public void validate() {
        this.validate = true;
    }

    public void unvalidate() {
        this.validate = false;
    }

    public void setPositionXmap(int x) {
        this.positionXmap = x;
    }

    public void setPositionYmap(int Y) {
        this.positionYmap = Y;
    }

    public int getPositionXmap() {
        return this.positionXmap;
    }

    public int getPositionYmap() {
        return this.positionYmap;
    }

    public int[] getPointAttach() {
        return this.pointAttach;
    }

    public int getPointAttach(int i) {
        return this.pointAttach[i];
    }

    public void addChemin(Chemin unChemin) {
        this.lesChemins.add(unChemin);
    }

    public void addCheminAll(ArrayList<Chemin> unChemin) {
        this.lesChemins.addAll(unChemin);
    }

    public String getLocationChemin() {
        return this.locationChemin;
    }

    public Chemin getChemin(int index) {
        return this.lesChemins.get(index);
    }

    public ArrayList<Chemin> getCheminAll() {
        return this.lesChemins;
    }

    public int getLesCheminsSize() {
        return this.lesChemins.size();
    }

    public boolean getPose() {
        return this.pose;
    }

    public void setPose() {
        this.pose = true;
    }
    
    public void setImageViewCarteRail(ImageView carteRail){
        this.setImageView(carteRail);
    }
    public boolean isEqualImageViewCarteRail(ImageView carteRail){
        return this.isEqualImageView(carteRail);
    }
    /*METHODE*/

    public ImageView dessinerCartes(double x, double y, int taille) {
        this.x = x;
        this.y = y * place;
        this.taille = taille;
        ImageView img = new ImageView();
        img.setImage(new Image(this.location));
        img.setTranslateX(this.x);
        img.setTranslateY(this.y);
        img.setFitHeight(this.taille);
        img.setFitWidth(this.taille);

        return img;
    }

    //permet de dessiner le chemin de la carte
    public Image getImageDessineChemin() {
        return new Image(this.locationChemin);
    }

    //permet de decaler le tableau 
    public void moveToArray(String cmd) {
        int[] tabreturn = new int[this.pointAttach.length];
        if (cmd.equalsIgnoreCase("gauche")) {
            for (int i = this.pointAttach.length - 1; i > 0; i--) {
                tabreturn[i - 1] = this.pointAttach[i];
            }
            tabreturn[this.pointAttach.length - 1] = this.pointAttach[0];
        }
        if (cmd.equalsIgnoreCase("droite")) {
            for (int i = 0; i < this.pointAttach.length - 1; i++) {
                tabreturn[i + 1] = this.pointAttach[i];
            }
            tabreturn[0] = this.pointAttach[this.pointAttach.length - 1];
        }
        this.pointAttach = tabreturn;
    }

    public boolean isEqualPosition(int x, int y) {
        //permet de retouerner si la postion correspond a la carte
        return (this.positionXmap == x && this.positionYmap == y);
    }

    //permet la comparaison des point d'attach entre deux carte. carte.this prioritaire donc peux etre egale a 1 alors que l'autre carte = 0
    public boolean comparePointAttach(int[] tab) {
        boolean valide = false;
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] == 1 && this.pointAttach[i] == 1) {
                valide = true;
            }
        }
        return valide;
    }
    
    //permet de verifier si le rail correspond bien
    public boolean isEqualIdRail(int id){
        return id == this.id;
    }
    
    //permet de calculer le nombre de sortir
    public int getNbSortie(){
        int result = 0;
        for(int i = 0; i<this.pointAttach.length; i++){
            if(this.pointAttach[i]==1){
                result = result + 1;
            }
        }
        return result;
    }
}
