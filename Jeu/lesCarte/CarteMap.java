/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.Jeu.lesCarte;

import javafx.scene.image.ImageView;
import projet_java_gl_reseau.Jeu.Carte;

/**
 *
 * @author greg
 */
public class CarteMap extends Carte {

    public int positionX;
    public int positionY;
    public int construct;

    public CarteMap(String name, ImageView imageView, int positionX, int positionY, int construct) {
        super(name, imageView);
        this.positionX = positionX;
        this.positionY = positionY;
        this.construct = construct;
    }
    
        public CarteMap(String name, ImageView imageView, int positionX, int positionY, double posX, double posY, int construct) {
        super(name, imageView, posX, posY);
        this.positionX = positionX;
        this.positionY = positionY;
        this.construct = construct;
    }
    
    public CarteMap(String name, String location) {
        super(name, location);
        this.positionX = 0;
        this.positionY = 0;
        this.construct = 0;
    }

    public String getName() {
        return this.name;
    }

    public int getPositionX() {
        return this.positionX;
    }

    public int getPositionY() {
        return this.positionY;
    }

    public int getConstruct() {
        return this.construct;
    }

    public void setConstruct() {
        this.construct = 1;
    }

    public boolean isEqualImageViewMap(ImageView imageView) {
        return this.isEqualImageView(imageView);
    }
    public boolean isEqualNameMap(String name) {
        return this.isEqualName(name);
    }

    public boolean isEqualPosition(int x, int y) {
        //permet de retouerner si la postion correspond a la carte
        return (this.positionX == x && this.positionY == y);
    }
    
    public void setImageViewCarteMap(ImageView carteRail){
        this.setImageView(carteRail);
    }
    public boolean isEqualImageViewCarteMap(ImageView carteRail){
        return this.isEqualImageView(carteRail);
    }
}
