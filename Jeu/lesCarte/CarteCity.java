/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.Jeu.lesCarte;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import projet_java_gl_reseau.Jeu.Carte;

/**
 *
 * @author greg
 */
public class CarteCity extends Carte {

    public int niveau;
    public int place; //la place sur la map 
    public int positionXmap;//position tableau
    public int positionYmap;//position tableau
    public String key;
    public final int id;
    public Color color;

    public CarteCity(String name, String location, int niveau, int place) {
        super(name, location);
        this.niveau = niveau;
        this.place = place;
        this.id = 0;
    }

    public CarteCity(String name, String location, int niveau, int place, String key, int id) {
        super(name, location);
        this.niveau = niveau;
        this.place = place;
        this.key = key;
        this.id = id;
    }

    //ACCESSEUR
    
    public Color getColor(){
        return this.color;
    }
    public void setColor(Color color){
        this.color = color;
    }
    public String getKey(){
        return this.key;
    }
    public int getId(){
        return this.id;
    }
    public int getPlace() {
        return this.place;
    }

    public int getPositionXMap() {
        return this.positionXmap;
    }

    public int getPositionYMap() {
        return this.positionYmap;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public void setPositionXmap(int x) {
        this.positionXmap = x;
    }

    public void setPositionYmap(int y) {
        this.positionYmap = y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public boolean getPose() {
        return this.pose;
    }

    public void setPose() {
        this.pose = true;
    }

    public int getNiveau() {
        return this.niveau;
    }

    public void setImageViewCarteCity(ImageView carteRail) {
        this.setImageView(carteRail);
    }

    public boolean isEqualImageViewCarteCity(ImageView carteRail) {
        return this.isEqualImageView(carteRail);
    }

    //METHODES

    public ImageView dessinerCartes(double x, double y, int taille) {
        this.x = x;
        this.y = y * place;
        this.taille = taille;
        ImageView img = new ImageView();
        img.setImage(new Image(this.location));
        img.setTranslateX(this.x);
        img.setTranslateY(this.y);
        img.setFitHeight(this.taille);
        img.setFitWidth(this.taille);

        return img;
    }

    public boolean isEqualPosition(double x, double y) {
        //permet de retouerner si la postion correspond a la carte
        return (this.positionXmap == x && this.positionYmap == y);
    }
    
        //permet de verifier si le rail correspond bien
    public boolean isEqualIdCity(int id){
        return id == this.id;
    }
}
