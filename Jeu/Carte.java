/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.Jeu;

import java.awt.Point;
import java.util.ArrayList;
import java.util.LinkedList;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Screen;

/**
 *
 * @author greg
 */
public class Carte {

    protected String name;
    protected String location;//adresse image 1
    protected int taille;
    protected double x; //position affichage 
    protected double y;//position affichage 
    protected ImageView imageView;//image de la carte
    protected Group group;
    protected boolean pose; //defini si le rail a deja etai posé ou non 
    protected ArrayList<Marchandise> lesMarchandise;

    public Carte(String name, String location) {
        this.name = name;
        this.location = location;
        this.lesMarchandise = new ArrayList<Marchandise>();
    }

    protected Carte(String name, ImageView imageView) {
        this.name = name;
        this.imageView = imageView;
    }
    
        protected Carte(String name, ImageView imageView, double x, double y) {
        this.name = name;
        this.imageView = imageView;
        this.x = x;
        this.y = y;
    }

    public String getLocation() {
        return this.location;
    }

    public String getName() {
        return this.name;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public ImageView getImageViw() {
        return this.imageView;
    }
    
    public void setImageView(ImageView View){
        this.imageView = View;
    }

    public boolean isEqualImageView(ImageView imageView) {
        return imageView == this.imageView;
    }
    public boolean isEqualName(String name) {
        return name.equalsIgnoreCase(this.name);
    }
    protected void addMarchandise(Marchandise marchandise){
        this.lesMarchandise.add(marchandise);
    }
    protected int getNbMarchandise(){
        return this.lesMarchandise.size();
    }
    
    //methode
    //permet de dupliqué l'image view
    public ImageView duplicate(){
        ImageView newImage = new ImageView(this.imageView.getImage());
        newImage.setTranslateX(this.imageView.getTranslateX());
        newImage.setTranslateY(this.imageView.getTranslateY());
        newImage.setX(this.imageView.getX());
        newImage.setY(this.imageView.getY());
        newImage.setRotate(this.imageView.getRotate());
        newImage.setFitHeight(this.imageView.getFitHeight());
        newImage.setFitWidth(this.imageView.getFitWidth());
        newImage.setOpacity(this.imageView.getOpacity());
        newImage.setScaleY(this.imageView.getScaleY());
        newImage.setScaleY(this.imageView.getScaleY());
        /*newImage.setOnMouseClicked(this.imageView.getOnMouseClicked());
        newImage.setOnMouseDragged(this.imageView.getOnMouseDragged());
        newImage.setOnMouseEntered(this.imageView.getOnMouseEntered());
        newImage.setOnMousePressed(this.imageView.getOnMousePressed());*/
        
        return newImage;
    }

}




