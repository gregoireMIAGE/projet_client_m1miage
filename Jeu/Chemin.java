/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.Jeu;

import java.util.ArrayList;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import projet_java_gl_reseau.Jeu.lesCarte.CarteRail;
import projet_java_gl_reseau.Jeu.lesCarte.CarteVille;

/**
 *
 * @author greg
 */
public class Chemin {
    /*#ATTRIBUTS*/

    private CarteVille carteVilleDep;
    private CarteVille carteVilleArr;

    private ArrayList<CarteRail> lesRails;

    /*CONSTRUCTEUR*/
    public Chemin(CarteVille villedep, CarteRail unRail) {
        this.lesRails = new ArrayList<CarteRail>();
        this.lesRails.add(unRail);
        this.carteVilleDep = villedep;
    }

    public Chemin(CarteVille villedep, CarteVille villeArrive, CarteRail unRail) {
        this.lesRails = new ArrayList<CarteRail>();
        this.lesRails.add(unRail);
        this.carteVilleDep = villedep;
        this.carteVilleArr = villeArrive;
    }

    /*ACCESSEUR*/
    public boolean getFinished() {
        return (this.carteVilleArr != null && this.carteVilleDep != null);
    }
    /*METHODE*/

    public void setVilleArrivee(CarteVille villeArrive) {
        this.carteVilleArr = villeArrive;
    }

    public void addCarteRail(CarteRail unRail) {
        this.lesRails.add(unRail);
    }

    public CarteVille getVilleDep() {
        return this.carteVilleDep;
    }

    public CarteVille getVilleArr() {
        return this.carteVilleArr;
    }

    public int getNbRail() {
        return this.lesRails.size();
    }

    public void dessinerCarteChemin(Scene scene, Group root) {
        for (int i = 0; i < lesRails.size(); i++) {
            ImageView image = new ImageView(new Image(this.lesRails.get(i).getLocationChemin()));
            image.setFitHeight(100);
            image.setFitWidth(100);
            image.setTranslateX(this.lesRails.get(i).getPositionXmap());
            image.setTranslateY(this.lesRails.get(i).getPositionYmap());
            root.getChildren().add(image);
        }
    }

    //rever permet de retourner le tableau de carte
    public void reverse() {
        CarteVille carte = this.carteVilleArr;
        this.carteVilleArr = this.carteVilleDep;
        this.carteVilleDep = carte;

        int i = 0;
        int j = this.lesRails.size();
        while (i<j) {
            this.lesRails.subList(i, (this.lesRails.size()-1)-i);
            i++;
            j--;
        }
    }
    
    //permet de retrouenr la liste des id des cartes du chemin
    public ArrayList<Integer> getIdCarteRail(){
        ArrayList<Integer> lesIdCarteRail = new ArrayList<Integer>();
        for(int i = 0; i<this.lesRails.size(); i++){
            lesIdCarteRail.add(lesRails.get(i).getId());
        }
        return lesIdCarteRail;
    }
    
    public CarteRail getCarteRail(int index){
        return this.lesRails.get(index);
    }

}
