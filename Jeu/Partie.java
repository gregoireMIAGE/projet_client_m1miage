/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.Jeu;

import java.util.ArrayList;
import java.util.LinkedList;
import projet_java_gl_reseau.player.Joueur;

/**
 *
 * @author greg
 */
public class Partie {

    //ATRIBUT
    private Joueur joueur;
    private LinkedList<Joueur> lesJoueur;
    private LinkedList<Joueur> lesAdversaire;
    private Map map;
    private static boolean pause;
    private Thread threadAfficheCmdServeur;//permet d'afficher les information provenant du serveur

    //CONSTRUCTEUR
    public Partie(Joueur leJoueur, Map map) {
        this.joueur = leJoueur;
        this.lesJoueur = new LinkedList<Joueur>();
        this.lesAdversaire = new LinkedList<Joueur>();
        this.map = map;
    }

    //ACCESSEUR
    public void addJoueur(Joueur leJoueur) {
        this.lesJoueur.add(leJoueur);
    }
    //permet de mettre a jour la liste complette des joueurs
    public void addJoueurAll(ArrayList<Joueur> lesJoueur) {
        this.lesJoueur.clear();
        this.lesJoueur.addAll(lesJoueur);
        for(int i = 0; i<lesJoueur.size(); i++){
            if(!this.lesJoueur.get(i).isEqualJoueur(joueur.getName())){
                this.lesAdversaire.add(this.lesJoueur.get(i));
            }
        }
    }
    public Map getMap(){
        return this.map;
    }
    public LinkedList<Joueur> getLesJoueurs(){
        return this.lesJoueur;
    }
    public LinkedList<Joueur> getLesAdversaire(){
        return this.lesAdversaire;
    }

    //METHDOES
}
