/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.Jeu;

import java.util.ArrayList;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import projet_java_gl_reseau.Jeu.lesCarte.CarteVille;

/**
 *
 * @author greg
 */
public class Marchandise {

    //ATRIVUTS
    private int id;
    private Color couleur;
    private double xMapAffiche;
    private double yMapAffiche;
    private int x;
    private int y;
    private double width;
    private double height;
    private Rectangle cube;

    private ArrayList<Carte> leChemin;//le chemin que le cube doit parcourir
    private int deplacementChemin; //permet de definir l'endroit ou s'arrête la marchandise

    //la carte sur la quel se situe la marchandise
    private Carte laCarteEncours;
    private CarteVille villeDep;

    //CONSTRUCTEUR
    public Marchandise(Color couleur) {
        this.couleur = couleur;
    }

    public Marchandise(int id, Color couleur, double xMap, double yMap, int x, int y, double width, double height) {
        this.couleur = couleur;
        this.xMapAffiche = xMap;
        this.yMapAffiche = yMap;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.id = id;
    }

    //ACCESSEUR
    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public Color getColor() {
        return this.couleur;
    }

    public Rectangle getCube() {
        return this.cube;
    }

    public void setCarteVilleDep(CarteVille carte) {
        this.villeDep = carte;
    }

    public CarteVille getCarteVilleDep() {
        return this.villeDep;
    }

    public void setCarte(Carte carte) {
        this.laCarteEncours = carte;
    }

    public Carte getCarte() {
        return this.laCarteEncours;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getWidth() {
        return this.width;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getHeight() {
        return this.height;
    }

    public void setXmap(double xMap) {
        this.xMapAffiche = xMap;
    }

    public void setYmap(double yXmap) {
        this.yMapAffiche = yXmap;
    }

    public void setLeChemins(ArrayList<Carte> leChemin) {
        this.leChemin = new ArrayList<Carte>(leChemin);
        deplacementChemin = 0;
    }

    public int getDeplacementChemin() {
        return this.deplacementChemin;
    }

    //METHODES

    public boolean isEqualPosition(double x, double y) {
        return (this.xMapAffiche == x && this.yMapAffiche == y);
    }

    public boolean isEqualRectangle(Rectangle cube) {
        return (this.cube == cube);
    }

    //pemret de dessiner un rectangle
    public Rectangle dessinCube(double xAffiche, double yAffiche, CarteVille carte) {
        this.villeDep = carte;
        this.cube = new Rectangle(this.getWidth(), this.getHeight());
        this.cube.setTranslateX(xAffiche);
        this.cube.setTranslateY(yAffiche);
        this.cube.setFill(this.couleur);
        this.cube.setStroke(Color.BLACK);
        return cube;
    }

    public Rectangle dessinCubeVille(CarteVille carte) {
        this.laCarteEncours = carte;
        this.villeDep = carte;
        this.xMapAffiche = carte.getX();
        this.yMapAffiche = carte.getY();
        this.x = carte.getPositionX();
        this.y = carte.getPositionY();
        this.cube = new Rectangle(xMapAffiche, yMapAffiche, this.getWidth(), this.getHeight());
        this.cube.setFill(this.couleur);
        this.cube.setStroke(Color.BLACK);
        return cube;
    }

    public void setTranslateCube(double x, double y, Carte carte) {
        this.yMapAffiche = y;
        this.xMapAffiche = x;
        this.cube.setTranslateX(x);
        this.cube.setTranslateY(y);
        this.laCarteEncours = carte;
    }

    public int getNbRailsChemin() {
        return this.leChemin.size();
    }

    public Carte getRailChemin(int index) {
        return this.leChemin.get(index);
    }

    //permet le deplacement du pointeur de chemin
    public void seekChemin(int nb) {
        this.deplacementChemin =+ nb; 
    }
}
