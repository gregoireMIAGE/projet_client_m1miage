/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.XML;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author greg
 */
public class LectureXML {

    private static File path;

    //permet de lire la map xml
    public static String lireXmlMap() {
        return lireXml("/src/projet_java_gl_reseau/config/projet_jeu/map.xml", "map");
    }

    public static String lireXmlStringlMap(String xml) {
        return lireXmlString(xml, "map");
    }

    //permet de lire la mapRail xml
    public static String lireXmlMapRail() {
        return lireXml("/src/projet_java_gl_reseau/config/map.xml", "mapRail");
    }

    public static String lireXmlStringMapRail(String xml) {
        return lireXmlString(xml, "mapRail");
    }
    //permet de lire les config carte xml

    /*public static Element lireXmlCartesRails() {
        return LireElementXml("/src/projet_java_gl_reseau/config/carteRail.xml", "cartesRail");
    }*/

    public static Document lireXmlCartesRails() {
        return LireElementXml("/src/projet_java_gl_reseau/config/carteRail.xml", "cartesRail");
    }

    /*public static Element lireXmlCartesCitys() {
        return LireElementXml("/src/projet_java_gl_reseau/config/cartesCitys.xml", "cartesCitys");
    }*/
    public static Document lireXmlCartesCitys() {
        return LireElementXml("/src/projet_java_gl_reseau/config/cartesCitys.xml", "cartesCitys");
    }

    /*public static Element lireXmlCartesMap() {
        return LireElementXml("/src/projet_java_gl_reseau/config/cartesMap.xml", "cartesMap");
    }*/
    public static Document lireXmlCartesMap() {
        return LireElementXml("/src/projet_java_gl_reseau/config/cartesMap.xml", "cartesMap");
    }

    private static String lireXml(String location, String id) {
        String name = "";
        path = new File(System.getProperty("user.dir") + location);
        try {
            DocumentBuilderFactory factory;
            factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            builder = factory.newDocumentBuilder();
            Document dom;
            dom = builder.parse(new FileInputStream(path));
            Element root = dom.getDocumentElement();
            NodeList items = root.getElementsByTagName(id);
            Node item = items.item(0).getFirstChild();
            if (item != null) {
                name = item.getNodeValue();
            } else {
                name = "";
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (org.xml.sax.SAXException e) {
            //Logger.getLogger(LectureXML.class.getName()).log(Level.SEVERE, null, ex);
            e.printStackTrace();
        }
        return name;
    }

    private static String lireXmlString(String xml, String id) {
        String name = "";
        try {
            DocumentBuilderFactory factory;
            factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            builder = factory.newDocumentBuilder();
            Document dom;
            dom = builder.parse(xml);
            Element root = dom.getDocumentElement();
            NodeList items = root.getElementsByTagName(id);
            Node item = items.item(0).getFirstChild();
            if (item != null) {
                name = item.getNodeValue();
            } else {
                name = "";
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (org.xml.sax.SAXException e) {
            //Logger.getLogger(LectureXML.class.getName()).log(Level.SEVERE, null, ex);
            e.printStackTrace();
        }
        return name;
    }

    /*private static Element LireElementXml(String location, String id) {

        String name = "";
        Element root = null;
        path = new File(System.getProperty("user.dir") + location);
        try {
            DocumentBuilderFactory factory;
            factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            builder = factory.newDocumentBuilder();
            Document dom;
            dom = builder.parse(new FileInputStream(path));
            root = dom.getDocumentElement();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (org.xml.sax.SAXException e) {
            //Logger.getLogger(LectureXML.class.getName()).log(Level.SEVERE, null, ex);
            e.printStackTrace();
        }
        return root;
    }*/

    private static Document LireElementXml(String location, String id) {

        String name = "";
        Element root = null;
        path = new File(System.getProperty("user.dir") + location);
        Document dom = null;
        try {
            DocumentBuilderFactory factory;
            factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            builder = factory.newDocumentBuilder();
            dom = builder.parse(new FileInputStream(path));
            dom.getDocumentElement().normalize();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (org.xml.sax.SAXException e) {
            //Logger.getLogger(LectureXML.class.getName()).log(Level.SEVERE, null, ex);
            e.printStackTrace();
        }
        return dom;
    }
}
