/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.Commande;

import java.util.ArrayList;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import projet_java_gl_reseau.InterfaceGraphique.AttenteJoueur.ControlerWaitPannel;
import projet_java_gl_reseau.Jeu.Map;
import projet_java_gl_reseau.Jeu.Partie;
import projet_java_gl_reseau.Projet_java_gl_reseau;
import projet_java_gl_reseau.controler.Controler;
import projet_java_gl_reseau.outil.DecryptMessage;
import projet_java_gl_reseau.player.Compte;
import projet_java_gl_reseau.player.Joueur;

/**
 *
 * @author greg
 */
public class CommandeAddJoueur implements InterfaceCommande {

    private Partie partie;
    private Scene scene;
    private Group root;

    public CommandeAddJoueur(Partie partie, Scene scene) {
        this.partie = partie;
        this.scene = scene;
    }

    @Override
    public void executeMessage(String commande) {
        //ajout du joueur a la parti
        ArrayList<Joueur> lesJoueur = DecryptMessage.decryptInforJoueurServeur(commande.split(":")[1]);

        //redessiner les joueur en attente
        if (Controler.getJoueurs().containsAll(lesJoueur) == false) {
            for (int i = 0; i < lesJoueur.size(); i++) {
                joueurRectangle(i, lesJoueur.get(i).getName(), lesJoueur.get(i).getCouleur());
            }
            Controler.addJoueurPartie(lesJoueur);
        }

        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    //permet d'afficher le joueur

    public void joueurRectangle(int nbPlayer, String namePlayer, Color color) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                //permet d'afficher le joueur en attente
                ControlerWaitPannel.addJoueur(nbPlayer, namePlayer, color);
            }
        });
    }

}
