/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.Commande;

import java.util.LinkedList;
import javafx.application.Platform;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.infoJeu.ControlerPlateauInfoPartie;
import projet_java_gl_reseau.controler.Controler;
import projet_java_gl_reseau.player.Joueur;

/**
 *
 * @author greg
 */
public class CommandeOrdreTour implements InterfaceCommande {

    @Override
    public void executeMessage(String commande) {
        LinkedList<Joueur> ordre = new LinkedList<Joueur>();
        String cmd = commande.split(":")[1];
        String[] joueurs = cmd.split(",");
        for(int i = 0; i<joueurs.length; i++){
            ordre.add(Controler.getIdJoueur(joueurs[i]));
        }
        
         Platform.runLater(new Runnable() {
            @Override
            public void run() {
                //on affiche l'ordre des tour sur la carte
                ControlerPlateauInfoPartie.setOrdreTour(ordre);
            }
         });
    }

}
