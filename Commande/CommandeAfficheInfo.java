/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.Commande;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import projet_java_gl_reseau.InterfaceGraphique.Information.ControlerInformation;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.ControlerPlateau;
import projet_java_gl_reseau.Jeu.Map;

/**
 *
 * @author greg
 */
public class CommandeAfficheInfo implements InterfaceCommande {

    public CommandeAfficheInfo() {
    }

    @Override
    public void executeMessage(String commande) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                ControlerInformation.setTextInfo(commande.split(":")[1]);
                ControlerInformation.showInfo();
            }
        });
    }

}
