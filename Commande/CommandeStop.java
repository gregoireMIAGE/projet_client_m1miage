/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.Commande;

import projet_java_gl_reseau.InterfaceGraphique.ControlerInterfaceGraphique;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.ControlerPlateau;

/**
 *
 * @author greg
 */
public class CommandeStop implements InterfaceCommande{

    @Override
    public void executeMessage(String commande) {
        //arrêt serveur, on affiche le menu
        //on affiche le menu
        ControlerPlateau.stop();
        ControlerInterfaceGraphique.afficheConnexion();
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
