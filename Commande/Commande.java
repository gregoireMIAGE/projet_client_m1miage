/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.Commande;

import java.util.ArrayList;
import java.util.HashMap;
import javafx.scene.Group;
import javafx.scene.Scene;
import projet_java_gl_reseau.Jeu.Map;
import projet_java_gl_reseau.Jeu.Partie;

/**
 *
 * @author greg
 */
public class Commande {

    public static ArrayList<String> commandeAExecuter;
    public static HashMap<String, InterfaceCommande> lesCommandes;

    //METHODE
    public static void init(Partie partie, Group root, Scene scene) {
        commandeAExecuter = new ArrayList<>();
        lesCommandes = new HashMap<>(); 
        lesCommandes.put("Affiche", new CommandeAfficheInfo());
        lesCommandes.put("MoveCard", new CommandeMoveCard());
        lesCommandes.put("RotateCard", new CommandeRotateCard());
        lesCommandes.put("closeCard", new CommandeCloseCard());
        lesCommandes.put("Pause", new CommandePause());
        lesCommandes.put("Joue", new CommandeJoue());
        lesCommandes.put("Marchandise", new CommandeInitMarachandise());
        lesCommandes.put("MarchandiseReserve", new ComandeInitMarchandiseReserve());
        lesCommandes.put("Player", new CommandeAddJoueur(partie, scene));
        lesCommandes.put("DessinMap", new CommandeDessinMap(scene));
        lesCommandes.put("Phase", new CommandePhaseEncours());
        lesCommandes.put("CarteAction", new CommandeTuileAction());
        lesCommandes.put("Validate", new CommandeValidateCarte());
        lesCommandes.put("MiseJourInfoJoueur", new CommandeInfoJoueur());
        lesCommandes.put("OrdreTour", new CommandeOrdreTour());
        lesCommandes.put("STOP", new CommandeStop());
    }

    //permet d'executer les commande
    public static void execute(String commande) {
        System.out.println("commande execute : " + commande);
        try {
            lesCommandes.get(commande.split(":")[0]).executeMessage(commande);
        } catch (java.lang.NullPointerException ex) {
            System.out.println("erruer :" + commande);
        }
    }

}
