/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.Commande;

import javafx.application.Platform;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.ControlerPlateau;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.InformationJoueur.ControlerPlateauInfoJoueur;

/**
 *
 * @author greg
 */
public class CommandeInfoJoueur implements InterfaceCommande {

    //exemple : MiseJourInfoJoueur:greg,10,3,5;tom,8,2,3
    @Override
    public void executeMessage(String commande) {
        String cmd = commande.split(":")[1];
        String[] joueurs = cmd.split(";");
        for (int i = 0; i < joueurs.length; i++) {
            String nom = joueurs[i].split(",")[0];
            int point = Integer.parseInt(joueurs[i].split(",")[1]);
            int level = Integer.parseInt(joueurs[i].split(",")[2]);
            int solde = Integer.parseInt(joueurs[i].split(",")[3]);
            ControlerPlateau.miseJourInfo(nom, point, level, solde);
        }
        //permet de mettre a jour les info des joueur
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                ControlerPlateauInfoJoueur.miseAjourInfoJoueur();
            }
        });
    }
}
