/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.Commande;

import javafx.application.Platform;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.naviguationPiece.ControlerNavBarPiece;

/**
 *
 * @author greg
 */
public class CommandeRotateCard implements InterfaceCommande{
        public CommandeRotateCard() {
    }

    @Override
    public void executeMessage(String commande) {
        String cmd = commande.split(":")[1];
        int idCarteRail = Integer.parseInt(cmd.split(",")[0]);
        double rotate = Double.parseDouble(cmd.split(",")[1]);
        String vector = cmd.split(",")[2];

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                //pemret de trouner la carte
                ControlerNavBarPiece.rotateCard(idCarteRail, rotate, vector);
            }
        });

    }
}
