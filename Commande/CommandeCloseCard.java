/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.Commande;

import javafx.application.Platform;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.naviguationPiece.ControlerNavBarPiece;

/**
 *
 * @author greg
 */
public class CommandeCloseCard implements InterfaceCommande {

    public CommandeCloseCard() {
    }

    @Override
    public void executeMessage(String commande) {
        String cmd = commande.split(":")[1];
         String carteType = cmd.split(",")[0];
        int idCarte = Integer.parseInt(cmd.split(",")[1]);

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                //pemret de bouger la carte
                if(carteType.equals("rail")){
                    ControlerNavBarPiece.closeCard(carteType, idCarte);
//                }else{
                    //city...
                }
            }
        });
    }
}
