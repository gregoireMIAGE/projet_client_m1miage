/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.Commande;

import javafx.application.Platform;
import projet_java_gl_reseau.InterfaceGraphique.Information.ControlerInformation;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.InformationJoueur.ControlerPlateauInfoJoueur;

/**
 *
 * @author greg
 */
public class CommandeTuileAction implements InterfaceCommande {

    @Override
    public void executeMessage(String commande) {
        String cmd = commande.split(":")[1];
        String tuile = cmd.split(",")[0];
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                ControlerPlateauInfoJoueur.addTuileAction(tuile);
                //ControlerInformation.setTuileAction(tuile);
                //ControlerInformation.showInfo();
            }
        });
    }

}
