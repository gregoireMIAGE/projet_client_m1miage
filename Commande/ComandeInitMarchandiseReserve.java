/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.Commande;

import javafx.scene.paint.Color;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.map.ControlerPlateauMap;

/**
 *
 * @author greg
 */
public class ComandeInitMarchandiseReserve implements InterfaceCommande {

    public ComandeInitMarchandiseReserve() {
    }

    @Override
    public void executeMessage(String commande) {
        String cmd = commande.split(":")[1];

        Color color = Color.valueOf(cmd.split(",")[0]);
        double width = Double.parseDouble(cmd.split(",")[1]);
        double height = Double.parseDouble(cmd.split(",")[2]);
        ControlerPlateauMap.addMarchandiseReserve(color, width, height);
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
