/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.Commande;

import javafx.application.Platform;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.naviguationPiece.ControlerNavBarPiece;
import projet_java_gl_reseau.Jeu.Map;

/**
 *
 * @author greg
 */
public class CommandeMoveCard implements InterfaceCommande {

    public CommandeMoveCard() {
    }

    @Override
    public void executeMessage(String commande) {
        String cmd = commande.split(":")[1];
        String carteType = cmd.split(",")[0];
        int idCarteRail = Integer.parseInt(cmd.split(",")[1]);
        int translateTargetX = Integer.parseInt(cmd.split(",")[2]);
        int translateTargetY = Integer.parseInt(cmd.split(",")[3]);
        double widthHeight = Double.parseDouble(cmd.split(",")[4]);

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                //pemret de bouger la carte
                ControlerNavBarPiece.moveCard(carteType, idCarteRail, translateTargetX, translateTargetY, widthHeight);
            }
        });

    }

}
