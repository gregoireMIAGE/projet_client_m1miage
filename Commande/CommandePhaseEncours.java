/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.Commande;

import javafx.application.Platform;
import projet_java_gl_reseau.InterfaceGraphique.Information.ControlerInformation;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.ControlerPlateau;

/**
 *
 * @author greg
 */
public class CommandePhaseEncours implements InterfaceCommande {

    public CommandePhaseEncours() {
    }

    @Override
    public void executeMessage(String commande) {
        String cmd = commande.split(":")[1];
        if (cmd.equals("phase2")) {
            ControlerPlateau.phase2();
        } else if (cmd.equals("phase3")) {
            ControlerPlateau.phase3();
        } else {
            ControlerPlateau.otherPhase();
        }
        //affcihe qu'on joue la phase 2 
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if (cmd.equals("phase2")) {
                    ControlerInformation.setTextInfo("joue la phase 2");
                } else if (cmd.equals("phase3")) {
                    ControlerInformation.setTextInfo("joue la phase 3");
                }
            }
        });
    }
}
