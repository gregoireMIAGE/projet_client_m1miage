/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.Commande;

import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Scene;
import projet_java_gl_reseau.InterfaceGraphique.ControlerInterfaceGraphique;
import projet_java_gl_reseau.InterfaceGraphique.Information.ControlerInformation;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.ControlerPlateau;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.infoJeu.ControlerPlateauInfoPartie;
import projet_java_gl_reseau.Jeu.Map;

/**
 *
 * @author greg
 */
public class CommandeDessinMap implements InterfaceCommande {

    private Scene scene;

    public CommandeDessinMap(Scene scene) {
        this.scene = scene;
    }

    @Override
    public synchronized void executeMessage(String message) {
        //dessiner la map
        //initialisation des pannel d'affichage
        String commande = message.split(":")[1];
        String mapStr = commande.split("/")[0];
        String mapStrRail = commande.split("/")[1];

        //ControlerPlateau.initControlerPlateau();
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                ControlerPlateau.dessinPlateau(scene, mapStr, mapStrRail);
                ControlerInformation.initControlerInformation(scene, ControlerInterfaceGraphique.getRootPlateau());
            }
        });//permert de dessiner la map
    }

}
