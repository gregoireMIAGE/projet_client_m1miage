/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau;

import java.util.ArrayList;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import javafx.stage.Stage;
import projet_java_gl_reseau.Commande.Commande;
import projet_java_gl_reseau.InterfaceGraphique.AttenteJoueur.ControlerWaitPannel;
import projet_java_gl_reseau.InterfaceGraphique.Connexion.ControlerConnexion;
import projet_java_gl_reseau.InterfaceGraphique.ControlerInterfaceGraphique;
import projet_java_gl_reseau.InterfaceGraphique.Menu.ControlerMenu;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.ControlerPlateau;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.InformationJoueur.ControlerPlateauInfoJoueur;
import projet_java_gl_reseau.InterfaceGraphique.Plateau.infoJeu.ControlerPlateauInfoPartie;
import projet_java_gl_reseau.Jeu.Map;
import projet_java_gl_reseau.Jeu.Partie;
import projet_java_gl_reseau.controler.Controler;
import projet_java_gl_reseau.player.Compte;
import projet_java_gl_reseau.player.Joueur;

/**
 *
 * @author greg
 */
public class Projet_java_gl_reseau extends Application {

    //private Map map;
    private Partie partie;
    private Joueur joueur;
    //variable pour la scene
    private Scene scene;
    private Group root;
    private ProgressIndicator loading;
    private Label label;
    //couleur determiné par le serveur
    private Color color;
    //thread affichage de données joueur
    private Thread afficheInfo;
    //thread recuperation des informations jouers
    private Thread recupInfo;
    //liste des joueur en attende de la partie
    ArrayList<Joueur> lesJoueurServeur;
    //  variable qui permet de finir les thread d'affichage et recup info
    private static boolean attend = true;

    @Override
    public void start(Stage primaryStage) {
        this.joueur = new Joueur("greg1", Color.RED, new Compte(), 1, 0);

        this.root = new Group();
        this.scene = new Scene(root, Screen.getPrimary().getBounds().getWidth(), Screen.getPrimary().getBounds().getHeight());

        this.partie = new Partie(this.joueur, new Map(this.scene, root, "USA", this.joueur));
        //initialisation des commande disponible
        Commande.init(this.partie, this.root, this.scene);

        //initialisation du controler serveur
        Controler.init(this.partie, this.root);
        ControlerInterfaceGraphique.initControlerInterfaceGraphique(root);

        //affichage de la premiere page
        ControlerConnexion.init(this.scene.getWidth(), this.scene.getHeight(), this.joueur);
        ControlerMenu.init(this.scene.getWidth(), this.scene.getHeight());
        ControlerWaitPannel.init(this.scene.getWidth(), this.scene.getHeight());
        ControlerPlateau.initControlerPlateau(this.joueur);

        //permet d'afficher l'interface de connexion
        //ControlerInterfaceGraphique.afficheConnexion();
        //test
        test();

        this.scene.setRoot(this.root);
        primaryStage.setScene(this.scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void finish() {
        attend = false;
    }

    public static void main(String[] args) {
        launch(args);
    }

    public void test() {
        projet_java_gl_reseau.controler.Controler.start();
        ControlerPlateau.test();
        
        //test en locle, execution des différentes commande pour els différent etat du jeu
        Commande.execute("player:greg1,0xffff00ff,0,1;tom,0x0000ffff,0,1");
        Commande.execute("DessinMap:0,P,P,P,P,VM2,P,P,0;P,P,P,P,V,P,P,P,P;P,P,V,P,P,VJ3,P,P,P;P,P,P,V,P,M,P,P,P;P,P,V,P,P,M,M,P,P;0,M,M,P,P,P,P,P,0/0,0,0,0,1,0,1,0,0;0,0,0,0,1,1,1,0,0;0,0,0,0,0,0,0,0,0;0,0,0,0,0,0,0,0,0;0,0,0,0,0,0,0,0,0;0,0,0,0,0,0,0,0,0");
        Commande.execute("Marchandise:0x000000ff,5,0,12");
        Commande.execute("Marchandise:0x0000ffff,5,0,13");
        Commande.execute("Marchandise:0x008000ff,5,2,14");
        Commande.execute("Marchandise:0x0000ffff,5,2,15");
        Commande.execute("Marchandise:0x0000ffff,5,2,16");
        Commande.execute("MarchandiseReserve:0x008000ff,25,25,0");
        Commande.execute("MarchandiseReserve:0x000000ff,25,25,1");
        Commande.execute("MarchandiseReserve:0x008000ff,25,25,2");
        Commande.execute("MarchandiseReserve:0x008000ff,25,25,3");
        Commande.execute("MarchandiseReserve:0xffff00ff,25,25,4");
        Commande.execute("MarchandiseReserve:0x000000ff,25,25,5");
        Commande.execute("MarchandiseReserve:0x000000ff,25,25,6");
        Commande.execute("MarchandiseReserve:0x000000ff,25,25,7");
        Commande.execute("MarchandiseReserve:0x0000ffff,25,25,8");
        Commande.execute("MarchandiseReserve:0x008000ff,25,25,9");
        Commande.execute("MarchandiseReserve:0x008000ff,25,25,10");
        Commande.execute("MarchandiseReserve:0x0000ffff,25,25,11");
        Commande.execute("Affiche:initialisation de la map...");
        Commande.execute("CarteAction:locomotive");
        Commande.execute("DessinMarchandise");
        Commande.execute("DessinMarchandisesReserves");
        Commande.execute("Phase:phase2");
        Commande.execute("Affiche:greg joue");
        Commande.execute("Affiche:greg a fini");
        Commande.execute("Joue");
    }

    public static void testPhase3() {
        Commande.execute("Pause");
        Commande.execute("Phase:phase3");
        Commande.execute("Affiche:greg joue");
        Commande.execute("Affiche:greg a fini");
        Commande.execute("Joue");
    }

    public static void testPhase4() {
        Commande.execute("Pause");
        Commande.execute("Phase:phase4");
        Commande.execute("Calcul des soldes");
        int solde = ControlerPlateau.calculPaiement();
        int pointvictoire = ControlerPlateau.getNbPointVictoire();
        //Commande.execute("player:greg1,0xffff00ff," + solde + "," + pointvictoire + ";tom,0x0000ffff,0,1");
        ControlerPlateauInfoJoueur.miseAjourSold(String.valueOf(solde));
        ControlerPlateauInfoJoueur.miseNbPoint(String.valueOf(pointvictoire));
        Commande.execute("Affiche:votre solde est " + solde);
        testPhase2();
    }

    public static void testPhase2() {
        Commande.execute("Pause");
        Commande.execute("CarteAction:ingenieur");
        Commande.execute("Phase:phase2");
        Commande.execute("Affiche:greg joue");
        Commande.execute("Affiche:greg a fini");
        Commande.execute("Joue");
    }
}
