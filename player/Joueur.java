/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau.player;

import java.util.ArrayList;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import projet_java_gl_reseau.Jeu.Chemin;

/**
 *
 * @author greg
 */
public class Joueur {

    /*ATTRIBUT*/
    private String name;
    private Color couleur;
    private ArrayList<Chemin> lesChemins;
    private Compte unCompte;
    private int nivLocomotive;
    private int pointVictoire;

    //attribut de dessins
    //cercle
    private Circle NivLocomotive;
    private Circle Compte;
    private Circle NbPoint;
    private ImageView nbPoint;
    //texte
    private Text nbCompteText;

    /*METHODE*/
    public Joueur(String name, Color couleur, Compte unCompte) {
        this.name = name;
        this.couleur = couleur;
        this.unCompte = unCompte;
        this.nivLocomotive = 0;
        this.pointVictoire = 0;
    }

    public Joueur(String name, Color couleur, Compte unCompte, int nivLocomotive, int pointVictoire) {
        this.name = name;
        this.couleur = couleur;
        this.unCompte = unCompte;
        this.nivLocomotive = nivLocomotive;
        this.pointVictoire = pointVictoire;
    }

    /*ACCESSEUR*/
    public String getName() {
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setCouleur(Color color){
        this.couleur = color;
    }
    public Color getCouleur(){
        return this.couleur;
    }
    public void addChemin(Chemin unChemin) {
        this.lesChemins.add(unChemin);
    }

    public void replaceChemin(Chemin newChemin, Chemin ancienChemin) {
        this.lesChemins.remove(ancienChemin);
        this.lesChemins.add(newChemin);
    }

    public int getNivLocomotive() {
        return this.nivLocomotive;
    }

    public int getPointVictoire() {
        return this.pointVictoire;
    }

    public void setNivLocomotive(int level) {
        this.nivLocomotive = level;
    }
    public void addNivLocomotive() {
        this.nivLocomotive =+1;
    }

    public void setPointVictoire(int pointVictoire) {
        this.pointVictoire = pointVictoire;
    }
    
    public void setSolde(int solde){
        unCompte.setSolde(solde);
    }
    public int getSolde(){
       return unCompte.getSolde();
    }

    /*METHODE*/
    //permet de desciner le chemin du joueur
    public void dessineChemin(Scene scene, Group root) {

    }

    public void dessineInformation(Scene scene, Group root) {
        //dessin et texte du nombre 3 
        /*this.NbPoint = new Circle(scene.getWidth() - 300, scene.getHeight() - 50, 25);
        this.NbPoint.setFill(Color.GREEN);
        root.getChildren().add(this.NbPoint);*/

        this.nbPoint = new ImageView(new Image("file:/Users/greg/Desktop/projet_jeu/nb_niveau.png"));
        this.nbPoint.setFitWidth(100);
        this.nbPoint.setFitHeight(50);
        this.nbPoint.setTranslateX(scene.getWidth() - 500);
        this.nbPoint.setTranslateY(scene.getHeight() - 70);
        root.getChildren().add(this.nbPoint);

        this.nbCompteText = new Text(this.nbPoint.getTranslateX() + (this.nbPoint.getFitWidth()/2), this.nbPoint.getTranslateY() + (this.nbPoint.getFitHeight()/2), String.valueOf(this.pointVictoire));
        this.nbCompteText.setFont(Font.font("Verdana", 20));
        root.getChildren().add(this.nbCompteText);

        //dessin du niveau de locomotive
    }
    
    public void dessineChemins(Scene scene, Group root){
        for(int i = 0; i<this.lesChemins.size(); i++){
            this.lesChemins.get(i).dessinerCarteChemin(scene, root);
        }
    }

    //pemret de tester l'egualité par rapport a son nom et ça couleur
    public boolean isEqualJoueur(Joueur unJoueur){
        return (this.name.equals(unJoueur.getName()) /*&& this.couleur.equals(unJoueur.getCouleur())*/);
    }
    public boolean isEqualJoueur(String name){
        return (this.name.equals(name) /*&& this.couleur.equals(unJoueur.getCouleur())*/);
    }
}
